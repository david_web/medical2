<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h4 class='modal-title'>Create poll</h4>
</div>
<div class="modal-body">
	<div id="name_div">
		<label for="name" class="control-label">Poll name</label>
		{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        <span class="help-block">
            <strong id="name-error"></strong>
        </span>
    </div>

	<div class="header">
		<h5 class='modal-title col-md-6'>Options:</h5>
		<div class="col-md-6 text-right" style="padding-right: 0">
			<button id="add_option" class="btn btn-info btn-xs">Add option</button>
		</div>
	</div>

	<div class="alert alert-danger text-center" id="opt_err_div" style="display: none;">
        <strong>The poll must have at least 2 options!</strong>
    </div> 

    <div id="option_div">
    	<ol id="options">
    		<li>
				<div>
					<label for="option" class="control-label">option</label>
					{!! Form::text('option', null, ['class' => 'form-control', 'id' => 'option1']) !!}
		        </div>
	        </li>

	        <li>
				<div>
					<label for="option" class="control-label">option</label>
					{!! Form::text('option', null, ['class' => 'form-control', 'id' => 'option2']) !!}
		        </div>
	        </li>
    	</ol>
    </div>
    <div style="padding: 10px">
	    <div class="checkbox">
			<label>
				<input type="checkbox" class="styled" id="public">
				Public
			</label>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" id="btn_add_poll" class="btn btn-primary">Save</button>
</div>

{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}