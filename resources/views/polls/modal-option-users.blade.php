<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h4 class='modal-title'>Users</h4>
</div>
<div class="modal-body">
	@if(count($users) < 1)
		<span style="font-size: 15px">There is no users</span>
	@else
		<ul class="media-list media-list-linked" style="padding-bottom: 10px">
			@foreach($users as $user)
				<li class="media">
					<a href="{{action('UserController@getUserPage', $user->id)}}" class="media-link">
						<div class="media-left">
							@if(!$user->path)
								<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
							@else
								<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
							@endif
						</div>
						<div class="media-body media-middle">
							<div class="media-heading text-semibold">{{$user->first_name}} {{$user->last_name}}</div>
						</div>
					</a>
				</li>
			@endforeach
		</ul>
	@endif
</div>