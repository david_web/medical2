<div class="col-md-12" style="padding: 0">
	<div class="col-md-12" style="padding: 0">
		<div style="margin-bottom: 10px">
			<span class="text-semibold" style="font-size: 15px">{{$option->poll->name}}</span>
		</div>
		@foreach($option->poll->options as $option)
			<span class="text-semibold">{{$option->name}}</span><span class="pull-right text-semibold">{{round(count($option->users)*100/$pollUsersCount)}} %</span>
			<div class="progress content-group-sm">
				@if($option->poll->is_public == 1 && count($option->users) > 0)
					<a class="option_users" data-id="{{$option->id}}"><span class="progressbar-back-text" style="color: black">{{count($option->users)}}</span></a>
				@else
					<span class="progressbar-back-text" style="color: black">{{count($option->users)}}</span>
				@endif
				<div class="progress-bar progress-bar-info" style="width: {{count($option->users)*100/$pollUsersCount}}%"></div>
			</div>
		@endforeach

		<!-- Modal -->
			<div id="option_users_modal" class="modal fade in">
				<div class="modal-dialog">
					<div class="modal-content" id="option_users_content">
						
					</div>
				</div>
			</div>
		<!-- /Modal -->
	</div>
	<span>{{$pollUsersCount}} votes</span><a class="delete_poll" data-id="{{$option->poll->id}}" style="margin-left: 30px"> delete</a>
</div>