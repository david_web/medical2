@extends('groups.group-layout')

@section('group-content')

<div id="announcement_panel" data-id="{{$group->id}}">
	<div class="col-md-12" style="padding-bottom: 20px">
		<h3 style="margin-top: 4px; margin-bottom: 4px; left: 10px" class="col-md-4">Announcements</h3>
		<div class="col-md-8 text-right">
			<button id="create_announcement_button" class="btn btn-primary">Create announcement</button>
		</div>
	</div>

	<div class="col-md-12" id="announcement_div" style="margin-bottom: 10px">
		@if(count($announcements) == 0)
			<span style="font-size: 18px">There is no announcements in society</span>
		@else
			@foreach($announcements as $announcement)
				<div style="margin-left: 30px" id="announcement_div{{$announcement->id}}">
					<h4>{{$announcement->title}}</h4>
					<span style="font-size: 15px">Date: {{$announcement->date}}</span><br>
					<span style="font-size: 15px">Time: {{$announcement->time}}</span><br>
					@if (Auth::user()->can('delete', $announcement) || Auth::user()->can('update', $groupData))
						<a id="edit_announcement" data-id="{{$announcement->id}}">edit | </a>
						<a class="delete_announcement" data-id="{{$announcement->id}}">delete</a>
					@else
						<span class="text-muted">edit | delete</span>
					@endif
				</div><hr id="hr{{$announcement->id}}">
			@endforeach
		@endif
	</div>
</div>

<!-- Modal -->
	<div id="announcement_modal" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content" id="anouncement_modal_content">

			</div>
		</div>
	</div>
<!-- /Modal -->

@stop

@section('group-javascript')

	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
	{!! HTML::script('assets/js/plugins/pickers/anytime.min.js') !!}
	{!! HTML::script('assets/js/plugins/ui/moment/moment.min.js') !!}
	{!! HTML::script('assets/js/plugins/pickers/daterangepicker.js') !!}
	{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
	{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}
	{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.time.js') !!}
	{!! HTML::script('assets/js/pages/picker_date.js') !!}
	{!! HTML::script('assets/js/main/group-announcements.js') !!}

@stop