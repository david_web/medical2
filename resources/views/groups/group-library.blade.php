@extends('groups.group-layout')

@section('group-content')
<div class="col-md-12" style="padding: 0; padding-bottom: 10px">
	<h3 style="margin-top: 4px; margin-bottom: 4px; left: 20px" class="col-md-4">Society library</h3>
	<div class="col-md-8 text-right">
		@if (Auth::user()->can('update', $groupData))
			<button id="button_add_folder" data-allowed="true" data-toggle="modal" data-target="#add_folder" class="btn btn-primary">Add folder</button>
		@endif
		<button id="button_add_note" class="btn btn-primary">Add note</button>
		<button id="button_add_post" class="btn btn-primary">Add post</button>
	</div>
</div>
<div class="header col-md-12" style="border-top: 1px solid #ddd; margin-top: 15px; border-bottom: 1px solid #ddd; margin-bottom: 15px;">
	<ul class="text-center col-md-10" style="margin-top: 7px;">
		<div class="col-md-6 no-padding">
			<li class="col-md-3 red" id="all_files"><a><b>ALL</b></a></li>
			<li class="col-md-3" id="pdfs"><a><b>PDF</b></a></li>
			<li class="col-md-3 red" id="texts"><a><b>TEXT</b></a></li>
			<li class="col-md-3" id="docs"><a><b>DOCS</b></a></li>
		</div>
		<div class="col-md-6 no-padding">
			<li class="col-md-3 red" id="ppt"><a><b>PPT</b></a></li>
			<li class="col-md-3" id="video"><a><b>VIDEO</b></a></li>
			<li class="col-md-3 red" id="photo"><a><b>PHOTO</b></a></li>
			<li class="col-md-3" id="audio"><a><b>AUDIO</b></a></li>
		</div>
	</ul>
	<div class="input-group col-md-2">
		{!! Form::text('search', null, ['placeholder' => 'Serach...', 'class' => 'form-control']) !!}
		<span class="input-group-btn">
			<button class="btn btn-primary" type="submit"><i class="fa fa-group glyphicon glyphicon-search" style="height: 20px; top: 3px"></i></button>
		</span>
	</div>
</div>
<div class="col-md-12" style="margin: 10px" id="folder_route_div">
	<span style="font-size: 20px;"><a>library/</a></span>
</div>
<div class="col-md-12" id="folder" data-id='{{$group->id}}'>
	<div class="col-md-4" id="folder1">
		<div>
			<div style="width: 140px; position: relative; float: left;" >
				<img style="height: 120px; border: 3px solid #2196f1" src="/assets/images/folder.png">
				<span style="position: absolute; left: 5px; bottom: 0">All files</span>
			</div>
			<div style="height: 120px; padding-left: 150px">
				<div style="margin-bottom: 80px;"><strong>All files</strong></div>
				<div><span class="help-block">Contains {{count($filesData)}} items</span></div>
			</div>
		</div><br>
		@foreach($foldersData as $folderData)
			<div id="folder_div{{$folderData->id}}">
				<div style="width: 140px; position: relative; float: left;" >
					<a class="folder" data-id="{{$folderData->id}}" data-name="{{$folderData->name}}">
						<img style="height: 120px;" src="/assets/images/folder.png">
						<button class="close delete_folder" data-toggle="modal" data-target="#delete_folder_modal{{$folderData->id}}" id="delete_folder{{$folderData->id}}" style="position: absolute; right: 5px; top: 0; display: none;" type="button">×</button>
					</a>
					<span style="position: absolute; left: 5px; bottom: 0">{{$folderData->name}}</span>
				</div>
				<div style="height: 120px; padding-left: 150px">
					<div style="margin-bottom: 80px;"><strong>{{$folderData->name}}</strong></div>
					<div><span class="help-block">Contains {{$folderData->fileCount}} items</span></div>
				</div>
			</div>
			<br id="br{{$folderData->id}}">

			<!-- Modal -->
				<div id="delete_folder_modal{{$folderData->id}}" class="modal fade in" style="margin-top: 10%">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class=" modal-title">You shure?</h4>
							</div>
							<div class="modal-body text-center">
								<button type="button" class="btn btn-primary delete_folder_button" data-id='{{$folderData->id}}' data-dismiss="modal">Yes</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
							</div>
						</div>
					</div>
				</div>
			<!-- /Modal -->
		@endforeach
	</div>
	<div class="col-md-8">
		<div id="files_div" data-type="">
			@foreach($filesData as $fileData)
				<div id="file_div{{$fileData->id}}">
					<div class="panel panel-flat col-md-12" style="height: 120px">
						<div class="col-md-2" style="padding-left: 0">
						   <a><img data-toggle="modal" data-target="#modal{{$fileData->id}}" style="height: 105px; margin: 5px; margin-left: 0" src="/assets/images/{{$fileData->type}}.png"></a>
						</div>
						<div class="col-md-1" >
						    <div style="text-align: center;">
						        <div>
						            <span style="font-size: 20px">1</span>
						            <div><span style="font-size: 14px">files</span></div>
						        </div><br>
							    <span>210 views</span>
						    </div>
						</div>
						<div class="col-md-9">
							<div style="padding: 14px">
								<span>{!!$fileData->description!!}</span>
							</div>
							<div class="col-md-12" style="padding-right: 0">
								@if (Auth::user()->can('update', $groupData) || Auth::user()->can('update', $fileData))
									<div class="col-md-4" style="position:relative; margin-top: 22px;">
										<span>share to</span><br>
										<a class="edit_file" data-id='{{$fileData->id}}'>edit | </a><a data-toggle="modal" data-target="#delete_file_modal{{$fileData->id}}">delete</a>
									</div>
								@else 
									<div class="col-md-4" style="position:relative; margin-top: 40px;">
										<span>share to</span><br>
									</div>
								@endif
								<div class="col-md-8 text-right" style="margin-bottom: 10px; padding-right: 0">
									<div>
										<time class="text-muted" id="{{$fileData->id}}" datetime='{{$fileData->created_at}}'>posted Oct 20 '08 at 13:20</time>
										<div>
											@if(!$fileData->user->path)
												<img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
											@else
												<img style="height: 40px; width: 40px" src="/assets/uploads/{{$fileData->user->path}}" alt="">
											@endif	
											<a href="{{action('UserController@getUserPage', $fileData->user->id)}}" style="margin-left: 10px">{{$fileData->user->first_name}} {{$fileData->user->last_name}}</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12" style="padding-left: 0px; margin-bottom: 15px;">
						<div style="border-bottom: 1px solid #DDDDDD">
							<table class="table table-xxs comments{{$fileData->id}}">
								@foreach($fileData->comments as $comment)
									<tr id="com{{$comment->id}}">
										<td style="font-size: 13px">
											{{$comment->comment}} –&nbsp; <a href="{{action('UserController@getUserPage', $comment->user->id)}}">{{$comment->user->first_name}} {{ $comment->user->last_name}}</a> 
											<span class="text-muted">Apr 13 at 14:51</span><br>
											@if ($comment->user_id == Auth::id())
												<a href="{{action('CommentController@getEditComment', $comment->id)}}">edit</a>
												<a class="del_comment" data-id="{{$comment->id}}">delete</a>
											@endif
										</td>
									</tr>
								@endforeach
							</table>
						</div>
						
						<div style="margin-top: 10px">
							<a id="comment_{{$fileData->id}}" class="add_comment1" data-id="{{$fileData->id}}">add a comment</a>
							<div id="comment{{$fileData->id}}" style="display: none; margin-top: 25px">
									{!! Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Write comment...', 'style' => 'resize: vertical', 'id' => 'comment-'.$fileData->id]) !!}
									<button style="margin-top: 10px" data-id="{{$fileData->id}}" class="btn btn-primary add_comment">Comment</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal -->
					<div id="modal{{$fileData->id}}" class="modal fade in" >
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>
								<div class="modal-body">
									@if($fileData->type == 'mp4')
										<video width="860" controls>
											<source src="/assets/uploads/{{$fileData->path}}">
										</video>
									@else
										<embed src="/assets/uploads/{{$fileData->path}}" width="860" height="800" />
									@endif	
								</div>
							</div>
						</div>
					</div>

					<div id="delete_file_modal{{$fileData->id}}" class="modal fade in" style="margin-top: 10%">
						<div class="modal-dialog modal-xs">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h4 class=" modal-title">You shure?</h4>
								</div>
								<div class="modal-body text-center">
									<button type="button" class="btn btn-primary delete_file_button" data-id='{{$fileData->id}}' data-dismiss="modal">Yes</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
								</div>
							</div>
						</div>
					</div>
				<!-- /Modal -->
			@endforeach
		</div>

		<div id="notes_div">
			@if(count($group->notes) > 0)
				<h4 style="margin-top: 0">Notes:</h4>
			@endif
			@foreach($group->notes as $note)
				<div style="margin-bottom: 15px" class="col-md-2" id="note_div{{$note->id}}">
					<a><img data-toggle="modal" data-target="#note_modal{{$note->id}}" style="height: 80px; margin: 5px; margin-left: 0" src="/assets/images/txt.png"></a>
					<br>
					<span>{{$note->name}}</span>
				</div>

				<!-- Modal -->
					<div id="note_modal{{$note->id}}" class="modal fade in" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>
								<div class="modal-body">
									<embed src="/assets/notes/{{$note->path}}" width="560px" height="550px">
								</div>
								<div class="modal-footer">
									<button class="btn btn-link" id="edit_note_btn" data-id="{{$note->id}}">edit</button>
									<button class="btn btn-link" id="delete_note_btn" data-id="{{$note->id}}">delete</button>
								</div>
							</div>
						</div>
					</div>
				<!-- /Modal -->
			@endforeach
		</div>
	</div>
</div>

<!-- Modal -->
	<div id="add_folder" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Create folder</h4>
				</div>
				<div class="modal-body" id="form">
					<label for="folder_name" class="control-label">Folder name</label>
					{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'folder_name']) !!}
					<span class="help-block">
						<strong id="error"></strong>
					</span>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="button" id="btn_add_folder" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="add_note" class="modal fade in">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" id="note_modal_content">
			
			</div>
		</div>
	</div>

	<div id="add_post" class="modal fade in">
		<div class="modal-dialog modal-lg">
			<div id="post_modal_content" class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">File upload</h4>
				</div>
				<div  class="modal-body">
					{!! Form::open(['action' => ['FileController@postGroupFileUpload', $group->id], 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
						<div style="margin-top: 10px">
							<div id="country_div" class="{{ $errors->has('country') ? ' has-error' : '' }}">
								<label for="country" class="control-label">Country</label>
		                        {!! Form::select('country', ['' => 'Select a country'] + $countries, null, ['class' => 'select-multiple-drag', 'id' => 'country']) !!}
		                            
		                        @if ($errors->has('country'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('country') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>

						<div style="margin-top: 10px">
							<div id="institution_div" class="{{ $errors->has('institution_name') ? ' has-error' : '' }}">
								<label for="institution_name" class="control-label">Institution name</label>
								{!! Form::text('institution_name', null, ['class' => 'form-control', 'id' => 'institution_name']) !!}

		                        @if ($errors->has('institution_name'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('institution_name') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>

						<div style="margin-top: 10px">
							<div id="course_div" class="{{ $errors->has('course') ? ' has-error' : '' }}">
								<label for="course" class="control-label">Course</label>
								{!! Form::text('course', null, ['class' => 'form-control', 'id' => 'course']) !!}

		                        @if ($errors->has('course'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('course') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>	

						<div style="margin-top: 10px">
							<div id="language_div" class="{{ $errors->has('language') ? ' has-error' : '' }}">
								<label for="language" class="control-label">Language of materials</label>
								{!! Form::text('language', null, ['class' => 'form-control', 'id' => 'language']) !!}

		                        @if ($errors->has('language'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('language') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>

						<div style="margin-top: 10px">
							<div id="description_div" class="{{ $errors->has('description') ? ' has-error' : '' }}">
								<label for="description" class="control-label">Short description of the material</label>
								{!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}

		                        @if ($errors->has('description'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('description') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>	

						<div style="margin-top: 10px">
							<label for="choose" class="display-block control-label">Choose folder</label>
							<div class="input-group">
								{!! Form::text(null, null, ['class' => 'form-control', 'id' => 'choose', 'readonly', 'placeholder' => 'No folder choosen', 'data-toggle' => 'modal', 'data-target' => '#choose_folder']) !!}

								{!! Form::hidden('folder_id', null, ['id' => 'hidden_choose']) !!}
								<span class="input-group-btn">
									<button id="choose_folder_button" data-toggle="modal" data-target="#choose_folder" type="button" class="btn btn-primary">Choose Folder</button>
								</span>
							</div>
						</div>

						<div style="margin-top: 10px">
							<div id="file_div" class="{{ $errors->has('file') ? ' has-error' : '' }}">
								<label for="upload" class="display-block control-label">Upload file</label>
								{{Form::file(null, ['class' => 'file-styled', 'id' => 'upload', 'name' => 'file'])}}
		                        <span class="help-block">Accepted formats: txt, pdf</span>

		                        @if ($errors->has('file'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('file') }}</strong>
		                            </span>
		                        @endif
							</div>
						</div>

			            <div class="text-right">
			            	<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			            </div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

    <div id="choose_folder" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" id="folder_modal">×</button>
					<h4 class=" modal-title">Choose folder</h4>
				</div>

				<div id="modal_folder" data-id="{{$group->id}}" class="modal-body" style="overflow: auto;">
					
				</div>

				<div class="modal-footer">
					<button id="save_folder" data-id="" data-name="" type="button" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>
<!-- /Modal -->

@stop

@section('group-javascript')

@stop