@extends('groups.group-layout')

@section('group-content')

<div class="panel-heading">
	<h5 class="panel-title" style="color: black">Management</h5>
</div>

<ul class="media-list media-list-linked" style="padding-bottom: 10px">
	@foreach($groupData->users as $user)
		<li class="media">
			<a href="{{action('UserController@getUserPage', $user->id)}}" class="media-link">
				<div class="media-left">
					@if(!$user->path)
						<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
					@else
						<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
					@endif
				</div>
				<div class="media-body">
					<div class="media-heading text-semibold">{{$user->first_name}} {{$user->last_name}}</div>
					<span class="text-muted">{{$user->pivot['role']}}</span>
				</div>
			</a>
		</li>
	@endforeach
</ul>

@stop

@section('group-javascript')


@stop