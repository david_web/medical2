@extends('layouts.layout')

@section('content')
<div class="col-md-9" id="group" data-id="{{$group->id}}">
	<div class="panel panel-flat col-md-12">
		<div class="panel-heading" style="padding: 0; overflow: auto;">
			<h2 class="col-md-9"; style="left: 20px">{{$group->name}}</h2>
			<div class="input-group col-md-3" style="margin-top: 20px;">
				{!! Form::text('search', null, ['placeholder' => 'Serach...', 'class' => 'form-control']) !!}
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="fa fa-group glyphicon glyphicon-search" style="height: 20px; top: 3px"></i></button>
				</span>
			</div><hr>
			<div class="header col-md-12">
				<ul class="text-center">
					@if(!$action)
						<li class="col-md-1"><a href="{{action('GroupController@getGroup', $group->id)}}"><b>library</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupAnnouncements', $group->id)}}"><b>announcements</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupDiscussion', $group->id)}}"><b>discussion</b></a></li>
						<li class="col-md-1"><a href="{{action('GroupController@getGroupPolls', $group->id)}}"><b>polls</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getSubGroups', $group->id)}}"><b>sub-groups</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupManagement', $group->id)}}"><b>management</b></a></li>
						<li class="col-md-2"><a data-toggle="modal" data-target="#add_user"><b>invite friend</b></a></li>
					@else
						<li class="col-md-1"><a href="{{action('GroupController@getGroup', $group->id)}}"><b>library</b></a></li>
						<li class="col-md-3"><a href="{{action('GroupController@getGroupAnnouncements', $group->id)}}"><b>announcements</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupDiscussion', $group->id)}}"><b>discussion</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupPolls', $group->id)}}"><b>polls</b></a></li>
						<li class="col-md-2"><a href="{{action('GroupController@getGroupManagement', $group->id)}}"><b>management</b></a></li>
						<li class="col-md-2"><a data-toggle="modal" data-target="#add_user"><b>invite friend</b></a></li>
					@endif
				</ul>
			</div>
		</div><hr>

		<div class="panel-body" style="padding: 10px;">
			@yield('group-content')
		</div>
	</div>
</div>

<div class="col-md-3">
	<div class="panel panel-flat">
		<div class="panel-body" style="text-align: center;">
			@if (Auth::user()->can('update', $groupData))
				@if(!$group->path)
					<a id="upload_image_img" data-toggle="modal" data-target="#upload_image"><img class="col-md-12" src="/assets/images/placeholder.jpg"></a>
				@else
					<a id="upload_image_img" data-toggle="modal" data-target="#upload_image"><img class="col-md-12" style="padding: 0;" src="/assets/uploads/{{$group->path}}"></a>
				@endif
			@else 
				@if(!$group->path)
					<img class="col-md-12" src="/assets/images/placeholder.jpg">
				@else
					<img class="col-md-12" style="max-width: 320px" src="/assets/uploads/{{$group->path}}">
				@endif
			@endif
		</div>
	</div>

	<div class="panel panel-flat col-md-12">
		<a data-toggle="modal" data-target="#members_modal">
			<div class="panel-heading">
				<h5 class="panel-title" style="color: black">Members <span style="color: grey; font-size: 14px; padding-left: 10px">{{count($group->users)}}</span></h5>
			</div>
		</a><hr style="margin: 0">

		<ul class="media-list media-list-linked" style="padding-bottom: 10px">
			@foreach($group->users as $user)
				<li class="media">
					<a href="{{action('UserController@getUserPage', $user->id)}}" class="media-link">
						<div class="media-left">
							@if(!$user->path)
								<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
							@else
								<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
							@endif
						</div>
						<div class="media-body media-middle">
							<div class="media-heading text-semibold">{{$user->first_name}} {{$user->last_name}}</div>
						</div>
					</a>
				</li>
			@endforeach
		</ul>
	</div>

	@if (Auth::user()->can('update', $groupData) && count($groupUserRequests->users) != 0)
		<div class="panel panel-flat col-md-12">
			<a data-toggle="modal" data-target="#user_requests_modal">
				<div class="panel-heading">
					<h5 class="panel-title" style="color: black">User requests <span style="color: grey; font-size: 14px; padding-left: 10px">{{count($groupUserRequests->users)}}</span></h5>
				</div>
			</a><hr style="margin: 0">

			<ul class="media-list media-list-linked" style="padding-bottom: 10px">
				@foreach($groupUserRequests->users as $user)
					<li class="media">
						<a href="{{action('UserController@getUserPage', $user->id)}}" class="media-link">
							<div class="media-left">
								@if(!$user->path)
									<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
								@else
									<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
								@endif
							</div>
							<div class="media-body media-middle">
								<div class="media-heading text-semibold">{{$user->first_name}} {{$user->last_name}}</div>
							</div>
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	@endif
</div>

<!-- Modal-->
	<div id="user_requests_modal" class="modal fade in">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">User requests <span style="color: grey; font-size: 15px; padding-left: 10px">{{count($groupUserRequests->users)}}</span></h4>
				</div>

				<div class="modal-body">
					<ul class="media-list" style="padding: 10px">
						@foreach($groupUserRequests->users as $user)
							<li class="media" >
								<div class="media-left media-middle">
									@if(!$user->path)
										<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
									@else
										<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
									@endif
								</div>

								<div class="media-body media-middle">
									<div class="media-heading text-semibold"><a href="{{action('UserController@getUserPage', $user->id)}}">{{$user->first_name}} {{$user->last_name}}</a></div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list text-nowrap">
				                    	<li class="dropdown">
				                    		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>

				                    		<ul class="dropdown-menu dropdown-menu-right">
						                    	<li><a href="{{action('GroupController@getAcceptRequest', [$group->id, $user->id])}}" data-toggle="modal"> Accept</a></li>
						                    	<li><a href="{{action('GroupController@getRejectRequest', [$group->id, $user->id])}}" data-toggle="modal"> Reject</a></li>
				                    		</ul>
				                    	</li>
				                	</ul>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="upload_image" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Upload group image</h4>
				</div>
				<div class="modal-body">
					{!! Form::open(['action' => ['GroupController@postGroupImageUpload', $group->id], 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
						<div id="image_div" class="{{ $errors->has('image') ? ' has-error' : '' }}">
							<label for="upload" class="control-label">Upload image</label>
							{{Form::file('image', ['class' => 'file-styled', 'id' => 'upload'])}}
	                        <span class="help-block">Accepted formats: gif, png, jpg.</span>
	                        <i class="icon-trash position-left"></i><a href="{{action('GroupController@getDeleteGroupImage', $group->id)}}">delete image</a>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

	<div id="add_user" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Invite friend</h4>
				</div>

				{!! Form::open(['action' => ['GroupController@postAddUserToGroup', $group->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
					<div class="modal-body">
						<div id="users">
							<label for="group_users" style="margin-left: 5px">Friends:</label>
							{!! Form::select(null, $allusers, null, ['class' => 'select-multiple-drag group_users', 'multiple' => 'multiple']) !!}
							<input id="user" type="hidden" name="users_id">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
						<button type="submit" id="btn_add_folder" class="btn btn-primary">Save</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div id="members_modal" class="modal fade in">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Members <span style="color: grey; font-size: 15px; padding-left: 10px">{{count($group->users)}}</span></h4>
				</div>

				<div class="modal-body">
					<ul class="media-list media-list-linked">
						@foreach($group->users as $user)
							<li class="media">
								<div class="media-link cursor-pointer" data-toggle="collapse" data-target="#col{{$user->id}}">
									<div class="media-left">
										@if(!$user->path)
											<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
										@else
											<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
										@endif
									</div>
									<div class="media-body media-middle">
										<div class="media-heading text-semibold"><a href="{{action('UserController@getUserPage', $user->id)}}" style="color: black">{{$user->first_name}} {{$user->last_name}}</a></div>
									</div>
									@if (Auth::user()->can('update', $groupData))
										<div class="media-right media-middle text-nowrap">
											<i class="icon-menu7 display-block"></i>
										</div>
									@endif
								</div>
								@if (Auth::user()->can('update', $groupData))
									@if (Auth::id() != $user->id)
										<div class="collapse" id="col{{$user->id}}">
											<div class="contact-details">
												@if (Auth::user()->can('update', $groupAdmins))
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>User role</label>
																<div class="radio">
																	<label>
																		<span class="radioRole" data-id="{{$user->id}}" data-role="admin">
																			@if ($user->pivot['role'] == 'admin')
																				<input name="{{$user->id}}" type="radio" class="styled" checked="checked">
																			@else
																				<input name="{{$user->id}}" type="radio" class="styled">
																			@endif
																		</span>
																		Admin
																	</label>
																</div>
																<div class="radio">
																	<label>
																		<span class="radioRole" data-id="{{$user->id}}" data-role="manager">
																			@if ($user->pivot['role'] == 'manager')
																				<input type="radio" name="{{$user->id}}" class="styled" checked="checked">
																			@else
																				<input type="radio" name="{{$user->id}}" class="styled">
																			@endif
																		</span>
																		Manager
																	</label>
																</div>
																<div class="radio">
																	<label>
																		<span class="radioRole" data-id="{{$user->id}}" data-role="member">
																			@if ($user->pivot['role'] == 'member')
																				<input type="radio" name="{{$user->id}}" class="styled" checked="checked">
																			@else
																				<input type="radio" name="{{$user->id}}" class="styled">
																			@endif
																		</span>
																		Member
																	</label>
																</div>
															</div>
														</div>
													</div>
												@endif
												<ul class="list-extended list-unstyled list-icons">
													<li><i class="icon-trash position-left"></i><a class="delete_user" data-id="{{$user->id}}">delete user</a></li>
												</ul>

												<!-- Delete-user-modal -->
													<div id="delete_user_modal{{$user->id}}" class="modal fade in" style="margin-top: 20%">
														<div class="modal-dialog modal-xs">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-id="{{$user->id}}">×</button>
																	<h4 class=" modal-title">You shure?</h4>
																</div>
																<div class="modal-body text-center">
																	<a href="{{action('GroupController@getDeleteUser', [$group->id, $user->id])}}"><button type="button" class="btn btn-primary">Yes</button></a>
																	<button type="button" class="btn btn-primary delete_user_no" data-id="{{$user->id}}">No</button>
																</div>
															</div>
														</div>
													</div>
												<!-- /Delete-user-modal -->
											</div>
										</div>
									@endif
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- /Modal -->
@stop

@section('javascript')

	@yield('group-javascript')
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
	{!! HTML::script('assets/js/main/group.js') !!}
	{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}
	{!! HTML::script('assets/js/core/libraries/jasny_bootstrap.min.js') !!}
	{!! HTML::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
	{!! HTML::script('assets/js/plugins/visualization/echarts/echarts.js') !!}

@stop