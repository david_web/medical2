@extends('groups.group-layout')

@section('group-content')

<div class="col-md-12" style="padding-bottom: 20px">
	<h3 style="margin-top: 4px; margin-bottom: 4px; left: 10px" class="col-md-4">Sub-groups</h3>
	@if (Auth::user()->can('update', $groupData))
		<div class="col-md-8 text-right">
			<button class="btn btn-primary" data-toggle="modal" data-target="#create_sub_group">Create sub-group</button>
		</div>
	@endif
</div>
@if(count($subGroups) == 0)
	<div class="col-md-12" style="padding-bottom: 10px">
		<span style="font-size: 18px;">There is no sub-groups in society</span>
	</div>
@else
	<div class="col-md-12">
		<ul class="media-list" style="padding: 20px">
			@foreach($subGroups as $subGroup)
				<li class="media" >
					<div class="media-left media-middle">
						@if(!$subGroup->path)
							<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
						@else
							<img src="/assets/uploads/{{$subGroup->path}}" class="img-circle" alt="">
						@endif
					</div>

					<div class="media-body media-middle">
						<div class="media-heading text-semibold">
							@if(Auth::user()->can('view', $subGroup))
								<a href="{{action('GroupController@getGroup', $subGroup->id)}}">{{$subGroup->name}}</a>
							@else
								<span>{{$subGroup->name}}</span>
							@endif
						</div>
						<span style="color: grey">{{count($subGroup->users)}} members</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
	                    	<li class="dropdown">
	                    		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>

	                    		<ul class="dropdown-menu dropdown-menu-right">
	                    			@if(Auth::user()->can('view', $subGroup))
			                    		<li><a href="{{action('GroupController@getLeaveGroup', $subGroup->id)}}"><i class="glyphicon glyphicon-minus"></i> leave society</a></li>
			                    	@elseif(Auth::user()->groups->where('group_accepted', 0)->contains($subGroup))
			                    		<li><a>request has been sent</a></li>
			                    	@else
			                    		<li><a href="{{action('GroupController@getAskToJoin', $subGroup->id)}}" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> ask to join</a></li>
			                    	@endif
	                    		</ul>
	                    	</li>
	                	</ul>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endif

<!-- Modal -->
	<div id="create_sub_group" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Create sub-group</h4>
				</div>

				{!! Form::open(['action' => ['GroupController@postAddSubGroup', $group->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
					<div class="modal-body">
						<div id="name">
							<label for="sub_group_name" class="control-label">Sub-group name</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'group_name']) !!}
						</div>

						<div id="users">
							<label for="group_users" style="margin-left: 5px">Add user:</label>
							{!! Form::select(null, $allusers, null, ['class' => 'select-multiple-drag group_users', 'multiple' => 'multiple']) !!}
							<input id="user" type="hidden" name="users_id">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
						<button type="submit" id="btn_add_folder" class="btn btn-primary">Save</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
<!-- /Modal -->

@stop

@section('group-javascript')

@stop