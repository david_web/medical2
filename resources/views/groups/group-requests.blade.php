@extends('groups.groups-layout')

@section('groups-content')

<div class="col-md-12">
	@if(count($groupRequests) > 0)
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h2 style="margin: 0px;">Groups: </h2>
			</div>
			<div class="panel-body">
				<div class="table-responsive text-center">
					<table class="table">
						<thead>
							<tr>
								<th class="text-center">Group name</th>
								<th class="text-center">Created at</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($groupRequests as $group)
								<tr id="request{{$group->id}}">
									<td>{{$group->name}}</td>
									<td>{{$group->created_at->toDateString()}}</td>
									<td>
										<button class="btn btn-success accept" data-id="{{$group->id}}">Accept</button>
										<button class="btn btn-danger reject" data-id="{{$group->id}}">Reject</button>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@else
		<h2>You have no society requests</h2>
	@endif
</div>

@stop

@section('javascript')
{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/main/group-requests.js') !!}
@stop