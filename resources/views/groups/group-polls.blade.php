@extends('groups.group-layout')

@section('group-content')

<div id="poll_panel" data-id="{{$group->id}}">
	<div class="col-md-12" style="padding-bottom: 20px">
		<h3 style="margin-top: 4px; margin-bottom: 4px; left: 10px" class="col-md-4">Polls</h3>
		<div class="col-md-8 text-right">
			<button id="create_poll_button" class="btn btn-primary">Create poll</button>
		</div>
	</div>

	<div class="col-md-12" id="polls_div" style="margin-bottom: 10px">
		@if(count($polls) == 0)
			<span style="font-size: 18px">There is no polls in society</span>
		@else
			<hr>
			@foreach($polls as $poll)
				<?php
					$isSelected = false;
					foreach(Auth::user()->options as $option) {
						if ($poll->id == $option->poll_id)
						{
							$isSelected = true;
						}
					}
				?>
				@if($isSelected)
					<div class="form-group col-md-12" style="border-bottom: 2px solid lightgrey; padding-bottom: 20px">
						<div class="col-md-12" style="padding: 0">
							<div style="margin-bottom: 10px">
								<span class="text-semibold" style="font-size: 15px">{{$poll->name}}</span>
							</div>
							@foreach($poll->options as $option)
								<span>{{$option->name}}</span><span class="pull-right text-semibold">{{round(count($option->users)*100/$poll['users_count'])}} %</span>
								<div class="progress content-group-sm">
									@if($poll->is_public == 1 && count($option->users) > 0)
										<a class="option_users" data-id="{{$option->id}}"><span class="progressbar-back-text" style="color: black">{{count($option->users)}}</span></a>
									@else
										<span class="progressbar-back-text" style="color: black">{{count($option->users)}}</span>
									@endif
									<div class="progress-bar progress-bar-info" style="width: {{count($option->users)*100/$poll['users_count']}}%"></div>
								</div>
							@endforeach
						</div>
						<span>{{$poll['users_count']}} votes</span><a class="delete_poll" data-id="{{$poll->id}}" style="margin-left: 30px">delete</a>
					</div>

					<!-- Modal -->
						<div id="option_users_modal" class="modal fade in">
							<div class="modal-dialog">
								<div class="modal-content" id="option_users_content">
									
								</div>
							</div>
						</div>
					<!-- /Modal -->
				@else
					<div class="form-group col-md-12" id="poll_div{{$poll->id}}" style="border-bottom: 2px solid lightgrey; padding-bottom: 20px">
						<label class="text-semibold" style="font-size: 15px">{{$poll->name}}</label>
						<div class="row">
							<div class="col-md-12">
								@foreach($poll->options as $option)
									<div class="radio">
										<label>
											<span>
												<input name="{{$poll->id}}" data-id="{{$option->id}}" type="radio" class="styled poll_option">
											</span>
											{{$option->name}}
										</label>
									</div>
								@endforeach
							</div>
						</div>
						<span>{{$poll['users_count']}} votes</span><a class="delete_poll" data-id="{{$poll->id}}" style="margin-left: 30px"> delete</a>
					</div>
				@endif
			@endforeach
		@endif
	</div>
</div>

<!-- Modal -->
	<div id="poll_modal" class="modal fade in">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" id="poll_modal_content">
				
			</div>
		</div>
	</div>
<!-- /Modal -->

@stop

@section('group-javascript')

{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/main/group-polls.js') !!}

@stop