@extends('layouts.layout')

@section('content')

<div class="panel panel-flat">
	<div class="panel-heading header">
		<ul class="col-md-10"  style="margin-top: 7px; padding-left: 5%">
			<li class="col-md-3"><a href="{{action('GroupController@getGroups')}}"><b>All societies</b></a></li>
			<li class="col-md-3"><a href="{{action('GroupController@getMyGroups')}}"><b>My societies</b></a></li>
			<li class="col-md-3"><a href="{{action('GroupController@getGroupRequests')}}"><b>Society requests <span id="req_count">@if(count($groupRequests) > 0) ({{count($groupRequests)}}) @endif</span></b></a></li>
			<li class="col-md-3"><a data-toggle="modal" data-target="#create_group"><b>Create societiy</b></a></li>
		</ul>
		<div class="input-group col-md-2">
			{!! Form::text('search', null, ['placeholder' => 'Serach...', 'class' => 'form-control']) !!}
			<span class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="fa fa-group glyphicon glyphicon-search" style="height: 20px; top: 3px"></i></button>
			</span>
		</div>
	</div><hr style="margin: 0">
</div>

<!-- Modals -->
	<div id="Delete" class="modal fade in" style="margin-top: 14%">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h2 class="modal-title text-center">You shure?</h2>
				</div>
				<div class="modal-body" style="text-align: center;">
					<button type="button" class="btn btn-primary delete_user" data-dismiss="modal" style="margin-right: 10px">Yes</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>

	<div id="create_group" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Create society</h4>
				</div>

				{!! Form::open(['action' => 'GroupController@postAddGroup', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
					<div class="modal-body">
						<div id="name">
							<label for="group_name" class="control-label">Society name</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'group_name']) !!}
		                    <span class="help-block">
								<strong id="error"></strong>
		                    </span>
						</div>

						<div id="users">
							<label for="group_users" style="margin-left: 5px">Add user:</label>
							{!! Form::select(null, $allusers, null, ['class' => 'select-multiple-drag group_users', 'multiple' => 'multiple']) !!}
							<input id="user" type="hidden" name="users_id">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
						<button type="submit" id="btn_add_folder" class="btn btn-primary">Save</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
<!-- /Modals -->


@yield('groups-content')

@stop

@section('javascript')
	
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}

	<script type="text/javascript">
		$(".group_users").select2();

		function displayUsers() {
			var users = $( ".group_users" ).val() || [];
			$('#user').val(JSON.stringify(users));
		};
		$( "select" ).change( displayUsers );
	</script>
@endsection