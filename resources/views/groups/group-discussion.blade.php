@extends('groups.group-layout')

@section('group-content')

	<div class="panel-heading">
		<h6 class="panel-title text-semiold">Discussion</h6>
		<div class="heading-elements">
			<ul class="list-inline list-inline-separate heading-text text-muted">
				<li>{{count($discussions)}} comments</li>
			</ul>
    	</div>
	</div>

	<hr class="no-margin">

	<div class="panel-body pre-scrollabl">
		<ul class="media-list stack-media-on-mobile">
		@foreach($discussions as $discussion)
			<li class="media">
				<div class="media-left">
					@if(!$discussion->user->path)
						<img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
					@else
						<img src="/assets/uploads/{{$discussion->user->path}}" class="img-circle img-sm" alt="">
					@endif
				</div>

				<div class="media-body">
					<div class="media-heading">
						<a href="{{action('UserController@getUserPage', $discussion->user->id)}}" class="text-semibold">
							{{ $discussion->user->first_name . ' ' . $discussion->user->last_name }}
						</a>
						<span class="media-annotation dotted">
							{{$discussion->created_at->format('M n o, g:i A')}}
						</span>
					</div>

					<p>{{ $discussion->message }}</p>

					<ul class="list-inline list-inline-separate text-size-small">
						<!-- <li>70 
							<a href="#"><i class="icon-arrow-up22 text-success"></i></a>
							<a href="#"><i class="icon-arrow-down22 text-danger"></i></a>
						</li> -->
						<li class="reply cursor-pointer" data-message="{{$discussion->message}}" data-id="{{ $discussion->id }}">
							<a>Reply</a>
						</li>

						@if (Auth::user()->can('update', $groupData) || Auth::user()->can('update', $discussion))
							<li class="cursor-pointer edit" data-type="disc" data-id="{{$discussion->id}}" data-message="{{$discussion->message}}">
								<a>Edit</a>
							</li>
							<li><a class="text-danger" href="/delete-discussion/{{$discussion->id}}/{{$group->id}}">Delete</a></li>
						@endif
					</ul>
					@if(count($discussion->subDiscussions) > 0)
						@foreach($discussion->subDiscussions as $subDiscussion)
						<div class="media">
							<div class="media-left">
								@if(!$subDiscussion->user->path)
									<img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
								@else
									<img src="/assets/uploads/{{$subDiscussion->user->path}}" class="img-circle img-sm" alt="">
								@endif
							</div>

							<div class="media-body">
								<div class="media-heading">
									<a href="{{action('UserController@getUserPage', $subDiscussion->user->id)}}" class="text-semibold">
										{{$subDiscussion->user->first_name . ' ' . $subDiscussion->user->last_name }}
									</a>
									<span class="media-annotation dotted">
										{{$discussion->created_at->format('M n o, g:i A')}}
									</span>
								</div>

								<p>{{ $subDiscussion->message }}</p>

								<ul class="list-inline list-inline-separate text-size-small">
									<!-- <li>67 
										<a href="#"><i class="icon-arrow-up22 text-success"></i></a>
										<a href="#"><i class="icon-arrow-down22 text-danger"></i></a>
									</li> -->
									@if (Auth::user()->can('update', $groupData) || Auth::user()->can('update', $subDiscussion))
										<li class="cursor-pointer edit" data-type="subDisc" data-id="{{ $subDiscussion->id }}" data-message="{{ $subDiscussion->message }}">
											<a>Edit</a>
										</li>
										<li><a class="text-danger" href="/delete-subdiscussion/{{$subDiscussion->id}}/{{$group->id}}">Delete</a></li>
									@endif
								</ul>
							</div>
						</div>
						@endforeach
					@endif
				</div>
			</li>
		@endforeach
		</ul>
	</div>

	<hr class="no-margin">

	<div class="panel-body">	
		{{ Form::open(['url' => 'add-discussion', 'method' => 'POST', 'id' => 'discussForm']) }}
    		<div class="col-md-12 text-right">
				<input type="hidden" name="group_id" value="{{ $group->id }}">
				<input type="hidden" name="discussion_id" value="" id="discussion_id">
				<input type="hidden" name="subdiscussion_id" value="" id="subdiscussion_id">
	        	<textarea name="message" class="form-control content-group" style="resize: vertical" rows="3" cols="1" placeholder="Enter your message..."></textarea>
                <button type="button" class="btn btn-default" id="cancell" style="display: none"><i class="icon-cross2"></i> Cancel</button>
                <button type="submit" class="btn bg-blue" id="edit" style="display: none"><b><i class="icon-pencil6"></i></b> Edit</button>
                <button type="submit" class="btn bg-blue" id="reply" style="display: none"><b><i class="icon-reply"></i></b> Reply</button>
                <button type="submit" class="btn bg-blue" id="send"><i class="icon-plus22"></i> Add comment</button>
    		</div>
        {{ Form::close() }}
	</div>

@stop

@section('group-javascript')

{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/main/group-polls.js') !!}

@stop