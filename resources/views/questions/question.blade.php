@extends('layouts.layout')

@section('content')

@if (Session::has('success'))
    <div class="alert alert-success">
        <strong>Success!</strong> {{ Session::get('success') }}
    </div>
@elseif (Session::has('danger'))
    <div class="alert alert-danger">
        <strong>Whoops!</strong> {{ Session::get('danger') }}
    </div>             
@endif

<div class="panel-heading" style="padding-bottom: 0px">
	<h1 class="heading-title">{{$questionData->title}}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h1><hr>
</div>

<div class="panel-body" style="padding-top: 0px">
	{!!$questionData->description!!}
</div>

<div class="panel-footer" style="border:none; background-color: #F5F5F5">
	<div class="heading-elements col-md-6">
		@foreach ($questionData->tags as $tag)
			<span class="heading-text" style="margin-left: 10px;  font-size: 13px; background-color: #455A64; color: #FFF; margin-top: 1px; padding-top: 7px; padding-bottom: 7px; padding-left: 14px; padding-right: 14px; border-radius: 3px;" ;>
				{{$tag->tag}}
			</span>
		@endforeach

		<div class="col-md-12" style="margin-top: 10px">
			@if (Auth::user()->can('update', $questionData))
				<a href="{{action('QuestionController@getEditQuestion', $questionData->id)}}">edit</a>
				<a href="{{action('QuestionController@getDeleteQuestion', $questionData->id)}}">delete</a>
			@else
				<span class="text-muted">edit</span>
				<span class="text-muted">delete</span>
			@endif
		</div>
	</div>
	<div class="col-md-2" style="padding: 10px; background-color: #E0EAF1">
		<div>
			<time class="text-muted" id="{{$questionData->id}}" datetime='{{$questionData->created_at}}'>asked Oct 20 '08 at 13:20</time>
		</div>
		<div>
			@if(!$questionData->user->path)
				<img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
			@else
				<img style="height: 40px; width: 40px" src="/assets/uploads/{{$questionData->user->path}}" alt="">
			@endif
			<a href="{{action('UserController@getUserPage', $questionData->user->id)}}" style="margin-left: 10px">{{$questionData->user->first_name}} {{$questionData->user->last_name}}</a>
		</div>
	</div>
</div>
<div class="col-md-8" style="text-align: right; margin-top: 5px">
	<a id="like" data-id='{{$questionData->id}}'>
		@if($questionData->likes->contains(Auth::user()))
			<img class="like1" src="/assets/images/like-field.png" style="height: 23px">
		@else
			<img class="like1" src="/assets/images/like.png" style="height: 23px">
		@endif
	</a>
	<span class="likesCount">
		@if ($likesCount->likes_count != 0)
			{{$likesCount->likes_count}}
		@endif
	</span>
</div>
<div class="col-md-8" style="padding-left: 0px; margin: 15px;">
	<div style="border-bottom: 1px solid #DDDDDD">
		<table class="table table-xxs comments">
			@foreach($questionData->comments as $comment)
				<tr id="{{$comment->id}}">
					<td style="font-size: 13px">
						{{$comment->comment}} –&nbsp; <a href="#">{{$comment->user->first_name}} {{ $comment->user->last_name}}</a> <span class="text-muted">Apr 13 at 14:51<span><br>
						@if ($comment->user_id == Auth::id())
							<a href="{{action('CommentController@getEditComment', $comment->id)}}">edit</a>
							<a class="del_comment" data-id="{{$comment->id}}">delete</a>
						@endif	
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div style="margin-top: 10px">
		<a id="comment">add a comment</a>
		<div id="comment1" style="display: none; margin-top: 25px">
				{!! Form::textarea('comment', null, ['class' => 'form-control comment', 'placeholder' => 'Write comment...']) !!}
				<button style="margin-top: 10px" data-id="{{$questionData->id}}" class="btn btn-primary add_comment">Comment</button>
		</div>
	</div>
</div>

@stop

@section('javascript')

	{!! HTML::script('assets/js/main/question.js') !!}

@stop