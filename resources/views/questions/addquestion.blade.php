@extends('layouts.layout')

@section('content')

@if($action == 'add')
	{!! Form::open(['action' => 'QuestionController@postAddQuestion', 'method' => 'POST']) !!}
@else
	{!! Form::model($question, ['action' => ['QuestionController@postEditQuestion', $question->id], 'method' => 'POST']) !!}
@endif
	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		<label for="title" style="margin-left: 5px">Title:</label>
		{!! Form::text('title', null, ['placeholder' => 'Title', 'class' => 'form-control', 'id' => 'title']) !!}<br>

		@if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
    	@endif

	</div>	

	<label for="editor-full" style="margin-left: 5px">Description:</label>
	<div class="content-group{{ $errors->has('description') ? ' has-error' : '' }}">
		{!! Form::textarea('description', null, ['id' => 'editor-full', 'rows' => '4', 'cols' => '4']); !!}<br>

		@if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
	    <div class="form-group{{ $errors->has('tag_id') ? ' has-error' : '' }}">
			<label for="tags" style="margin-left: 5px">Tags:</label>
			{!! Form::select('tag_id', $alltags, null, ['class' => 'select-multiple-drag tags', 'multiple' => 'multiple']) !!}
			
			<input id="tag" type="hidden" name="tags">

			@if ($errors->has('tag_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('tag_id') }}</strong>
	            </span>
        	@endif
		</div>
    </div>


    <div class="text-right">
	    @if($action == 'add')
	        <button id="submit" type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
	    @else
	    	<button id="submit" type="submit" class="btn bg-teal-400">Edit form <i class="icon-arrow-right14 position-right"></i></button>
	    @endif    
    </div>
{!! Form::close() !!}

@stop

@section('javascript')

{!! HTML::script('ckeditor/ckeditor.js') !!}
{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}

    <script type="text/javascript">
        CKEDITOR.replace('editor-full')
        $(".tags").select2();

        function displayTags() {
			var tags = $( ".tags" ).val();
			$('#tag').val(JSON.stringify(tags));
			if (tags == null) {
				$('#tag').val('');
			}
		}
		$( "select" ).change( displayTags );

		$( '.tags' ).trigger( "change" );

    </script>

@stop