<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h4 class=" modal-title">Create note</h4>
</div>
@if($action == 'edit')
	{!! Form::model($note, ['class' => 'form-horizontal']) !!}
@endif
<div class="modal-body" id="form">
	<div id="note_name_div">
		<label for="note_name" class="control-label">Note name</label>
		{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'note_name']) !!}
	    <span class="help-block">
	        <strong id="note-name-error"></strong>
	    </span>
	</div>

	<div style="margin-top: 10px">
		<label for="note_folder" class="display-block control-label">Choose folder</label>
		<div class="input-group">
			{!! Form::text(null, $name, ['class' => 'form-control', 'id' => 'note_folder', 'readonly', 'placeholder' => 'No folder choosen', 'data-toggle' => 'modal', 'data-target' => '#choose_folder']) !!}

			{!! Form::hidden('folder_id', null, ['id' => 'note_folder_id', 'readonly']) !!}
			<span class="input-group-btn">
				<button id="choose_folder_button" data-toggle="modal" data-target="#choose_folder" type="button" class="btn btn-primary">Choose Folder</button>
			</span>
		</div>
	</div>

	<div style="margin-top: 10px" id="note_content_div">
		<label for="folder_name" class="control-label">Note content</label>
		{!! Form::textarea('content', $content, ['class' => 'form-control', 'id' => 'note_content', 'style' => 'resize: vertical']) !!}
        <span class="help-block">
            <strong id="note-content-error"></strong>
        </span>
    </div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
	@if($action == 'edit')
		<button type="button" id="btn_edit_note" data-id="{{$note->id}}" class="btn btn-primary">Edit</button>
	@else
		<button type="button" id="btn_add_note" class="btn btn-primary">Save</button>
	@endif
</div>
@if($action == 'edit')
	{!! Form::close() !!}
@endif