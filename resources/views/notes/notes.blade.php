@if(count($group->notes) > 0)
	<h4 style="margin-top: 0">Notes:</h4>
@endif
@foreach($group->notes as $note)
	<div style="margin-bottom: 15px" class="col-md-2" id="note_div{{$note->id}}">
		<a><img data-toggle="modal" data-target="#note_modal{{$note->id}}" style="height: 80px; margin: 5px; margin-left: 0" src="/assets/images/txt.png"></a>
		<br>
		<span>{{$note->name}}</span>
	</div>

	<!-- Modal -->
		<div id="note_modal{{$note->id}}" class="modal fade in" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
					</div>
					<div class="modal-body">
						<embed src="/assets/notes/{{$note->path}}" width="560px" height="550px">
					</div>
					<div class="modal-footer">
						<button class="btn btn-link" id="edit_note_btn" data-id="{{$note->id}}">edit</button>
						<button class="btn btn-link" id="delete_note_btn" data-id="{{$note->id}}">delete</button>
					</div>
				</div>
			</div>
		</div>
	<!-- /Modal -->
@endforeach