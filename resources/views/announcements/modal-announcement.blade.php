<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	@if($action == 'edit')
		<h4 class="modal-title">Edit announcement</h4>
	@else
		<h4 class="modal-title">Create announcement</h4>
	@endif
</div>
@if($action == 'edit')
	{!! Form::model($announcement, ['class' => 'form-horizontal']) !!}
@endif
	<div class="modal-body">
		<div id="title_div">
			<label for="title" class="control-label">Title</label>
			{!! Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) !!}
            <span class="help-block">
                <strong id="title-error"></strong>
            </span>
        </div>

        <div id="date_div">
			<label for="date" class="control-label">Announcement date</label>
			<div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar3"></i></span>
				{!! Form::text('date', null, ['class' => 'form-control pickadate', 'id' => 'date', 'placeholder' => 'Select date']) !!}
            </div>
            <span class="help-block">
                <strong id="date-error"></strong>
            </span>
        </div>

        <div id="time_div">
            <label for="time" class="control-label">Announcement time</label>
            <div class="input-group">
				<span class="input-group-addon"><i class="icon-alarm"></i></span>
				{!! Form::text('time', null, ['class' => 'form-control pickatime', 'id' => 'time', 'placeholder' => 'Select time']) !!}
			</div>
			<span class="help-block">
				<strong id="time-error"></strong>
			</span>
		</div>

		<div id="users_div">
			<label for="users" class="control-label">Invite user</label>
			{!! Form::select('user_id', $groupUsers, null, ['class' => 'select-multiple-drag users', 'multiple' => 'multiple']) !!}
			<input id = "edit_users" type="hidden" name="announcement_users">

			<span class="help-block">
				<strong id="users-error"></strong>
			</span>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
		@if($action == 'edit')
			<button type="button" id="btn_edit_announcement" data-id='{{$announcement->id}}' class="btn btn-primary">Edit</button>
		@else
			<button type="button" id="btn_add_announcement" class="btn btn-primary">Save</button>
		@endif
	</div>
@if(!$action == 'edit')
	{!! Form::close() !!}
@endif