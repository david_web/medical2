@foreach($announcements as $announcement)
	<div style="margin-left: 30px" id="announcement_div{{$announcement->id}}">
		<h4>{{$announcement->title}}</h4>
		<span style="font-size: 15px">Date: {{$announcement->date}}</span><br>
		<span style="font-size: 15px">Time: {{$announcement->time}}</span><br>
		@if (Auth::user()->can('delete', $announcement) || Auth::user()->can('update', $groupData))
			<a id="edit_announcement" data-id="{{$announcement->id}}">edit | </a>
			<a class="delete_announcement" data-id="{{$announcement->id}}">delete</a>
		@else
			<span class="text-muted">edit | delete</span>
		@endif
	</div><hr id="hr{{$announcement->id}}">
@endforeach