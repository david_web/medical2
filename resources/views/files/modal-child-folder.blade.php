<span class="modal_folder_route"><a id="modal_my_library">library/</a></span>
<p id="selected_child_folder"></p>
@foreach($foldersData as $folderData)
	@foreach($folderData->parent_folders as $child)
		<div style="float: left;">
			<a class="modal_folder" data-id="{{$child->id}}" data-name="{{$child->name}}">
				<img class="folder_img" id="folder_img{{$child->id}}" style="height: 70px; margin-right: 15px" src="/assets/images/folder.png">
			</a><br>
			<span style="font-size: 12px">{{$child->name}}</span>
		</div>
	@endforeach
@endforeach