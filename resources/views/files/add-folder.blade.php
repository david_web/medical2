<div id="folder_div{{$folder->id}}">
	<div style="width: 140px; position: relative; float: left;" >
		<a class="folder" data-id="{{$folder->id}}" data-name="{{$folder->name}}">
			<img style="height: 120px;" src="/assets/images/folder.png">
			<button class="close delete_folder" data-toggle="modal" data-target="#delete_folder_modal{{$folder->id}}" id="delete_folder{{$folder->id}}" style="position: absolute; right: 5px; top: 0; display: none;" type="button">×</button>
		</a>
		<span style="position: absolute; left: 5px; bottom: 0">{{$folder->name}}</span>
	</div>
	<div style="height: 120px; padding-left: 150px">
		<div style="margin-bottom: 80px;"><strong>{{$folder->name}}</strong></div>
		<div><span class="help-block">Contains 0 items</span></div>
	</div>
</div>
<br id="br{{$folder->id}}">

<!-- Modal -->
	<div id="delete_folder_modal{{$folder->id}}" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">You shure?</h4>
				</div>
				<div class="modal-body text-center">
					<button type="button" class="btn btn-primary delete_folder_button" data-id='{{$folder->id}}' data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>
<!-- /Modal -->