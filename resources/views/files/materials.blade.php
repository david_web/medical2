@extends('layouts.layout')

@section('content')

<div id="files_div">
	@foreach ($filesData as $fileData)
		<div class="col-md-12" id="file_div{{$fileData->id}}" style="padding: 0">
			<div class="panel panel-flat col-md-12">
				<div class="col-md-1" >
				   <a><img data-toggle="modal" data-target="#modal{{$fileData->id}}" style="height: 120px; margin: 10px; margin-left: 0" src="/assets/images/{{$fileData->type}}.png"></a>
				</div>
				<div class="col-md-1">
				    <div style="margin-top: 15px; text-align: center;">
				        <div>
				            <span style="font-size: 30px">{{$fileData->filesCount}}</span>
				            <div><span style="font-size: 14px">files</span></div>
				        </div><br>
					    <strong>210 views</strong>
				    </div>
				</div>
				<div class="col-md-10">
					<div class="panel-body">
						<span>{!!$fileData->description!!}</span>
					</div>
					<div class="panel-footer" style="border:none; background-color: white;">
						@if(Auth::user()->can('update', $fileData))
							<div class="col-md-6" style="position:relative; margin-top: 25px;">
								<span>share with friends |</span>
								<span>share with group</span><br>
								<a  class="edit_file" data-id='{{$fileData->id}}'>edit |</a><a data-toggle="modal" data-target="#delete_file_modal{{$fileData->id}}"> delete</a>
							</div>
						@else
							<div class="col-md-6" style="position:relative; margin-top: 40px;">
								<span>share with friends |</span>
								<span>share with group</span>
							</div>
						@endif

						<div class="col-md-6 text-right" style="margin-bottom: 10px;">
							<time class="text-muted" id="{{$fileData->id}}" datetime='{{$fileData->created_at}}'>posted Oct 20 '08 at 13:20</time>
							<div>
								@if(!$fileData->user->path)
									<img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
								@else
									<img style="height: 40px; width: 40px" src="/assets/uploads/{{$fileData->user->path}}" alt="">
								@endif	
								<a href="{{action('UserController@getUserPage', $fileData->user->id)}}" style="margin-left: 10px">{{$fileData->user->first_name}} {{$fileData->user->last_name}}</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6" style="padding-left: 0px; margin: 15px;">
				<div style="border-bottom: 1px solid #DDDDDD">
					<table class="table table-xxs comments{{$fileData->id}}">
						@foreach($fileData->comments as $comment)
							<tr id="com{{$comment->id}}">
								<td style="font-size: 13px">
									{{$comment->comment}} –&nbsp; <a href="{{action('UserController@getUserPage', $comment->user->id)}}">{{$comment->user->first_name}} {{ $comment->user->last_name}}</a> <span class="text-muted">Apr 13 at 14:51</span><br>
									@if ($comment->user_id == Auth::id())
										<a href="{{action('CommentController@getEditComment', $comment->id)}}">edit</a>
										<a class="del_comment" data-id="{{$comment->id}}">delete</a>
									@endif	
								</td>
							</tr>
						@endforeach
					</table>
				</div>
				
				<div style="margin-top: 10px">
					<a id="comment_{{$fileData->id}}" class="add_comment1" data-id="{{$fileData->id}}">add a comment</a>
					<div id="comment{{$fileData->id}}" style="display: none; margin-top: 25px">
							{!! Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Write comment...', 'style' => 'resize: vertical', 'id' => 'comment-'.$fileData->id]) !!}
							<button style="margin-top: 10px" data-id="{{$fileData->id}}" class="btn btn-primary add_comment">Comment</button>
					</div>
				</div>
			</div>

			<!-- Modal-->
				<div id="modal{{$fileData->id}}" class="modal fade in" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
							</div>
							<div class="modal-body">
								@if($fileData->type == 'mp4')
									<video width="860" controls>
										<source src="/assets/uploads/{{$fileData->path}}">
									</video>
								@else
									<embed src="/assets/uploads/{{$fileData->path}}" width="860" height="800" />
								@endif	
							</div>
						</div>
					</div>
				</div>

				<div id="delete_file_modal{{$fileData->id}}" class="modal fade in" style="margin-top: 10%">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class=" modal-title">You shure?</h4>
							</div>
							<div class="modal-body text-center">
								<button type="button" class="btn btn-primary delete_file_button" data-id='{{$fileData->id}}' data-dismiss="modal">Yes</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
							</div>
						</div>
					</div>
				</div>
			<!-- /Modal -->
		</div>
	@endforeach
</div>

<!-- Modal-->
	<div id="edit_post" class="modal fade in">
		<div class="modal-dialog modal-lg">
			<div id="post_modal_content" class="modal-content">

			</div>
		</div>
	</div>
<!-- /Modal -->

@stop

@section('javascript')

	{!! HTML::script('assets/js/main/materials.js') !!}
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}

@stop