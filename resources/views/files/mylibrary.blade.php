@extends('users.user-layout')

@section('user-content')

<div class="container-detached">
    <div class="content-detached">
		<button id="button_add_folder" data-toggle="modal" data-target="#add_folder" class="btn btn-primary">Add folder</button>
		<div class="col-md-12" style="margin: 10px" id="folder_route_div">
			<span style="font-size: 20px;"><a>library/</a></span>
		</div>
		<div class="col-md-12" id="folder">
			<div class="col-md-4" id="folder1">
				<div>
					<div style="width: 140px; position: relative; float: left;" >
						<img style="height: 120px; border: 3px solid #2196f1" src="/assets/images/folder.png">
						<span style="position: absolute; left: 5px; bottom: 0">All files</span>
					</div>
					<div style="height: 120px; padding-left: 150px">
						<div style="margin-bottom: 80px;"><strong>All files</strong></div>
						<div><span class="help-block">Contains {{count($filesData)}} items</span></div>
					</div>
				</div><br>
				@foreach($foldersData as $folderData)
					<div id="folder_div{{$folderData->id}}">
						<div style="width: 140px; position: relative; float: left;" >
							<a class="folder" data-id="{{$folderData->id}}" data-name="{{$folderData->name}}">
								<img style="height: 120px;" src="/assets/images/folder.png">
								<button class="close delete_folder" data-toggle="modal" data-target="#delete_folder_modal{{$folderData->id}}" id="delete_folder{{$folderData->id}}" style="position: absolute; right: 5px; top: 0; display: none;" type="button">×</button>
							</a>
							<span style="position: absolute; left: 5px; bottom: 0">{{$folderData->name}}</span>
						</div>
						<div style="height: 120px; padding-left: 150px">
							<div style="margin-bottom: 80px;"><strong>{{$folderData->name}}</strong></div>
							<div><span class="help-block">Contains {{$folderData->fileCount}} items</span></div>
						</div>
					</div>
					<br id="br{{$folderData->id}}">

					<!-- Modal -->
						<div id="delete_folder_modal{{$folderData->id}}" class="modal fade in" style="margin-top: 10%">
							<div class="modal-dialog modal-xs">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class=" modal-title">You shure?</h4>
									</div>
									<div class="modal-body text-center">
										<button type="button" class="btn btn-primary delete_folder_button" data-id='{{$folderData->id}}' data-dismiss="modal">Yes</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
									</div>
								</div>
							</div>
						</div>
					<!-- /Modal -->
				@endforeach
			</div>
			<div class="col-md-8" id="files_div">
				@foreach($filesData as $fileData)
					<div id="file_div{{$fileData->id}}" class="panel panel-flat col-md-12" style="height: 120px">
						<div class="col-md-2" style="padding-left: 0">
						   <a><img data-toggle="modal" data-target="#modal{{$fileData->id}}" style="height: 105px; margin: 5px; margin-left: 0" src="/assets/images/{{$fileData->type}}.png"></a>
						</div>
						<div class="col-md-1" >
						    <div style="text-align: center;">
						        <div>
						            <span style="font-size: 20px">1</span>
						            <div><span style="font-size: 14px">files</span></div>
						        </div><br>
							    <span>210 views</span>
						    </div>
						</div>
						<div class="col-md-9">
							<div style="padding: 14px">
								<span>{!!$fileData->description!!}</span>
							</div>
							<div class="col-md-12" style="padding-right: 0">
								<div class="col-md-4" style="position:relative; margin-top: 22px;">
									<span>share to</span><br>
									<a class="edit_file" data-id='{{$fileData->id}}'>edit | </a><a data-toggle="modal" data-target="#delete_file_modal{{$fileData->id}}">delete</a>
								</div>
								<div class="col-md-8 text-right" style="margin-bottom: 10px; padding-right: 0">
									<div>
										<time class="text-muted" id="{{$fileData->id}}" datetime='{{$fileData->created_at}}'>posted Oct 20 '08 at 13:20</time>
										<div>
											@if(!$fileData->user->path)
												<img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
											@else
												<img style="height: 40px; width: 40px" src="/assets/uploads/{{$fileData->user->path}}" alt="">
											@endif	
											<a href="{{action('UserController@getUserPage', $fileData->user->id)}}" style="margin-left: 10px">{{$fileData->user->first_name}} {{$fileData->user->last_name}}</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal -->
						<div id="modal{{$fileData->id}}" class="modal fade in" >
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
									</div>
									<div class="modal-body">
										@if($fileData->type == 'mp4')
											<video width="860" controls>
												<source src="/assets/uploads/{{$fileData->path}}">
											</video>
										@else
											<embed src="/assets/uploads/{{$fileData->path}}" width="860" height="800" />
										@endif	
									</div>
								</div>
							</div>
						</div>

						<div id="delete_file_modal{{$fileData->id}}" class="modal fade in" style="margin-top: 10%">
							<div class="modal-dialog modal-xs">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class=" modal-title">You shure?</h4>
									</div>
									<div class="modal-body text-center">
										<button type="button" class="btn btn-primary delete_file_button" data-id='{{$fileData->id}}' data-dismiss="modal">Yes</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
									</div>
								</div>
							</div>
						</div>
					<!-- /Modal -->
				@endforeach
			</div>
		</div>

		<!-- Modal -->
			<div id="add_folder" class="modal fade in" style="margin-top: 10%">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class=" modal-title">Create folder</h4>
						</div>
						<div class="modal-body" id="form">
							<label for="folder_name" class="control-label">Folder name</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'folder_name']) !!}
		                    <span class="help-block">
		                        <strong id="error"></strong>
		                    </span>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<button type="button" id="btn_add_folder" class="btn btn-primary">Save</button>
						</div>
					</div>
				</div>
			</div>

			<div id="add_post" class="modal fade in">
				<div class="modal-dialog modal-lg">
					<div id="post_modal_content" class="modal-content">

					</div>
				</div>
			</div>

			<div id="choose_folder" class="modal fade in" style="margin-top: 10%">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class=" modal-title">Choose folder</h4>
						</div>

						<div id="modal_folder" class="modal-body" style="overflow: auto;">
							<a>library/</a>
							<p id="folder_select">You didn't select any folder</p>
							@foreach($foldersData as $folderData)
								<div style="float: left;">
									<a class="modal_folder" data-id="{{$folderData->id}}" data-name="{{$folderData->name}}">
										<img class="folder_img" id="folder_img{{$folderData->id}}" style="height: 70px; margin-right: 15px;" src="/assets/images/folder.png">
									</a><br>
									<span style="font-size: 12px">{{$folderData->name}}</span>
								</div>
							@endforeach
						</div>

						<div class="modal-footer">
							<button id="save_folder" data-id="" data-name="" type="button" data-dismiss="modal" class="btn btn-primary">Save</button>
						</div>
					</div>
				</div>
			</div>
		<!-- /Modal -->
	</div>
</div>

@stop

@section('user-javascript')

{!! HTML::script('assets/js/main/mylibrary.js') !!}
{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}

@stop