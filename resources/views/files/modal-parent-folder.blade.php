<a>library/</a>
<p id="folder_select">You didn't select any folder</p>
@foreach($foldersData as $folderData)
	<div style="float: left;">
		<a class="modal_folder" data-id="{{$folderData->id}}" data-name="{{$folderData->name}}">
			<img class="folder_img" id="folder_img{{$folderData->id}}" style="height: 70px; margin-right: 15px;" src="/assets/images/folder.png">
		</a><br>
		<span style="font-size: 12px">{{$folderData->name}}</span>
	</div>
@endforeach