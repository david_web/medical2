<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	@if($action == 'edit')
		<h4 class=" modal-title">Edit post</h4>
	@else
		<h4 class=" modal-title">File upload</h4>
	@endif
</div>
<div  class="modal-body">
	@if($action == 'edit')
		{!! Form::model($fileData, ['class' => 'form-horizontal']) !!}
	@else
		{!! Form::open(['action' => ['FileController@postGroupFileUpload', $groupId], 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
	@endif
		<div style="margin-top: 10px">
			<div id="country_div" class="{{ $errors->has('country') ? ' has-error' : '' }}">
				<label for="country" class="control-label">Country</label>
                {!! Form::select('country', ['' => 'Select a country'] + $countries, null, ['class' => 'select-multiple-drag', 'id' => 'country']) !!}
                    
                @if($action == 'add')
		            @if ($errors->has('country'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('country') }}</strong>
		                </span>
		            @endif
		        @else
		        	<span class="help-block">
                        <strong id="country-error"></strong>
                    </span>
		        @endif
			</div>
		</div>

		<div style="margin-top: 10px">
			<div id="institution_div" class="{{ $errors->has('institution_name') ? ' has-error' : '' }}">
				<label for="institution_name" class="control-label">Institution name</label>
				{!! Form::text('institution_name', null, ['class' => 'form-control', 'id' => 'institution_name']) !!}

				@if($action == 'add')
	                @if ($errors->has('institution_name'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('institution_name') }}</strong>
	                    </span>
	                @endif
	            @else
	            	<span class="help-block">
                        <strong id="institution_name-error"></strong>
                    </span>
	            @endif
			</div>
		</div>

		<div style="margin-top: 10px">
			<div id="course_div" class="{{ $errors->has('course') ? ' has-error' : '' }}">
				<label for="course" class="control-label">Course</label>
				{!! Form::text('course', null, ['class' => 'form-control', 'id' => 'course']) !!}

				@if($action == 'add')
	                @if ($errors->has('course'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('course') }}</strong>
	                    </span>
	                @endif
	            @else
	            	<span class="help-block">
                        <strong id="course-error"></strong>
                    </span>
	            @endif
			</div>
		</div>	

		<div style="margin-top: 10px">
			<div id="language_div" class="{{ $errors->has('language') ? ' has-error' : '' }}">
				<label for="language" class="control-label">Language of materials</label>
				{!! Form::text('language', null, ['class' => 'form-control', 'id' => 'language']) !!}

				@if($action == 'add')
	                @if ($errors->has('language'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('language') }}</strong>
	                    </span>
	                @endif
	            @else
	            	<span class="help-block">
                        <strong id="language-error"></strong>
                    </span>
	            @endif
			</div>
		</div>

		<div style="margin-top: 10px">
			<div id="description_div" class="{{ $errors->has('description') ? ' has-error' : '' }}">
				<label for="description" class="control-label">Short description of the material</label>
				{!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}

				@if($action == 'add')
	                @if ($errors->has('description'))
	                    <span class="help-block">
	                        <strong id="description-error">{{ $errors->first('description') }}</strong>
	                    </span>
	                @endif
	            @else
	            	<span class="help-block">
                        <strong id="description-error"></strong>
                    </span>
	            @endif
			</div>
		</div>	

		@if($view == 'group' || $view == 'mylibrary')
			<div style="margin-top: 10px">
				<label for="choose" class="display-block control-label">Choose folder</label>
				<div class="input-group">
					@if($action == 'edit')
						{!! Form::text(null, $name, ['class' => 'form-control', 'id' => 'choose', 'readonly', 'placeholder' => 'No folder choosen', 'data-toggle' => 'modal', 'data-target' => '#choose_folder']) !!}
					@else
						{!! Form::text(null, null, ['class' => 'form-control', 'id' => 'choose', 'readonly', 'placeholder' => 'No folder choosen', 'data-toggle' => 'modal', 'data-target' => '#choose_folder']) !!}
					@endif

					{!! Form::hidden('folder_id', null, ['class' => 'form-control', 'id' => 'hidden_choose', 'readonly', 'placeholder' => 'No folder choosen']) !!}
					<span class="input-group-btn">
						<button id="choose_folder_button" data-toggle="modal" data-target="#choose_folder" type="button" class="btn btn-primary">Choose Folder</button>
					</span>
				</div>
			</div>
		@endif

		@if($action == 'add')
			<div style="margin-top: 10px">
				<div id="file_div" class="{{ $errors->has('file') ? ' has-error' : '' }}">
					<label for="upload" class="display-block control-label">Upload file</label>
					{{Form::file(null, ['class' => 'file-styled', 'id' => 'upload', 'name' => 'file'])}}
	                <span class="help-block">Accepted formats: txt, pdf</span>

	                @if ($errors->has('file'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('file') }}</strong>
	                    </span>
	                @endif
				</div>
			</div>
		@endif

		@if($view == 'materials' || $view == 'mylibrary')
			<div class="checkbox">
				<label id="checkbox">
					<input id="public" type="checkbox" class="styled">
					{!! Form::hidden('is_public', null, ['id' => 'hidden_public']) !!}
					Public
				</label>
			</div>
		@endif

        <div class="text-right">
			@if($action == 'edit')
	    		<button type="button" id="edit_post_button" data-id="{{$fileData->id}}" class="btn btn-primary" style="margin-top: 20px">
	    		Edit post <i class="icon-arrow-right14 position-right"></i></button>
	        @else
	        	<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
	        @endif
        </div>
	{!! Form::close() !!}
</div>

{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}
@if($action == 'add')
	{!! HTML::script('assets/js/plugins/visualization/echarts/echarts.js') !!}
@endif

@if($view == 'materials' || $view == 'mylibrary')
	<script type="text/javascript">
		if($('#hidden_public').val() == 1) {
			$('#public').attr('checked', 'checked');
		}

		$('#checkbox').on('click', function() {
			if ($('#public').is(':checked')) {
				$('#hidden_public').val(1);
			} else {
				$('#hidden_public').val(0);
			}
		})
	</script>
@endif