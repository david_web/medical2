<div class="col-md-4" id="folder1" data-id='{{$folder->id}}'>
	@foreach($foldersData as $folderData)
		@foreach($folderData->parent_folders as $child)
			<div id="folder_div{{$child->id}}">
				<div style="width: 140px; position: relative; float: left;">
					<a class="folder" data-id="{{$child->id}}" data-name="{{$child->name}}">
						<img style="height: 120px;" src="/assets/images/folder.png">
						<button class="close delete_folder" data-toggle="modal" data-target="#delete_folder_modal{{$child->id}}" id="delete_folder{{$child->id}}" style="position: absolute; right: 5px; top: 0; display: none;" type="button">×</button>
					</a>
					<span style="position: absolute; left: 5px; bottom: 0">{{$child->name}}</span>
				</div>
				<div style="height: 120px; padding-left: 150px">
					<div style="margin-bottom: 80px;"><strong>{{$child->name}}</strong></div>
					<div><span class="help-block">Contains {{$child->fileCount}} items</span></div>
				</div>
			</div>
			<br id="br{{$child->id}}">

			<!-- Modal -->
				<div id="delete_folder_modal{{$child->id}}" class="modal fade in" style="margin-top: 10%">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class=" modal-title">You shure?</h4>
							</div>
							<div class="modal-body text-center">
								<button type="button" class="btn btn-primary delete_folder_button" data-id='{{$child->id}}' data-dismiss="modal">Yes</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
							</div>
						</div>
					</div>
				</div>
			<!-- /Modal -->
		@endforeach
	@endforeach
</div>
<div class="col-md-8">
	<div id="files_div" data-type="">
		@foreach($filesData as $fileData)
			<div id="file_div{{$fileData->id}}">
				<div class="panel panel-flat col-md-12" style="height: 120px">
					<div class="col-md-2" style="padding-left: 0">
					   <a><img data-toggle="modal" data-target="#modal{{$fileData->id}}" style="height: 105px; margin: 5px; margin-left: 0" src="/assets/images/{{$fileData->type}}.png"></a>
					</div>
					<div class="col-md-1" >
					    <div style="text-align: center;">
					        <div>
					            <span style="font-size: 20px">1</span>
					            <div><span style="font-size: 14px">files</span></div>
					        </div><br>
						    <span>210 views</span>
					    </div>
					</div>
					<div class="col-md-9">
						<div style="padding: 14px">
							<span>{!!$fileData->description!!}</span>
						</div>
						<div class="col-md-12" style="padding-right: 0">
							@if (Auth::user()->can('update', $groupData) || Auth::user()->can('update', $fileData))
								<div class="col-md-4" style="position:relative; margin-top: 22px;">
									<span>share to</span><br>
									<a class="edit_file" data-id='{{$fileData->id}}'>edit | </a><a data-toggle="modal" data-target="#delete_file_modal{{$fileData->id}}">delete</a>
								</div>
							@else 
								<div class="col-md-4" style="position:relative; margin-top: 40px;">
									<span>share to</span><br>
								</div>
							@endif
							<div class="col-md-8 text-right" style="margin-bottom: 10px; padding-right: 0">
								<div>
									<time class="text-muted" id="{{$fileData->id}}" datetime='{{$fileData->created_at}}'>posted Oct 20 '08 at 13:20</time>
									<div>
										@if(!$fileData->user->path)
											<img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
										@else
											<img style="height: 40px; width: 40px" src="/assets/uploads/{{$fileData->user->path}}" alt="">
										@endif	
										<a href="{{action('UserController@getUserPage', $fileData->user->id)}}" style="margin-left: 10px">{{$fileData->user->first_name}} {{$fileData->user->last_name}}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				@if($groupData != null)
					<div class="col-md-12" style="padding-left: 0px; margin-bottom: 15px;">
						<div style="border-bottom: 1px solid #DDDDDD">
							<table class="table table-xxs comments{{$fileData->id}}">
								@foreach($fileData->comments as $comment)
									<tr id="com{{$comment->id}}">
										<td style="font-size: 13px">
											{{$comment->comment}} –&nbsp; <a href="#">{{$fileData->user->first_name}} {{ $fileData->user->last_name}}</a> <span class="text-muted">Apr 13 at 14:51</span><br>
											@if ($fileData->user_id == Auth::id())
												<a href="{{action('CommentController@getEditComment', $comment->id)}}">edit</a>
												<a class="del_comment" data-id="{{$comment->id}}">delete</a>
											@endif	
										</td>
									</tr>
								@endforeach
							</table>
						</div>
						
						<div style="margin-top: 10px">
							<a id="comment_{{$fileData->id}}" class="add_comment1" data-id="{{$fileData->id}}">add a comment</a>
							<div id="comment{{$fileData->id}}" style="display: none; margin-top: 25px">
									{!! Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Write comment...', 'id' => 'comment-'.$fileData->id]) !!}
									<button style="margin-top: 10px" data-id="{{$fileData->id}}" class="btn btn-primary add_comment">Comment</button>
							</div>
						</div>
					</div>
				@endif
			</div>

			<!-- Modal -->
				<div id="modal{{$fileData->id}}" class="modal fade in" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
							</div>
							<div class="modal-body">
								@if($fileData->type == 'mp4')
									<video width="860" controls>
										<source src="/assets/uploads/{{$fileData->path}}">
									</video>
								@else
									<embed src="/assets/uploads/{{$fileData->path}}" width="860" height="800" />
								@endif	
							</div>
						</div>
					</div>
				</div>

				<div id="delete_file_modal{{$fileData->id}}" class="modal fade in" style="margin-top: 10%">
					<div class="modal-dialog modal-xs">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class=" modal-title">You shure?</h4>
							</div>
							<div class="modal-body text-center">
								<button type="button" class="btn btn-primary delete_file_button" data-id='{{$fileData->id}}' data-dismiss="modal">Yes</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
							</div>
						</div>
					</div>
				</div>
			<!-- /Modal -->
		@endforeach
	</div>

	@if($groupData != null)
		<div id="notes_div">
			@if(count($folder->notes) > 0)
				<h4 style="margin-top: 0">Notes:</h4>
			@endif
			@foreach($folder->notes as $note)
				<div style="margin-bottom: 15px" class="col-md-2" id="note_div{{$note->id}}">
					<a><img data-toggle="modal" data-target="#note_modal{{$note->id}}" style="height: 80px; margin: 5px; margin-left: 0" src="/assets/images/txt.png"></a>
					<br>
					<span>{{$note->name}}</span>
				</div>

				<!-- Modal -->
					<div id="note_modal{{$note->id}}" class="modal fade in" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>
								<div class="modal-body">
									<embed src="/assets/notes/{{$note->path}}" width="560px" height="550px">
								</div>
								<div class="modal-footer">
									<button class="btn btn-link" id="edit_note_btn" data-id="{{$note->id}}">edit</button>
									<button class="btn btn-link" id="delete_note_btn" data-id="{{$note->id}}">delete</button>
								</div>
							</div>
						</div>
					</div>
				<!-- /Modal -->
			@endforeach
		</div>
	@endif
</div>