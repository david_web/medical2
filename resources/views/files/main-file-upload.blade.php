@extends('layouts.layout')

@section('content')

<div class="panel panel-flat">
	<div class="panel-heading">
		<h6 class="panel-title">File upload</h6>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>
	<div class="panel-body">
		{!! Form::open(['action' => 'FileController@postFileUpload', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
			<div>
				<div class="{{ $errors->has('country') ? ' has-error' : '' }}" id="countries_div" style="margin-bottom: 20px">
					<label for="country" class="control-label">Country</label>
                    {!! Form::select('country', ['' => 'Select a country'] + $countries, null, ['class' => 'select-multiple-drag', 'id' => 'country']) !!}
                        
                    @if ($errors->has('country'))
                        <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div>
				<div class="{{ $errors->has('institution_name') ? ' has-error' : '' }}" style="margin-bottom: 20px">
					<label for="institution_name" class="control-label">Institution name</label>
					{!! Form::text('institution_name', null, ['class' => 'form-control', 'id' => 'institution_name']) !!}

                    @if ($errors->has('institution_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('institution_name') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div>
				<div class="{{ $errors->has('course') ? ' has-error' : '' }}" style="margin-bottom: 20px">
					<label for="course" class="control-label">Course</label>
					{!! Form::text('course', null, ['class' => 'form-control', 'id' => 'course']) !!}

                    @if ($errors->has('course'))
                        <span class="help-block">
                            <strong>{{ $errors->first('course') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div>
				<div class="{{ $errors->has('language') ? ' has-error' : '' }}" style="margin-bottom: 20px">
					<label for="language" class="control-label">Language of materials</label>
					{!! Form::text('language', null, ['class' => 'form-control', 'id' => 'language']) !!}

                    @if ($errors->has('language'))
                        <span class="help-block">
                            <strong>{{ $errors->first('language') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div>
				<div class="{{ $errors->has('description') ? ' has-error' : '' }}" style="margin-bottom: 20px">
					<label for="description" class="control-label">Short description of the material</label>
					{!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}

                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
				</div>
			</div>
          
            <div>
				<div class="{{ $errors->has('file') ? ' has-error' : '' }}" id="file_div" style="margin-bottom: 20px">
					<label for="upload" class="display-block control-label">Upload file</label>
					{{Form::file(null, ['class' => 'file-styled', 'id' => 'upload', 'name' => 'file'])}}
                    <span class="help-block">Accepted formats: txt, pdf, jpg, jpeg, png, mp4, m4v, mov, mpg, mpeg, asf, avi</span>

                    @if ($errors->has('file'))
                        <span class="help-block">
                            <strong>{{ $errors->first('file') }}</strong>
                        </span>
                    @endif
				</div>
            </div>

            <div class="text-right">
            	<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
		{!! Form::close() !!}
	</div>
</div>

@stop

@section('javascript')

{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}
{!! HTML::script('assets/js/core/libraries/jasny_bootstrap.min.js') !!}
{!! HTML::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
{!! HTML::script('assets/js/plugins/visualization/echarts/echarts.js') !!}
{!! HTML::script('assets/js/main/main-file-upload.js') !!}

@stop