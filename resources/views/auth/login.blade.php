<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Studio Billing - Login </title>
    <meta name="keywords" content="Login Dancing Studio Billing "/>
    <meta name="description" content="Studio Billing - Login ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

    {!! HTML::style('css/all.css') !!}

</head>

<body class="login-container">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">INFOSHR</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="#">
                    <i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Go to website</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contact admin</span>
                </a>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog3"></i>
                    <span class="visible-xs-inline-block position-right"> Options</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                            <!-- Simple login form -->
                {!! Form::open(['action' => 'AuthController@postLogin', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="panel panel-body login-form">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ Session::get('success') }}
                            </div> 
                        @elseif (Session::has('danger'))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> {{ Session::get('danger') }}
                            </div>             
                        @endif

                        <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                            <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input name="email" type="text" class="form-control" placeholder="Email" value="{{old('email')}}">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>
                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked name="remember">
                                        Remember Me
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="{{ url('password/email') }}">Forgot password?</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{action('AuthController@getRegister')}}">Sign up</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- /simple login form -->


                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

{!! HTML::script('/js/app.js') !!}
{!! HTML::script('/js/config.js') !!}

<script>
    $('document').ready(function () {
        // Default initialization
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });
    });
</script>

</body>
</html>