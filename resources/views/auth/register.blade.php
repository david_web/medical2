<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Medical - Register </title>
    <meta name="keywords" content="Login Dancing Studio Billing "/>
    <meta name="description" content="Studio Billing - Login ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

    {!! HTML::style('css/all.css') !!}

</head>

<body class="register-container">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">INFOSHR</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="#">
                    <i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Go to website</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contact admin</span>
                </a>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog3"></i>
                    <span class="visible-xs-inline-block position-right"> Options</span>
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    {!! Form::open(['action' => 'AuthController@postRegister', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first-name" class="col-md-4 control-label">First name</label>

                            <div class="col-md-6">
                                <input id="first-name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}"  autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('other_name') ? ' has-error' : '' }}">
                            <label for="other-name" class="col-md-4 control-label">Other name</label>

                            <div class="col-md-6">
                                <input id="other-name" type="text" class="form-control" name="other_name" value="{{ old('other_name') }}"  autofocus>

                                @if ($errors->has('other_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('other_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"  autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                            <label for="date_of_birth" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                    <input id="date_of_birth" type="text" name="date_of_birth" class="form-control pickadate-selectors" value="{{ old('date_of_birth') }}">
                                </div>
                
                                @if ($errors->has('date_of_birth'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                            <label for="nationality" class="col-md-4 control-label">Nationality</label>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::select('nationality', $countries, null, ['class' => 'select-multiple-drag', 'id' => 'nationality']) !!}
                                </div>

                                @if ($errors->has('nationality'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('residence') ? ' has-error' : '' }}">
                            <label for="place-of-resid" class="col-md-4 control-label">Place of residence</label>

                            <div class="col-md-6">
                                <input id="place-of-resid" type="text" class="form-control" name="residence" value="{{ old('residence') }}"  autofocus>

                                @if ($errors->has('residence'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('residence') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('profession_id') ? ' has-error' : '' }}">
                            <label for="profession" class="col-md-4 control-label">Profession</label>

                            <div id="profession_div" class="col-md-6">
                                <div class="form-group">
                                    {!! Form::select('profession_id', $allprofessions, null, ['class' => 'select-multiple-drag', 'id' => 'profession']) !!}
                                </div>

                                @if ($errors->has('profession_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profession_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::text('other_profession', null, ['class' => 'form-control', 'id' => 'input1', 'style'=> 'display: none', 'placeholder' => 'Write your profession...']) !!}
                                    </div>
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('place_of_work_or_study') ? ' has-error' : '' }}">
                            <label for="place-of-work-or-study" class="col-md-4 control-label">Place of work or study</label>

                            <div class="col-md-6">
                                <input id="place-of-work-or-study" type="text" class="form-control" name="place_of_work_or_study" value="{{ old('place_of_work_or_study') }}"  autofocus>

                                @if ($errors->has('place_of_work_or_study'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('place_of_work_or_study') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('institution_name') ? ' has-error' : '' }}">
                            <label for="name-of-institution" class="col-md-4 control-label">Name of institution</label>

                            <div class="col-md-6">
                                <input id="name-of-institution" type="text" class="form-control" name="institution_name" value="{{ old('institution_name') }}"  autofocus>

                                @if ($errors->has('institution_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('institution_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                            <label for="mobile-number" class="col-md-4 control-label">Mobile number</label>

                            <div class="col-md-6">
                                <input id="mobile-number" type="text" class="form-control" name="mobile_number" value="{{ old('mobile_number') }}" >

                                @if ($errors->has('mobile_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('whatsapp') ? ' has-error' : '' }}">
                            <label for="whatsapp" class="col-md-4 control-label">Whatsapp</label>

                            <div class="col-md-6">
                                <input id="whatsapp" type="text" class="form-control" name="whatsapp" value="{{ old('whatsapp') }}"  autofocus>

                                @if ($errors->has('whatsapp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('whatsapp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                            <label for="facebook" class="col-md-4 control-label">Facebook</label>

                            <div class="col-md-6">
                                <input id="facebook" type="text" class="form-control" name="facebook" value="{{ old('facebook') }}"  autofocus>

                                @if ($errors->has('facebook'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('vk') ? ' has-error' : '' }}">
                            <label for="vk" class="col-md-4 control-label">Vk</label>

                            <div class="col-md-6">
                                <input id="vk" type="text" class="form-control" name="vk" value="{{ old('vk') }}"  autofocus>

                                @if ($errors->has('vk'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vk') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('viber') ? ' has-error' : '' }}">
                            <label for="viber" class="col-md-4 control-label">Viber</label>

                            <div class="col-md-6">
                                <input id="viber" type="text" class="form-control" name="viber" value="{{ old('viber') }}"  autofocus>

                                @if ($errors->has('viber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('viber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


{!! HTML::script('/js/app.js') !!}
{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/plugins/pickers/anytime.min.js') !!}
{!! HTML::script('assets/js/plugins/ui/moment/moment.min.js') !!}
{!! HTML::script('assets/js/plugins/pickers/daterangepicker.js') !!}
{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}
{!! HTML::script('assets/js/plugins/pickers/pickadate/picker.time.js') !!}
{!! HTML::script('assets/js/pages/picker_date.js') !!}


<script>
    jQuery(document).ready(function() {
        $('#nationality, #profession').select2();

        $('#profession').on('change',function(){
            var optionVar = $('#profession option').filter(':selected').val();
            if (optionVar == '5'){
                $('#profession_div').removeClass("col-md-6").addClass("col-md-2");
                $('#input1').show();
            }
            else {
                $('#profession_div').removeClass("col-md-2").addClass("col-md-6");
                $('#input1').hide();
            }
        });
        $( '#profession' ).trigger( 'change' );
    });
</script>

</body>
</html>
