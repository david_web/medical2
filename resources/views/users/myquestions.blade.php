@extends('users.user-layout')

@section('user-content')

<div class="container-detached">
    <div class="content-detached">

        {!! Form::open(['action' => 'QuestionController@getSearch', 'method' => 'GET']) !!}
            <div class="input-group col-md-3">
                {!! Form::text('search', null, ['placeholder' => 'Serach...', 'class' => 'form-control']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-group glyphicon glyphicon-search" style="height: 20px; top: 3px"></i></button>
                </span>
            </div><br>
        {!! Form::close() !!}


        @if(count($questionsData) == 0)
            <span style="font-size: 20px">Your search returned no matches.</span>
        @else   
            @foreach ($questionsData as $questionData)
                <div class="panel panel-flat col-md-12">
                    <div class="col-md-2" >
                        <div style="margin-top: 25px; text-align: center;">
                            <div>
                                <span style="font-size: 23px">4</span>
                                <div><span style="font-size: 12px">votes</span></div>
                            </div>
                            <div>
                                <span style="font-size: 23px">3</span>
                                <div><span style="font-size: 12px">answers</span></div>
                            </div><br>
                            <strong>210 views</strong>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="panel-heading">
                            <h4 style="margin: 0px;"><a href="{{action('QuestionController@getQuestion', [$questionData->id, $questionData->keyword])}}">{{$questionData->title}}</a></h4>
                        </div>
                        
                        <div class="panel-body">
                            {!!$questionData->description!!}
                        </div>

                        <div class="panel-footer" style="border:none; background-color: white">
                            <div class="heading-elements col-md-7">
                                @foreach ($questionData->tags as $tag)
                                    <span class="heading-text" style="margin-left: 10px; font-size: 13px; background-color: #455A64; color: #FFF; margin-top: 1px; padding-top: 7px; padding-bottom: 7px; padding-left: 14px; padding-right: 14px; border-radius: 3px;" ;>
                                        {{$tag->tag}}
                                    </span>
                                @endforeach
                            </div>
                            <div class="col-md-5 text-right" style="margin-bottom: 10px;">
                                <time class="text-muted" id="{{$questionData->id}}" datetime='{{$questionData->created_at}}'>asked Oct 20 '08 at 13:20</time>
                                <div>
                                    @if(!$questionData->user->path)
                                        <img src="/assets/images/placeholder.jpg" style="height: 40px; width: 40px" alt="">
                                    @else
                                        <img style="height: 40px; width: 40px" src="/assets/uploads/{{$questionData->user->path}}" alt="">
                                    @endif
                                    <a href="{{action('UserController@getUserPage', $questionData->user->id)}}" style="margin-left: 10px">{{$questionData->user->first_name}} {{$questionData->user->last_name}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        <div style="text-align: center;">{{ $questionsData->links() }}</div>
    </div>
</div>
@stop

@section('user-javascript')

<script type="text/javascript">
	jQuery(document).ready(function() {

	});
</script>

@stop