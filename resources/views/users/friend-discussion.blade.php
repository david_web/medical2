<br>
<div>
	<button type="button" class="btn btn-primary btn-sm" id="bootbox_form" data-toggle="modal" data-target="#modal_iconified">
		Create New Discussion
	</button>
</div>
<br>
@foreach($user->discussions as $discussion)
	@if($discussion->pivot->accepted == 1&&$discussion->is_public == 1)
	    <div class="panel-group panel-group-control panel-group-control-right" id="chat-accordion">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title">
						<a data-toggle="collapse" href="#collapsible-james{{$discussion->id}}" data-parent="#chat-accordion">
							<img src="/assets/images/placeholder.jpg" alt="" class="img-circle position-left">
							{{$discussion->name}}
							<span class="status-mark border-success position-right"></span>						
						</a>
					</h6>
				</div>
				<div id="collapsible-james{{$discussion->id}}" class="panel-collapse collapse">
					<div class="panel-body">
						<ul class="media-list chat-list content-group" style=" max-height: 320px;">
							@foreach($discussion->measeges as $measege)								
								@if($measege->user_id == Auth::id())
									<li class="media">
										<div class="media-left">
											<a href="assets/images/placeholder.jpg">
												<img src="{{asset('assets/uploads/'.Auth::user()->path)}}" class="img-circle" alt="">
											</a>
										</div>
										<div class="media-body">
											<div class="media-content">
												{{$measege->measege}}
											</div>
											<span class="media-annotation display-block mt-10">
												{{ $measege->created_at->format('Y-m-d  H:i:s') }}	 
												<a href="#">
													<i class="icon-pin-alt position-right text-muted"></i>
												</a>
											</span>
										</div>
									</li>
								@else	
									<li class="media reversed">
										<div class="media-body">
											<div class="media-content">
												{{$measege->measege}}
											</div>
											<span class="media-annotation display-block mt-10">
												{{ $measege->created_at->format('Y-m-d  H:i:s') }}
												<a href="#">
													<i class="icon-pin-alt position-right text-muted"></i>
												</a>
											</span>
										</div>

										<div class="media-right">
											<a href="assets/images/placeholder.jpg">
												<img src="{{asset('assets/uploads/'.$measege->user->path)}}" class="img-circle" alt="">
											</a>
										</div>    
									</li>
								@endif
							@endforeach	
						</ul>
						{!! Form::open(array('action' => array('DiscussionController@postSendMeasege'))) !!}		
	                    	<textarea name="measege" class="form-control content-group measege_text" rows="3" cols="1" placeholder="Enter your message..."></textarea>
	                    	<input name="user_id" type="hidden" value="{{Auth::id()}}">
	                    	<input name="discussion_id" type="hidden" value="{{$discussion->id}}">
	                		
	                		<div class=" text-right" >
	                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right send_measege"><b><i class="icon-circle-right2"></i></b> Send</button>
	                		</div>
						{!!Form::close()!!}    
						@if(Auth::id() == $discussion->user_id)								
								<div class=" text-left" style="margin-top: 10px;">
	                    				<input name="id" type="hidden" value="{{$discussion->id}}">
			                            <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right send_measege"><b><i class="icon-circle-right2"></i></b><a style="color: white;" href="{{action('DiscussionController@getRemoveDiscussion',$discussion->id)}}"> Delete Discussion</a></button>
			                	</div> 						
			                @endif	
	                	</div>
					</div>
				</div>
		</div>
	@endif
@endforeach	

