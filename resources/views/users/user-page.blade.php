@extends('layouts.layout')

@section('content')

<div class="sidebar-detached">
	<div class="sidebar sidebar-default sidebar-separate">
		<div class="sidebar-content">
			<!-- User details -->
			<div class="content-group">
				<div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain; margin-bottom: 20px">
					<div class="content-group-sm">
						<h6 class="text-semibold no-margin-bottom">
							{{$user->first_name}} {{$user->last_name}}
						</h6>
					</div>

					<div class="display-inline-block">
						@if(!$user->path)
							<img src="/assets/images/placeholder.jpg" class="img-circle img-responsive" alt="" style="height: 200px; width: 200px">
						@else
							<img src="/assets/uploads/{{$user->path}}" class="img-circle img-responsive" alt="" style="height: 200px; width: 200px">
						@endif
					</div><br><br>
					
					<div class="content-group-sm" id="add_friend_div">
						@if($userFriends->contains(Auth::user())) 
							<div class="btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									Your associate <span class="caret"></span>
								</button>
	                            <ul class="dropdown-menu dropdown-menu-right" style="padding: 0;">
									<li><a href="{{action('UserController@getRemoveFriend', $user->id)}}">Remove from associates</a></li>
								</ul>
							</div>
						@elseif($friendRequests->contains(Auth::user()))
							<div class="btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									Request has been sent <span class="caret"></span>
								</button>
	                            <ul class="dropdown-menu dropdown-menu-right" style="padding: 0;">
									<li><a href="{{action('UserController@getRemoveFriend', $user->id)}}">Cancel</a></li>
								</ul>
							</div>
						@elseif($user->followers->contains(Auth::user()))
							<div class="btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									Sent request to you <span class="caret"></span>
								</button>
	                            <ul class="dropdown-menu dropdown-menu-right" style="padding: 0;">
									<li><a href="{{action('UserController@getAcceptRequest', $user->id)}}">Accept</a></li>
									<li><a href="{{action('UserController@getRejectRequest', $user->id)}}">Reject</a></li>
								</ul>
							</div>
						@else
							<button type="button" class="btn btn-primary" id="add_friend_button" data-id="{{$user->id}}">Add as associate</button><br>
						@endif
					</div>
				</div>

				<div class="sidebar-category">
					<div class="category-title">
						<span>{{$user->first_name}} associates</span>
						<ul class="icons-list">
							<li><a href="#" data-action="collapse" class=""></a></li>
						</ul>
					</div>

					<div class="category-content" style="display: block; padding: 0;">
						@if (Auth::user()->can('view', $user))
							@if(count($userFriends) == 0)
								<div style="padding: 20px;">
									<span>{{$user->first_name}} have no associates yet</span>
								</div>
							@else
								<ul class="media-list media-list-linked">
									@foreach($userFriends as $friend)
										<li class="media">
											<a href="{{action('UserController@getUserPage', $friend->id)}}" class="media-link">
												<div class="media-left">
													@if(!$friend->path)
														<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
													@else
														<img src="/assets/uploads/{{$friend->path}}" class="img-circle" alt="">
													@endif
												</div>
												<div class="media-body media-middle">
													<div class="media-heading text-semibold">{{$friend->first_name}} {{$friend->last_name}}</div>
												</div>
											</a>
										</li>
									@endforeach
								</ul>
							@endif
						@else
							<div style="padding: 20px;">
								<span>You can`t view {{$user->first_name}} associates</span>
							</div>
						@endif
					</div>
				</div>

				<div class="sidebar-category">
					<div class="category-title">
						<span>{{$user->first_name}} Societies</span>
						<ul class="icons-list">
							<li><a href="#" data-action="collapse" class=""></a></li>
						</ul>
					</div>

					<div class="category-content" style="display: block;">
						@if (Auth::user()->can('view', $user))
							@if(count($groups) == 0)
								<span>{{$user->first_name}} have no societies</span>
							@else
								<ul class="media-list">
									@foreach($groups as $group)
										<li class="media">
											<a class="media-left">
												@if(!$group->path)
													<img src="/assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
												@else
													<img src="/assets/uploads/{{$group->path}}" class="img-sm img-circle" alt="">
												@endif
											</a>
											<div class="media-body">
												@if (Auth::user()->can('view', $group))
													<a href="{{action('GroupController@getGroup', $group->id)}}" class="media-heading text-semibold">{{$group->name}}</a>
												@else
													<span class="media-heading text-semibold">{{$group->name}}</span>
												@endif
												<span class="text-size-mini text-muted display-block">{{$group->users_count}}
													@if ($group->users_count == 1)
														member
													@else 
														members
													@endif
												</span>
											</div>
										</li>
									@endforeach
								</ul>
							@endif
						@else
							<span>You can`t view {{$user->first_name}} societies</span>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-detached">
	<div class="content-detached">
		<div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title text-semibold">{{$user->first_name}} {{$user->last_name}}</h6>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>
			<div class="panel-body">
				@if (Auth::user()->can('view', $user))
					<div class="col-md-6">
						<p><strong>Date of Birth:</strong> {{$user->date_of_birth}}</p>
						<p><strong>Country:</strong> {{$user->nationality}}</p>
						<p><strong>Place of residence:</strong> {{$user->residence}}</p>
						<p><strong>Profession:</strong> {{$user->other_profession}}</p>
						<p><strong>Place of work or study:</strong> {{$user->place_of_work_or_study}}</p>
					</div>
					<div class="col-md-6">
						<p><strong>Name of institution:</strong> {{$user->institution_name}}</p>
						<p><strong>E-Mail Address:</strong> {{$user->email}}</p>
						<p><strong>Mobile number:</strong> {{$user->mobile_number}}</p>
						<p><strong>Facebook:</strong> {{$user->facebook}}</p>
						<p><strong>Vk:</strong> {{$user->vk}}</p>
					</div>
				@else
					<span>You can`t view {{$user->first_name}} information</span>
				@endif
			</div>
        </div>

		<div class="panel panel-white">
			<div class="panel-heading">
				<!-- <h6 class="panel-title text-semibold">{{$user->first_name}} library</h6> -->
				 <ul class="header">
				    <li class=" li" id="friend_library" data-id="{{$user->id}}"><a>{{$user->first_name}} library</a></li>
				    <li class="li" id="friend_discussion" data-id="{{$user->id}}"><a>Discussions</a></li>
				    <li class="li"><a>Followed</a></li>
				    <li class="li"><a>Following</a></li>
				    <li class="li" data-target="#modal_iconified1" data-toggle="modal"><a >Ask Me Question</a></li>
				  </ul>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>
            <!-- library content -->
			@if (Auth::user()->can('view', $user))
	            <div class="tab-content">
	            	<div id="friend_library_discussions" class="tab-pane fade in active">
						<table class="table table-striped media-library table-lg">
			                <thead>
			                    <tr>
			                    	<th><input type="checkbox" class="styled"></th>
			                        <th>Preview</th>
			                        <th>Name</th>
			                        <th>Author</th>
			                        <th>Date</th>
			                        <th>File info</th>
			                        <th class="text-center">Actions</th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($files as $file)
				                    <tr>
				                    	<td><input type="checkbox" class="styled"></td>
				                        <td>
					                        <a>
						                        <img data-toggle="modal" data-target="#modal{{$file->id}}" style="height: 70px;" src="/assets/images/{{$file->type}}.png">
					                        </a>
				                        </td>
				                        <td>{{$file->description}}</td>
				                        <td>{{$user->first_name}} {{$user->last_name}}</td>
				                        <td>{{$file->created_at}}</td>
				                        <td>
				                        	<ul class="list-condensed list-unstyled no-margin">
					                        	<li><span class="text-semibold">Format:</span> .{{$file->type}}</li>
				                        	</ul>
				                        </td>
				                        <td class="text-center">
				                            <ul class="icons-list">
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-menu9"></i>
													</a>

													<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
														<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
														<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
														<li class="divider"></li>
														<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
													</ul>
												</li>
											</ul>
				                        </td>
				                    </tr>										         
					                <!-- Modal -->
										<div id="modal{{$file->id}}" class="modal fade in" >
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">×</button>
													</div>
													<div class="modal-body">
														@if($file->type == 'mp4')
															<video width="860" controls>
																<source src="/assets/uploads/{{$file->path}}">
															</video>
														@else
															<embed src="/assets/uploads/{{$file->path}}" width="860" height="800" />
														@endif	
													</div>
												</div>
											</div>
										</div>
									<!-- /Modal -->
			                    @endforeach
			                </tbody>
			            </table>
		       		</div>
		        </div>   
	        @else
	        	<div style="padding: 20px;">
	        		<span>You can`t view {{$user->first_name}} library</span>
	        	</div>
	        @endif
        </div>
	</div>
</div>

<!-- Modal discussion -->
	<div id="modal_iconified" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title"><i class="icon-menu7"></i> Creat New Discussion</h5>
				</div>
				{!! Form::open(array('action' => array('DiscussionController@postCreateDiscussion', $user->id,Auth::user()->id))) !!}

					<div class="modal-body">
						<label>Topic of discussion </label>
						<input type="text" class="form-control" name="name">
						<div class="form-group">
							<label class="checkbox-inline">
								<input type="checkbox" class="styled" name="public">
										Public
						   </label>
						</div>							
					</div>
					<div class="modal-footer">
						<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
						<button class="btn btn-primary" type="submit" data-id="{{$user->id}}" data="{{Auth::user()->id}}" id="create_discussion"><i class="icon-check"></i> Create</button>
					</div>
				{!!  Form::close() !!}
			</div>
		</div>
	</div>
<!-- /Modal -->
<!-- Modal question -->
		    <div id="modal_iconified1" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title"><i class="icon-menu7"></i> Ask Question</h5>
					</div>
					<div class="modal-body">
						<label>Topic of question </label>				
						<input type="text" class="form-control" name="discussion_topic">
					<div class="form-group">	
						<label class="checkbox-inline">
							<input type="checkbox" class="styled">
									Public
						</label>
					</div>
							<textarea class="form-control comment" placeholder="Ask Question..." name="question" cols="50" rows="10"></textarea>
					</div>
					<div class="modal-footer">
						<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
						<button class="btn btn-primary"><i class="icon-check"></i> Ask</button>
					</div>
				</div>
			</div>
		</div>
<!-- /Modal -->

@stop

@section('javascript')

	{!! HTML::script('assets/js/pages/gallery_library.js') !!}
	{!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js') !!}
	{!! HTML::script('assets/js/plugins/media/fancybox.min.js') !!}
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
	{!! HTML::script('assets/js/main/user-page.js') !!}

@endsection