@extends('layouts.layout')

@section('content')

<!-- Detached sidebar -->
<div class="sidebar-detached">
	<div class="sidebar sidebar-default sidebar-separate">
		<div class="sidebar-content">

			<div class="content-group">
				<div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<div class="content-group-sm">
						<h6 class="text-semibold no-margin-bottom">
							{{Auth::user()->first_name}} {{Auth::user()->last_name}}
						</h6>
					</div>

					<a data-toggle="modal" data-target="#upload_image" class="display-inline-block">
						@if(!Auth::user()->path)
							<img src="/assets/images/placeholder.jpg" class="img-circle img-responsive" alt="" style="height: 200px; width: 200px">
						@else
							<img src="/assets/uploads/{{Auth::user()->path}}" class="img-circle img-responsive" alt="" style="height: 200px; width: 200px">
						@endif
					</a>
				</div>

				<div class="panel no-border-top no-border-radius-top">
					<ul class="navigation navigation-accordion navigation-main">
						<li class="navigation-header">Navigation</li>
						<li><a href="{{action('UserController@getMyProfile')}}" class="list-group-item"><i class="icon-user"></i> My profile</a></li>
						<li><a href="{{action('UserController@getMyFriends')}}" class="list-group-item"><i class="icon-users"></i> My associates 
							@if(count($friendRequests) > 0)
								({{count($friendRequests)}})
							@endif
						</a></li>
						<li><a href="{{action('QuestionController@getMyQuestions')}}"><i class="fa fa-group glyphicon glyphicon-question-sign"></i> My questions</a></li>
						<li><a href="{{action('FileController@getMyLibrary')}}"><i class="icon-files-empty"></i> My library</a></li>
						<li><a href="{{action('FileController@getUserFileUpload')}}"><i class="icon-files-empty"></i> File upload</a></li>
						<li><a href="{{action('UserController@getProfileInformation')}}"><i class="icon-cog3"></i></i> Profile information</a></li>
						<li class="navigation-divider"></li>
						<li><a href="{{action('AuthController@getLogout')}}"><i class="icon-switch2"></i> Log out</a></li>
					</ul>
				</div>
			</div>

			@yield('user-sidebar-content')
			
		</div>
	</div>
</div>
<!-- /detached sidebar -->

<!-- Modal-->
	<div id="upload_image" class="modal fade in" style="margin-top: 10%">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class=" modal-title">Upload profile image</h4>
				</div>
				<div class="modal-body">
					{!! Form::open(['action' => 'UserController@postImageUpload', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
						<div id="image_div" class="{{ $errors->has('image') ? ' has-error' : '' }}">
							<label for="upload" class="control-label">Upload image</label>
							{{Form::file('image', ['class' => 'file-styled', 'id' => 'upload'])}}
	                        <span class="help-block">Accepted formats: gif, png, jpg.</span>
	                        <i class="icon-trash position-left"></i><a href="{{action('UserController@getDeleteImage')}}">delete image</a>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
<!-- /Modal -->

@yield('user-content')

@stop


@section('javascript')
	
	@yield('user-javascript')

	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
	{!! HTML::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
	{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}
	{!! HTML::script('assets/js/core/libraries/jasny_bootstrap.min.js') !!}
	{!! HTML::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
	{!! HTML::script('assets/js/plugins/visualization/echarts/echarts.js') !!}

@endsection