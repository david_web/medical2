@extends('users.user-layout')

@section('user-content')

<div class="container-detached">
    <div class="content-detached">

    	<div class="panel panel-flat col-md-12">
			<div class="panel-heading">
				<h3 class="panel-title" style="color: black">
					<a style="margin-right: 60px" id="my_friends">
						My associates
						@if (count($userFriends) > 0)
							<span style="color: grey; font-size: 18px; padding-left: 10px">{{count($userFriends)}}</span>
						@endif
					</a>

					<a id="friend_requests">
						Associate requests 
						@if (count($friendRequests) > 0)
							<span style="color: grey; font-size: 18px; padding-left: 10px">{{count($friendRequests)}}</span>
						@endif
					</a>
				</h3>
			</div><hr style="margin-top: 0">

			<div class="panel-body" id="friends_div">
				@if (count($userFriends) == 0)
					<span style="font-size: 18px">You have no associates yet</span>
				@else
					<ul class="media-list">
						@foreach($userFriends as $user)
							<li class="media">
								<div class="media-left media-middle">
									@if(!$user->path)
										<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
									@else
										<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
									@endif
								</div>

								<div class="media-body media-middle">
									<div class="media-heading text-semibold"><a href="{{action('UserController@getUserPage', $user->id)}}">{{$user->first_name}} {{$user->last_name}}</a></div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list text-nowrap">
				                    	<li class="dropdown">
				                    		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>

				                    		<ul class="dropdown-menu dropdown-menu-right">
						                    	<li><a href="{{action('UserController@getRemoveFriend', $user->id)}}" data-toggle="modal">Remove from associates</a></li>
				                    		</ul>
				                    	</li>
				                	</ul>
								</div>
							</li>
						@endforeach
					</ul>
				@endif
			</div>
		</div>

	</div>
</div>

@stop

@section('user-javascript')

	{!! HTML::script('assets/js/main/my-friends.js') !!}

@stop