@extends('users.user-layout')

@section('user-content')


<!-- Detached content -->
<div class="container-detached">
	<div class="content-detached">

		<!-- Tab content -->
		<div class="tab-content">
			<div class="tab-pane fade in active" id="profile">

				<!-- Profile info -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Profile information</h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-body">
						{!! Form::model($user, ['action' => 'UserController@postEditUserInformation', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
							<div class="form-group">
								<div class="row">
									<div class="col-md-4{{ $errors->has('first_name') ? ' has-error' : '' }}">
										<label for="first-name" class="control-label">First name</label>
										{!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first-name']) !!}
		                                @if ($errors->has('first_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('first_name') }}</strong>
		                                    </span>
		                                @endif
									</div>
									<div class="col-md-4{{ $errors->has('other_name') ? ' has-error' : '' }}">
										<label for="other-name" class="control-label">Other name</label>
										{!! Form::text('other_name', null, ['class' => 'form-control', 'id' => 'other-name']) !!}
		                                @if ($errors->has('other_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('other_name') }}</strong>
		                                    </span>
		                                @endif
									</div>
									<div class="col-md-4{{ $errors->has('last_name') ? ' has-error' : '' }}">
										<label for="last-name" class="control-label">Last name</label>
										{!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last-name']) !!}
		                                @if ($errors->has('last_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('last_name') }}</strong>
		                                    </span>
		                                @endif
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-6{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
										<label for="date_of_birth" class="control-label">Date of Birth</label>
		                                <div class="input-group">
		                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
		                                    {!! Form::text('date_of_birth', null, ['class' => 'form-control', 'id' => 'date_of_birth']) !!}
		                                </div>
		                
		                                @if ($errors->has('date_of_birth'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
		                                    </span>
		                                @endif
									</div>
									<div class="col-md-6{{ $errors->has('nationality') ? ' has-error' : '' }}">
										<label for="nationality" class="control-label">Nationality</label>
		                                <div class="form-group">
		                                	{!! Form::select('nationality', $countries, null, ['class' => 'select-multiple-drag', 'id' => 'country']) !!}
		                                    @if ($errors->has('nationality'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('nationality') }}</strong>
			                                    </span>
			                                @endif
		                                </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-6{{ $errors->has('residence') ? ' has-error' : '' }}">
										<label for="place-of-resid" class="control-label">Place of residence</label>
										{!! Form::text('residence', null, ['class' => 'form-control', 'id' => 'place-of-resid']) !!}
		                                @if ($errors->has('residence'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('residence') }}</strong>
		                                    </span>
		                                @endif
									</div>
									<div id="profession_div" class="col-md-6 {{ $errors->has('profession_id') ? ' has-error' : '' }}">
										<label for="profession" class="control-label">Profession</label>
										<div class="form-group">
											{!! Form::select('profession_id', $allprofessions, null, ['class' => 'select-multiple-drag', 'id' => 'profession']) !!}
		         		                </div>

		                                @if ($errors->has('profession_id'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('profession_id') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<label id="hidden_label" style="display: none;" for="input1" class="control-label">Write your profession</label>
											{!! Form::text('other_profession', null, ['class' => 'form-control', 'id' => 'input1', 'style'=> 'display: none']) !!}
		         		                </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-md-6{{ $errors->has('place_of_work_or_study') ? ' has-error' : '' }}">
										<label for="place-of-work-or-study" class="control-label">Place of work or study</label>
										{!! Form::text('place_of_work_or_study', null, ['class' => 'form-control', 'id' => 'place-of-work-or-study']) !!}
		                                @if ($errors->has('place_of_work_or_study'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('place_of_work_or_study') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-6{{ $errors->has('institution_name') ? ' has-error' : '' }}">
										<label for="name-of-institution" class="control-label">Name of institution</label>
										{!! Form::text('institution_name', null, ['class' => 'form-control', 'id' => 'name-of-institution']) !!}

		                                @if ($errors->has('institution_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('institution_name') }}</strong>
		                                    </span>
		                                @endif
									</div>
								</div>
							</div>

	                        <div class="form-group">
		                        <div class="row">
									<div class="col-md-6{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
										<label for="mobile-number" class="control-label">Mobile number</label>
										{!! Form::text('mobile_number', null, ['class' => 'form-control', 'id' => 'mobile-number']) !!}
		                                @if ($errors->has('mobile_number'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('mobile_number') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-6{{ $errors->has('whatsapp') ? ' has-error' : '' }}">
										<label for="whatsapp" class="control-label">Whatsapp</label>
										{!! Form::text('whatsapp', null, ['class' => 'form-control', 'id' => 'whatsapp']) !!}
		                                @if ($errors->has('whatsapp'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('whatsapp') }}</strong>
		                                    </span>
		                                @endif
									</div>
								</div>
	                        </div>

	                        <div class="form-group">
		                        <div class="row">
									<div class="col-md-6{{ $errors->has('facebook') ? ' has-error' : '' }}">
										<label for="facebook" class="control-label">Facebook</label>
										{!! Form::text('facebook', null, ['class' => 'form-control', 'id' => 'facebook']) !!}
		                                @if ($errors->has('facebook'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('facebook') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-6{{ $errors->has('vk') ? ' has-error' : '' }}">
										<label for="vk" class="control-label">Vk</label>
										{!! Form::text('vk', null, ['class' => 'form-control', 'id' => 'vk']) !!}
		                                @if ($errors->has('vk'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('vk') }}</strong>
		                                    </span>
		                                @endif
									</div>
								</div>
	                        </div>
	                        
	                        <div class="form-group">
		                        <div class="row">
		                        	<div class="col-md-6{{ $errors->has('viber') ? ' has-error' : '' }}">
										<label for="viber" class="control-label">Viber</label>
										{!! Form::text('viber', null, ['class' => 'form-control', 'id' => 'viber']) !!}
		                                @if ($errors->has('viber'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('viber') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-6{{ $errors->has('email') ? ' has-error' : '' }}">
										<label for="email" class="control-label">E-Mail Address</label>
										{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
									</div>
								</div>
	                        </div>

	                        <div class="text-right">
	                        	<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
	                        </div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /profile info -->

				<!-- Account settings -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Account settings</h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

                    
					<div class="panel-body">
						@if (Session::has('success'))
	                        <div class="alert alert-success">
	                            <strong>Success!</strong> {{ Session::get('success') }}
	                        </div> 
	                    @elseif (Session::has('danger'))
	                        <div class="alert alert-danger">
	                            <strong>Whoops!</strong> {{ Session::get('danger') }}
	                        </div>             
	                    @endif
						{!! Form::open(['action' => 'UserController@postChangePassword', 'method' => 'POST']) !!}

							<div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-4{{ $errors->has('current_password') ? ' has-error' : '' }}">
										<label for="current_password" class="control-label">Current password</label>
	                                    <input id="current_password" type="password" class="form-control" name="current_password">
		                                @if ($errors->has('current_password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('current_password') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-4{{ $errors->has('password') ? ' has-error' : '' }}">
										<label for="password" class="control-label">New password</label>
	                                    <input id="password" type="password" class="form-control" name="password">
		                                @if ($errors->has('password'))					
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="col-md-4{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
										<label for="password-confirm" class="control-label">Confirm password</label>
	                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
		                                @if ($errors->has('password_confirmation'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
		                                    </span>
		                                @endif
									</div>
	                        	</div>
	                        </div>

	                        <div class="text-right">
	                        	<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
	                        </div>
                        {!! Form::close() !!}
					</div>
				</div>
				<!-- /account settings -->
			</div>
		</div>
		<!-- /tab content -->
	</div>
</div>
<!-- /detached content -->

@stop

@section('user-javascript')

{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/plugins/pickers/anytime.min.js') !!}

<script type="text/javascript">
	jQuery(document).ready(function() {

		$('#country, #profession').select2();
		$("#date_of_birth").AnyTime_picker({
        format: "%Z-%m-%d"
        });
        $('#profession').on('change',function(){
            var optionVar = $('#profession option').filter(':selected').val();
            if (optionVar == '5'){
                $('#profession_div').removeClass("col-md-6").addClass("col-md-2");
                $('#hidden_label').show();
                $('#input1').show();
            }
            else {
            	$('#profession_div').removeClass("col-md-2").addClass("col-md-6");
            	$('#hidden_label').hide();
                $('#input1').hide();
            }
        });
        $( '#profession' ).trigger( "change" );

	});
</script>

@stop