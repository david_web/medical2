@extends('users.user-layout')

@section('user-sidebar-content')

<div class="sidebar-category">
	<div class="category-title">
		<span>my societies</span>
		<ul class="icons-list">
			<li><a href="#" data-action="collapse" class=""></a></li>
		</ul>
	</div>
	<div class="category-content" style="display: block;">
		<ul class="media-list">
			@foreach($groups as $group)
				<li class="media">
					<a class="media-left">
						@if(!$group->path)
							<img src="/assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
						@else
							<img src="/assets/uploads/{{$group->path}}" class="img-sm img-circle" alt="">
						@endif
					</a>
					<div class="media-body">
						<a href="{{action('GroupController@getGroup', $group->id)}}" class="media-heading text-semibold">{{$group->name}}</a>
						<span class="text-size-mini text-muted display-block">{{$group->users_count}}
							@if ($group->users_count == 1)
								member
							@else 
								members
							@endif
						</span>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
</div>

@stop

@section('user-content')

<div class="container-detached">
    <div class="content-detached">

    	<div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title text-semibold">{{$user->first_name}} {{$user->last_name}}</h6>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>
			<div class="panel-body">
				<div class="col-md-6">
					<p><strong>Date of Birth:</strong> {{$user->date_of_birth}}</p>
					<p><strong>Country:</strong> {{$user->nationality}}</p>
					<p><strong>Place of residence:</strong> {{$user->residence}}</p>
					<p><strong>Profession:</strong> {{$user->other_profession}}</p>
					<p><strong>Place of work or study:</strong> {{$user->place_of_work_or_study}}</p>
				</div>
				<div class="col-md-6">
					<p><strong>Name of institution:</strong> {{$user->institution_name}}</p>
					<p><strong>E-Mail Address:</strong> {{$user->email}}</p>
					<p><strong>Mobile number:</strong> {{$user->mobile_number}}</p>
					<p><strong>Facebook:</strong> {{$user->facebook}}</p>
					<p><strong>Vk:</strong> {{$user->vk}}</p>
				</div>
			</div>
        </div>

        <div class="panel panel-white">
			<div class="panel-heading header" >
				<!-- <h6 class="panel-title text-semibold">{{$user->first_name}} library</h6> -->
				<ul class="col-md-10">
				    <li class=" li" id="user_library"><a data-toggle="tab" href="#library">{{$user->first_name}} library</a></li>
				    <li class=" li" id="user_discussion" data-id="{{$user->id}}">
					    <a>
					    	Discussions
					    </a>
                    </li>
				    <li class=" li"><a  href="#followed">Followed</a></li>
				    <li class=" li"><a  href="#following">Following</a></li>
				    <li class=" li"><a  href="#question">My Questions</a></li>
				  </ul>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div>
			<div id="library_discussions">	
				<table class="table table-striped media-library table-lg">
	                <thead>
	                    <tr>
	                    	<th><input type="checkbox" class="styled"></th>
	                        <th>Preview</th>
	                        <th>Name</th>
	                        <th>Author</th>
	                        <th>Date</th>
	                        <th>File info</th>
	                        <th class="text-center">Actions</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@foreach($files as $file)
		                    <tr>
		                    	<td><input type="checkbox" class="styled"></td>
		                        <td>
			                        <a>
				                        <img data-toggle="modal" data-target="#modal{{$file->id}}" style="height: 70px;" src="/assets/images/{{$file->type}}.png">
			                        </a>
		                        </td>
		                        <td>{{$file->description}}</td>
		                        <td>{{$user->first_name}} {{$user->last_name}}</td>
		                        <td>{{$file->created_at}}</td>
		                        <td>
		                        	<ul class="list-condensed list-unstyled no-margin">
			                        	<li><span class="text-semibold">Format:</span> .{{$file->type}}</li>
		                        	</ul>
		                        </td>
		                        <td class="text-center">
		                            <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
												<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
												<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
												<li class="divider"></li>
												<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
											</ul>
										</li>
									</ul>
		                        </td>
		                    </tr>

		                    <!-- Modal -->
								<div id="modal{{$file->id}}" class="modal fade in" >
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">×</button>
											</div>
											<div class="modal-body">
												@if($file->type == 'mp4')
													<video width="860" controls>
														<source src="/assets/uploads/{{$file->path}}">
													</video>
												@else
													<embed src="/assets/uploads/{{$file->path}}" width="860" height="800" />
												@endif	
											</div>
										</div>
									</div>
								</div>
							<!-- /Modal -->
	                    @endforeach
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- Modal discussion -->
    <div id="modal_iconified" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title"><i class="icon-menu7"></i> Creat New Discussion</h5>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label for="inp">Topic of discussion </label>
						<input type="text" class="form-control " id="inp" name="name">
					</div>
					<div class="form-group" id="sel" >
						<label>Invite Friend</label>
						<select name="user_id" id="friends_sel">
							@foreach($friends as $friend)
								<option value="{{$friend->id}}" class="option">
									{{$friend->first_name}} {{$friend->last_name}} 
								</option>
							@endforeach
						</select>
					</div>		
					<div class="checkbox">
						<label id="checkbox">
							<input id="public" type="checkbox" checked="checked" class="styled" name="public">
							Public
						</label>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
					<button class="btn btn-primary" type="submit" data-id="{{$user->id}}" data="{{Auth::user()->id}}" id="create_discussion"><i class="icon-check"></i> Create</button>
				</div>
			</div>
		</div>
	</div>
<!-- /Modal -->
@stop

@section('user-javascript')

	{!! HTML::script('assets/js/pages/gallery_library.js') !!}
	{!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js') !!}
	{!! HTML::script('assets/js/plugins/media/fancybox.min.js') !!}
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}

@stop