@extends('users.user-layout')

@section('user-content')

<div class="container-detached">
    <div class="content-detached">
		<!-- Account settings -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title">File upload</h6>
				<div class="heading-elements">
					<ul class="icons-list">
		        		<li><a data-action="collapse"></a></li>
		        		<li><a data-action="reload"></a></li>
		        		<li><a data-action="close"></a></li>
		        	</ul>
		    	</div>
			</div>
			<div class="panel-body">
				{!! Form::open(['action' => 'FileController@postUserFileUpload', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
					<div>
						<div class="{{ $errors->has('country') ? ' has-error' : '' }}" id="countries_div" style="margin-bottom: 20px">
							<label for="country" class="control-label">Country</label>
		                    {!! Form::select('country', ['' => 'Select a country'] + $countries, null, ['class' => 'select-multiple-drag', 'id' => 'country']) !!}
		                        
		                    @if ($errors->has('country'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('country') }}</strong>
		                        </span>
		                    @endif
						</div>
					</div>

					<div>
						<div class="{{ $errors->has('institution_name') ? ' has-error' : '' }}" style="margin-bottom: 20px">
							<label for="institution_name" class="control-label">Institution name</label>
							{!! Form::text('institution_name', null, ['class' => 'form-control', 'id' => 'institution_name']) !!}

		                    @if ($errors->has('institution_name'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('institution_name') }}</strong>
		                        </span>
		                    @endif
						</div>
					</div>

					<div>
						<div class="{{ $errors->has('course') ? ' has-error' : '' }}" style="margin-bottom: 20px">
							<label for="course" class="control-label">Course</label>
							{!! Form::text('course', null, ['class' => 'form-control', 'id' => 'course']) !!}

		                    @if ($errors->has('course'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('course') }}</strong>
		                        </span>
		                    @endif
						</div>
					</div>

					<div>
						<div class="{{ $errors->has('language') ? ' has-error' : '' }}" style="margin-bottom: 20px">
							<label for="language" class="control-label">Language of materials</label>
							{!! Form::text('language', null, ['class' => 'form-control', 'id' => 'language']) !!}

		                    @if ($errors->has('language'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('language') }}</strong>
		                        </span>
		                    @endif
						</div>
					</div>

					<div>
						<div class="{{ $errors->has('description') ? ' has-error' : '' }}" style="margin-bottom: 20px">
							<label for="description" class="control-label">Short description of the material</label>
							{!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}

		                    @if ($errors->has('description'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('description') }}</strong>
		                        </span>
		                    @endif
						</div>
					</div>

					<div>
						<label for="choose" class="display-block control-label">Choose folder</label>
						<div class="input-group" style="margin-bottom: 20px">
							{!! Form::text(null, null, ['class' => 'form-control', 'id' => 'choose', 'readonly', 'placeholder' => 'No folder choosen', 'data-toggle' => 'modal', 'data-target' => '#choose_folder']) !!}

							{!! Form::hidden('folder_id', null, ['class' => 'form-control', 'id' => 'hidden_choose']) !!}
							<span class="input-group-btn">
								<button data-toggle="modal" data-target="#choose_folder" type="button" class="btn btn-primary">Choose Folder</button>
							</span>
						</div>
		            </div>
		          
		            <div>
						<div class="{{ $errors->has('file') ? ' has-error' : '' }}" id="file_div" style="margin-bottom: 20px">
							<label for="upload" class="display-block control-label">Upload file</label>
							{{Form::file(null, ['class' => 'file-styled', 'id' => 'upload', 'name' => 'file'])}}
		                    <span class="help-block">Accepted formats: txt, pdf, jpg, jpeg, png, mp4, m4v, mov, mpg, mpeg, asf, avi</span>

		                    @if ($errors->has('file'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('file') }}</strong>
		                        </span>
		                    @endif
						</div>
		            </div>

				    <div class="checkbox">
						<label id="checkbox">
							<input id="public" type="checkbox" class="styled">
							{!! Form::hidden('is_public', 0, ['id' => 'hidden_public']) !!}
							Public
						</label>
					</div>

		            <div class="text-right">
		            	<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
		            </div>
				{!! Form::close() !!}
			</div>
		</div>

		<!-- Modal -->
			<div id="choose_folder" class="modal fade in" style="margin-top: 10%">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class=" modal-title">Choose folder</h4>
						</div>

						<div id="modal_folder" class="modal-body" style="overflow: auto;">
							<a>library/</a>
							<p id="folder_select">You didn't select any folder</p>
							@foreach($foldersData as $folderData)
								<div style="float: left;">
									<a class="modal_folder" data-id="{{$folderData->id}}" data-name="{{$folderData->name}}">
										<img class="folder_img" id="folder_img{{$folderData->id}}" style="height: 70px; margin-right: 15px;" src="/assets/images/folder.png">
									</a><br>
									<span style="font-size: 12px">{{$folderData->name}}</span>
								</div>
							@endforeach
						</div>

						<div class="modal-footer">
							<button id="save_folder" data-id="" data-name="" type="button" data-dismiss="modal" class="btn btn-primary">Save</button>
						</div>
					</div>
				</div>
			</div>
		<!-- /Modal -->
		
	</div>
</div>

@stop

@section('user-javascript')

{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
{!! HTML::script('assets/js/pages/user_profile_tabbed.js') !!}
{!! HTML::script('assets/js/core/libraries/jasny_bootstrap.min.js') !!}
{!! HTML::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
{!! HTML::script('assets/js/plugins/visualization/echarts/echarts.js') !!}
{!! HTML::script('assets/js/main/user-file-upload.js') !!}

@stop