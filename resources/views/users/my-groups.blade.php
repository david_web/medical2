@extends('groups.groups-layout')

@section('groups-content')

@foreach($groups as $group)
	<div class="panel panel-flat col-md-6">
		<div class="col-md-3">
		    @if(!$group->path)
				<img style="height: 130px; margin: 10px; margin-left: 0" src="/assets/images/placeholder.jpg">
			@else
				<img style="height: 130px; max-width: 170px; margin: 10px; margin-left: 0" src="/assets/uploads/{{$group->path}}">
			@endif
		</div>

		<div class="col-md-5">
			<div class="panel-body">
				<span style="font-size: 18px"><b><a href="{{action('GroupController@getGroup', $group->id)}}">{{$group->name}}</a></b></span>
				<div style="color: grey; margin-top: 23px">
					<span><b>15</b> subgroups</span><br>
					<span><b>{{$group->users_count}}</b> members</span><br>
					<a data-toggle="modal" data-target="#leave_society_modal{{$group->id}}"><i style="font-size: 12px" class="glyphicon glyphicon-minus"></i> leave society</a>
				</div>
			</div>
		</div>
	</div>
@endforeach

@stop

@section('javascript')
	{!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
	<script type="text/javascript">
		
		$(".group_users").select2();

		function displayUsers() {
			var users = $( ".group_users" ).val() || [];
			$('#user').val(JSON.stringify(users));
		}
		$( "select" ).change( displayUsers );

	</script>
@stop
