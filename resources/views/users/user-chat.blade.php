<div class="panel-body">
<div></div>
	<button type="button" class="btn btn-primary" id="bootbox_form" data-toggle="modal" data-target="#modal_iconified">
		Create New Discussion
	</button>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title">Discussion Requests</h6>
		</div>
		<div class="panel-body " style="overflow: auto;  max-height: 150px;">  		
				@foreach(Auth::User()->discussions as $discussion)
					@if(Auth::id()!=$discussion->user->id && $discussion->pivot->accepted == 0)
						<li class="media" id="li{{$discussion->id}}">
							<div class="media-left">
								<a href="#"><img src="{{asset('assets/uploads/'.$discussion->user->path)}}" class="img-circle" alt=""></a>
								{{$discussion->user->first_name}}
								{{$discussion->user->last_name}}

							</div>

							<div class="media-body">
								<div class="media-heading text-semibold">
									{{$discussion->name}}
								</div>
							</div>

							<div class="media-right media-middle">
								<ul class="icons-list text-nowrap">
									<li>
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a class="start_chat" data-id='{{$discussion->id}}' data="{{$discussion->user->id}} " data-topic="{{$discussion->name}} "><i class="icon-comment-discussion pull-right"></i> Start chat</a>
											</li>
											<li><a class="reject" data-id='{{$discussion->id}}' data="{{$discussion->user->id}} " data-topic="{{$discussion->name}} ">
												<i class="icon-bin pull-right"></i></i>Reject</a>
											</li>
											
										</ul>
									</li>
								</ul>
							</div>
						</li>
					
					@endif	
				@endforeach
			</ul>
		</div>
	</div>





	<h6 class="content-group text-semibold">Discussions</h6>
	@foreach(Auth::User()->discussions as $discussion)	
		@if($discussion->pivot->accepted == 1)
		    <div class="panel-group panel-group-control panel-group-control-right" id="chat-accordion">
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a data-toggle="collapse" href="#collapsible-james{{$discussion->id}}" data-parent="#chat-accordion">
								<img src="assets/images/placeholder.jpg" alt="" class="img-circle position-left">
								{{$discussion->name}}
								<span class="status-mark border-success position-right"></span>						
							</a>
						</h6>
						<ul class="icons-list text-nowrap " style="float: right;">
							<li>
								<a href="#" ><i class="icon-menu7"></i></a>
							<!-- 	class="dropdown-toggle" data-toggle="dropdown" -->
								<ul class="dropdown-menu dropdown-menu-right">
									<li>
										<a data-toggle="modal" data-target="#choose_folder" class="start_chat" data-id='{{$discussion->id}}' data="{{$discussion->user->id}} " data-topic="{{$discussion->name}} "><i class="glyphicon glyphicon-edit pull-right"></i> Edit</a>
									</li>
									<li><a class="reject" data-id='{{$discussion->id}}' data="{{$discussion->user->id}} " data-topic="{{$discussion->name}} ">
										<i class="icon-bin pull-right"></i></i>Delete</a>
									</li>
									
								</ul>
							</li>
						</ul>			
					</div>
						<div id="collapsible-james{{$discussion->id}}" class="panel-collapse collapse">
							<div class="panel-body">
								<ul class="media-list chat-list content-group" style=" max-height: 320px;">
									@foreach($discussion->measeges as $measege)										
									@if($measege->user_id == Auth::id())
										<li class="media">
											<div class="media-left">
												<a href="assets/images/placeholder.jpg">
													<img src="{{asset('assets/uploads/'.Auth::user()->path)}}" class="img-circle" alt="">
												</a>
											</div>

											<div class="media-body">
												<div class="media-content">
													{{$measege->measege}}
												</div>
												<span class="media-annotation display-block mt-10">
													{{ $measege->created_at->format('Y-m-d  H:i:s') }}
													 
													<a href="#">
														<i class="icon-pin-alt position-right text-muted"></i>
													</a>
												</span>
											</div>
										</li>
									@else	
										<li class="media reversed">
											<div class="media-body">
												<div class="media-content">
													{{$measege->measege}}
												</div>
												<span class="media-annotation display-block mt-10">
													{{ $measege->created_at->format('Y-m-d  H:i:s') }}
													<a href="#">
														<i class="icon-pin-alt position-right text-muted"></i>
													</a>
												</span>
											</div>
											<div class="media-right">
												<a href="assets/images/placeholder.jpg">
													<img src="{{asset('assets/uploads/'.$measege->user->path)}}" class="img-circle" alt="">
												</a>
											</div>
										</li>
										@endif
									@endforeach	
								</ul>
								{!! Form::open(array('action' => array('DiscussionController@postSendMeasege'))) !!}		
			                    	<textarea name="measege" class="form-control content-group measege_text" rows="3" cols="1" placeholder="Enter your message..."></textarea>
			                    	<input name="user_id" type="hidden" value="{{Auth::id()}}">
			                    	<input name="discussion_id" type="hidden" value="{{$discussion->id}}">
			                		
			                		<div class=" text-right" >
			                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right send_measege"><b><i class="icon-circle-right2"></i></b> Send</button>
			                		</div>
								{!!Form::close()!!}    
								@if(Auth::id() == $discussion->user_id)	
										<div class=" text-left" style="margin-top: 10px;">
			                    				<input name="id" type="hidden" value="{{$discussion->id}}">
					                            <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right send_measege"><b><i class="icon-circle-right2"></i></b><a style="color: white;" href="{{action('DiscussionController@getRemoveDiscussion',$discussion->id)}}"> Delete Discussion</a></button>
					                	</div> 
				                @endif	
	                    	</div>
						</div>
					</div>
				</div>
		@endif
		<!-- Modal -->
<div id="choose_folder" class="modal fade in" style="margin-top: 10%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class=" modal-title">Edit</h4>
			</div>
			<div class="modal-body">
				<div id="title_div">
					<label for="name" class="control-label">Title</label>
					<input type="text" class="form-control" name="discussion_name" id="name">
		            
		        </div>
				
			
			</div>	
			<div class="modal-footer">
				<button id="save_folder" data-id="" data-name="" type="button" data-dismiss="modal" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>
		<!-- /Modal -->

	@endforeach	
</div>
