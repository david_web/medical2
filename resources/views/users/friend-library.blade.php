<div class="tab-content">
	<div id="friend_library_discussions" class="tab-pane fade in active">
		@if (Auth::user()->can('view', $user))
			<table class="table table-striped media-library table-lg">
                <thead>
                    <tr>
                    	<th><input type="checkbox" class="styled"></th>
                        <th>Preview</th>
                        <th>Name</th>
                        <th>Author</th>
                        <th>Date</th>
                        <th>File info</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($files as $file)
	                    <tr>
	                    	<td><input type="checkbox" class="styled"></td>
	                        <td>
		                        <a>
			                        <img data-toggle="modal" data-target="#modal{{$file->id}}" style="height: 70px;" src="/assets/images/{{$file->type}}.png">
		                        </a>
	                        </td>
	                        <td>{{$file->description}}</td>
	                        <td>{{$user->first_name}} {{$user->last_name}}</td>
	                        <td>{{$file->created_at}}</td>
	                        <td>
	                        	<ul class="list-condensed list-unstyled no-margin">
		                        	<li><span class="text-semibold">Format:</span> .{{$file->type}}</li>
	                        	</ul>
	                        </td>
	                        <td class="text-center">
	                            <ul class="icons-list">
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown">
											<i class="icon-menu9"></i>
										</a>

										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#"><i class="icon-pencil7"></i> Edit file</a></li>
											<li><a href="#"><i class="icon-copy"></i> Copy file</a></li>
											<li><a href="#"><i class="icon-eye-blocked"></i> Unpublish</a></li>
											<li class="divider"></li>
											<li><a href="#"><i class="icon-bin"></i> Move to trash</a></li>
										</ul>
									</li>
								</ul>
	                        </td>
	                    </tr>
				 
		                <!-- Modal -->
							<div id="modal{{$file->id}}" class="modal fade in" >
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">×</button>
										</div>
										<div class="modal-body">
											@if($file->type == 'mp4')
												<video width="860" controls>
													<source src="/assets/uploads/{{$file->path}}">
												</video>
											@else
												<embed src="/assets/uploads/{{$file->path}}" width="860" height="800" />
											@endif	
										</div>
									</div>
								</div>
							</div>
						<!-- /Modal -->
	                @endforeach
	            </tbody>
        	</table>
		@else
			<div style="padding: 20px;">
				<span>You can`t view {{$user->first_name}} library</span>
			</div>
		@endif
	</div>
</div>

<!-- Modal -->
    <div id="modal_iconified" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title"><i class="icon-menu7"></i> Creat New Discussion</h5>
			</div>

			<div class="modal-body">
				<label>Topic of discussion </label>
				<input type="text" class="form-control" name="">
			<div class="form-group">
				

				<label class="checkbox-inline">
					<input type="checkbox" class="styled">
							Public
				</label>
			</div>
					
			</div>

			<div class="modal-footer">
				<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
				<button class="btn btn-primary"><i class="icon-check"></i> Save</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->

    {!! HTML::script('assets/js/pages/gallery_library.js') !!}
    {!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js') !!}
    {!! HTML::script('assets/js/plugins/media/fancybox.min.js') !!}
    {!! HTML::script('assets/js/plugins/forms/selects/select2.min.js') !!}
