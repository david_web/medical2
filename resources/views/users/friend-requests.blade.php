@if (count($friendRequests) == 0)
	<span style="font-size: 18px">You have no associate requests</span>
@else
	<ul class="media-list">
		@foreach($friendRequests as $user)
			<li class="media">
				<div class="media-left media-middle">
					@if(!$user->path)
						<img src="/assets/images/placeholder.jpg" class="img-circle" alt="">
					@else
						<img src="/assets/uploads/{{$user->path}}" class="img-circle" alt="">
					@endif
				</div>

				<div class="media-body media-middle">
					<div class="media-heading text-semibold"><a href="{{action('UserController@getUserPage', $user->id)}}">{{$user->first_name}} {{$user->last_name}}</a></div>
				</div>

				<div class="media-right media-middle">
					<ul class="icons-list text-nowrap">
                    	<li class="dropdown">
                    		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>

                    		<ul class="dropdown-menu dropdown-menu-right">
		                    	<li><a href="{{action('UserController@getAcceptRequest', $user->id)}}" data-toggle="modal"> Accept</a></li>
		                    	<li><a href="{{action('UserController@getRejectRequest', $user->id)}}" data-toggle="modal"> Reject</a></li>
                    		</ul>
                    	</li>
                	</ul>
				</div>
			</li>
		@endforeach
	</ul>
@endif