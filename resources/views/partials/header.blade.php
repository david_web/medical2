 <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <!-- <a class="navbar-brand" href="{{action('AdminController@getDashboard')}}"><img src="{{ url('assets/images/logo_light.png') }}" alt=""></a> -->
            <a class="navbar-brand" href="{{action('AdminController@getDashboard')}}">INFOSHR</a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        @if(!Auth::user()->path)
                            <img src="/assets/images/placeholder.jpg" alt="">
                        @else
                            <img src="/assets/uploads/{{Auth::user()->path}}" alt="" style="width: 30px; height: 30px">
                        @endif    
                        <span>{{Auth::user()->first_name}}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{action('UserController@getMyProfile')}}" class="list-group-item"><i class="icon-user"></i> My profile</a></li>
                        <li><a href="{{action('UserController@getMyFriends')}}" class="list-group-item"><i class="icon-users"></i> My associates</a></li>
                        <li><a href="{{action('QuestionController@getMyQuestions')}}"><i class="fa fa-group glyphicon glyphicon-question-sign"></i> My questions</a></li>
                        <li><a href="{{action('FileController@getMyLibrary')}}"><i class="icon-files-empty"></i> My library</a></li>
                        <li><a href="{{action('FileController@getUserFileUpload')}}"><i class="icon-files-empty"></i> File upload</a></li>
                        <li class="divider"></li>
                        <li><a href="{{action('UserController@getProfileInformation')}}"><i class="icon-cog3"></i></i> Profile information</a></li>
                        <li><a href="{{action('AuthController@getLogout')}}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
<!-- /main navbar -->