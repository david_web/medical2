
<div class="sidebar sidebar-main">

    <!-- Start: Sidebar Left Content -->
    <div class="sidebar-content">
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <!-- Start: Sidebar Menu -->
                <ul class="navigation navigation-main navigation-accordion">
                    <li class="navigation-header">Main Menu</li>
                    <li>
                        <a href="{{action('AdminController@getDashboard')}}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    
                    <li class="navigation-header">Data Entry</li>

                    <li>
                        <a href="{{action('UserController@getMyProfile')}}">
                            <i class="fa fa-list-ul glyphicon glyphicon-user"></i>
                            <span>My page</span>
                        </a>
                    </li>

                    <li class="#">
                        <a class="accordion-toggle" href="#">
                            <i class="fa fa-group glyphicon glyphicon-question-sign"></i>
                            <span class="sidebar-title">Questions & Answers</span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="">
                                <a href="{{action('QuestionController@getAddQuestion')}}">
                                    <i class="fa fa-list-ul glyphicon glyphicon-plus"></i>Add question</a>
                            </li>

                            <li class="">
                                <a href="{{action('QuestionController@getQuestions')}}">
                                    <i class="fa fa-list-ul glyphicon glyphicon-list-alt"></i>Questions & Answers</a>
                            </li>
                        </ul>
                    </li>

                    <li class="#">
                        <a class="accordion-toggle" href="#">
                            <i class="icon-files-empty"></i>
                            <span class="sidebar-title">Files</span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="">
                                <a href="{{action('FileController@getMaterials')}}">
                                    <i class="icon-files-empty"></i></i>Materials</a>
                            </li>

                            <li class="">
                                <a href="{{action('FileController@getFileUpload')}}">
                                    <i class="icon-files-empty"></i></i>File upload</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="{{action('GroupController@getGroups')}}">
                            <i class="icon-users"></i>
                            <span>Societies</span>
                        </a>
                    </li>
                </ul>
                <!-- End: Sidebar Menu -->
            </div>
        </div>
    </div>
    <!-- End: Sidebar Left Content -->
</div>
