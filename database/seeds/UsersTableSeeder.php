<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'email' => 'david1994martirosyan@gmail.com',
        	'password' => bcrypt(1),
        	'first_name' => 'David',
        	'last_name' => 'Martirosyan',
        	'other_name' => null,
        	'date_of_birth' => '1996-05-02',
        	'nationality' => 'Armenia',
        	'path' => null,
        	'residence' => 'aaa',
        	'profession_id' => 3,
        	'other_profession' => null,
        	'place_of_work_or_study' => 'aaa',
        	'institution_name' => 'aaa',
        	'mobile_number' => '1111111111',
        	'whatsapp' => '1111111111',
        	'facebook' => 'aaa',
        	'vk' => 'aaa',
            'viber' => '1111111111',
        	'is_validated' => 1,
        	'verify_token' => null,
        	'points' => null,
        ]);

        DB::table('users')->insert([
            'email' => 'hovo2099@gmail.com',
            'password' => bcrypt(1),
            'first_name' => 'Hovo',
            'last_name' => 'Kulijanyan',
            'other_name' => null,
            'date_of_birth' => '1996-05-02',
            'nationality' => 'Armenia',
            'path' => null,
            'residence' => 'aaa',
            'profession_id' => 3,
            'other_profession' => null,
            'place_of_work_or_study' => 'aaa',
            'institution_name' => 'aaa',
            'mobile_number' => '1111111111',
            'whatsapp' => '1111111111',
            'facebook' => 'aaa',
            'vk' => 'aaa',
            'viber' => '1111111111',
            'is_validated' => 1,
            'verify_token' => null,
            'points' => null,
        ]);

        DB::table('users')->insert([
            'email' => 'mher2099@gmail.com',
            'password' => bcrypt(1),
            'first_name' => 'Mher',
            'last_name' => 'Kulijanyan',
            'other_name' => null,
            'date_of_birth' => '1996-05-02',
            'nationality' => 'Armenia',
            'path' => null,
            'residence' => 'aaa',
            'profession_id' => 3,
            'other_profession' => null,
            'place_of_work_or_study' => 'aaa',
            'institution_name' => 'aaa',
            'mobile_number' => '1111111111',
            'whatsapp' => '1111111111',
            'facebook' => 'aaa',
            'vk' => 'aaa',
            'viber' => '1111111111',
            'is_validated' => 1,
            'verify_token' => null,
            'points' => null,
        ]);
    }
}
