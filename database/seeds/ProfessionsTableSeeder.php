<?php

use Illuminate\Database\Seeder;
use App\Profession;

class ProfessionsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professions')->insert(['profession' => 'student']);
        DB::table('professions')->insert(['profession' => 'nurse']);
        DB::table('professions')->insert(['profession' => 'doctor']);
        DB::table('professions')->insert(['profession' => 'pharmacist']);
        DB::table('professions')->insert(['profession' => 'other']);
    }
}
