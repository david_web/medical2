<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert(['tag' => 'surgery']);
        DB::table('tags')->insert(['tag' => 'stomatology']);
        DB::table('tags')->insert(['tag' => 'pharmacist']);
        DB::table('tags')->insert(['tag' => 'dietology']);
        DB::table('tags')->insert(['tag' => 'gynecology']);
        DB::table('tags')->insert(['tag' => 'immunology']);
        DB::table('tags')->insert(['tag' => 'endocrinology']);
    }
}