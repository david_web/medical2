<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('other_name')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('nationality')->nullable();
            $table->string('path')->nullable();
            $table->string('residence')->nullable();
            $table->integer('profession_id')->nullable();
            $table->string('other_profession')->nullable();
            $table->string('place_of_work_or_study')->nullable();
            $table->string('institution_name')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('facebook')->nullable();
            $table->string('vk')->nullable();
            $table->string('viber')->nullable();
            $table->boolean('is_validated')->default(0);
            $table->string('verify_token')->nullable();
            $table->integer('points')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
