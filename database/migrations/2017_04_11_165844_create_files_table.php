<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('folder_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->string('country')->nullable();
            $table->string('institution_name')->nullable();
            $table->string('course')->nullable();
            $table->string('language')->nullable();
            $table->text('description')->nullable();
            $table->string('path')->nullable();
            $table->string('type')->nullable();
            $table->boolean('is_public')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
