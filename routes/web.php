<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


### AuthController ###
Route::get('login', 'AuthController@getLogin');
Route::post('login', 'AuthController@postLogin');
Route::get('register', 'AuthController@getRegister');
Route::post('register', 'AuthController@postRegister');
Route::get('logout', 'AuthController@getLogout');
Route::get('verify/{token}', 'AuthController@getVerify');

### AdminController ###
Route::get('/', 'AdminController@getDashboard');

### QuestionController ###
Route::get('add-question', 'QuestionController@getAddQuestion');
Route::post('add-question', 'QuestionController@postAddQuestion');
Route::get('questions', 'QuestionController@getQuestions');
Route::get('question/{id}/{keyword}', 'QuestionController@getQuestion');
Route::get('edit-question/{id}', 'QuestionController@getEditQuestion');
Route::post('edit-question/{id}', 'QuestionController@postEditQuestion');
Route::get('delete-question/{id}', 'QuestionController@getDeleteQuestion');
Route::get('add-like/{id}', 'QuestionController@getAddLike');
Route::get('search', 'QuestionController@getSearch');
Route::get('my-questions', 'QuestionController@getMyQuestions');

### CommentController ###
Route::post('add-comment', 'CommentController@postAddComment');
Route::post('add-file-comment', 'CommentController@postAddFileComment');
Route::get('delete-comment/{id}', 'CommentController@getDeleteComment');
Route::get('edit-comment/{id}', 'CommentController@getEditComment');

### UserController ###
Route::get('my-profile', 'UserController@getMyProfile');
Route::get('profile-information', 'UserController@getProfileInformation');
Route::get('my-friends', 'UserController@getMyFriends');
Route::post('edit-user', 'UserController@postEditUserInformation');
Route::post('change-password', 'UserController@postChangePassword');
Route::post('upload-user-image', 'UserController@postImageUpload');
Route::get('delete-user-image', 'UserController@getDeleteImage');
Route::get('user-page/{id}', 'UserController@getUserPage');
Route::get('add-friend/{id}', 'UserController@getAddFriend');
Route::get('friend-requests', 'UserController@getFriendRequests');
Route::get('friends', 'UserController@getFriends');
Route::get('accept-friend-request/{id}', 'UserController@getAcceptRequest');
Route::get('reject-friend-request/{id}', 'UserController@getRejectRequest');
Route::get('remove-friend/{id}', 'UserController@getRemoveFriend');
//Route::get('user-discussion','UserController@getUserDiscussion'); 
Route::get('user-library','UserController@getUserLibrary'); 
Route::get('friend-discussion/{id}','UserController@getFriendDiscussion');
Route::get('friend-library/{id}','UserController@getFriendLibrary');
//Route::post('create-discussion/{id}','UserController@postCreateDiscussion');
//Route::post('start-chat','UserController@postStartChat');
//Route::post('send-measege/{id}','UserController@postSendMeasege');
### FileController ###
Route::get('file-upload', 'FileController@getFileUpload');
Route::post('file-upload', 'FileController@postFileUpload');
Route::get('user-file-upload', 'FileController@getUserFileUpload');
Route::post('user-file-upload', 'FileController@postUserFileUpload');
Route::get('materials', 'FileController@getMaterials');
Route::get('my-library', 'FileController@getMyLibrary');
Route::post('group-file-upload/{id}', 'FileController@postGroupFileUpload');
Route::get('add-file/{id}', 'FileController@getAddFile');
Route::get('edit-file/{id}/{groupId?}', 'FileController@getEditFile');
Route::post('edit-file/{id}/{groupId?}', 'FileController@postEditFile');
Route::get('delete-file/{id}/{groupId?}', 'FileController@getDeleteFile');
Route::get('all-files/{id}/{folderId?}', 'FileController@getAllFiles');
Route::get('pdf-files/{id}/{folderId?}', 'FileController@getPdfFiles');
Route::get('txt-files/{id}/{folderId?}', 'FileController@getTxtFiles');
Route::get('docs-files/{id}/{folderId?}', 'FileController@getDocsFiles');
Route::get('ppt-files/{id}/{folderId?}', 'FileController@getPptFiles');
Route::get('video-files/{id}/{folderId?}', 'FileController@getVideoFiles');
Route::get('photo-files/{id}/{folderId?}', 'FileController@getPhotoFiles');
Route::get('audio-files/{id}/{folderId?}', 'FileController@getAudioFiles');

### FolderController ###
Route::post('add-folder/{id?}', 'FolderController@postAddFolder');
Route::get('folder/{id}/{groupId?}', 'FolderController@getChildFolder');
Route::get('folder', 'FolderController@getParentFolders');
Route::get('modal-folder/{id}', 'FolderController@getModalChildFolder');
Route::get('modal-folder', 'FolderController@getModalParentFolders');
Route::get('delete-folder/{id}/{groupId?}', 'FolderController@getDeleteFolder');
Route::get('group-folder/{id}', 'FolderController@getGroupParentFolders');
Route::get('group-modal-folder/{id}', 'FolderController@getGroupModalParentFolders');
Route::get('modal-folder', 'FolderController@getModalParentFolders');
Route::post('add-group-folder/{id?}', 'FolderController@postAddGroupFolder');

### GroupController ###
Route::get('societies', 'GroupController@getGroups');
Route::get('my-societies', 'GroupController@getMyGroups');
Route::post('add-group', 'GroupController@postAddGroup');
Route::post('add-user-to-group/{id}', 'GroupController@postAddUserToGroup');
Route::get('group/{id}', 'GroupController@getGroup');
Route::get('group-requests', 'GroupController@getGroupRequests');
Route::get('accept-request/{id}/{userId?}', 'GroupController@getAcceptRequest');
Route::get('reject-request/{id}/{userId?}', 'GroupController@getRejectRequest');
Route::post('upload-image/{id}', 'GroupController@postGroupImageUpload');
Route::get('delete-group-image/{id}', 'GroupController@getDeleteGroupImage');
Route::get('management/{id}', 'GroupController@getGroupManagement');
Route::post('change-role/{id}', 'GroupController@postChangeRole');
Route::get('delete-group-user/{id}/{userId}', 'GroupController@getDeleteUser');
Route::get('announcements/{id}', 'GroupController@getGroupAnnouncements');
Route::get('discussion/{id}', 'GroupController@getGroupDiscussion');
Route::get('polls/{id}', 'GroupController@getGroupPolls');
Route::get('leave-society/{id}', 'GroupController@getLeaveGroup');
Route::get('ask-to-join/{id}', 'GroupController@getAskToJoin');
Route::get('sub-groups/{id}', 'GroupController@getSubGroups');
Route::post('add-sub-group/{id}', 'GroupController@postAddSubGroup');

### AnnouncementController ###
Route::get('add-announcement/{id}', 'AnnouncementController@getAddAnnouncement');
Route::post('add-announcement', 'AnnouncementController@postAddAnnouncement');
Route::get('delete-announcement/{id}/{groupId}', 'AnnouncementController@getDeleteAnnouncement');
Route::get('edit-announcement/{id}/{groupId}', 'AnnouncementController@getEditAnnouncement');
Route::post('edit-announcement/{id}/{groupId}', 'AnnouncementController@postEditAnnouncement');

### PollController ###
Route::get('add-poll/{id}', 'PollController@getAddPoll');
Route::post('add-poll/{id}', 'PollController@postAddPoll');
Route::get('choose-option/{id}/{groupId}', 'PollController@getChooseOption');
Route::get('delete-poll/{id}/{groupId}', 'PollController@getDeletePoll');
Route::get('option-users/{id}/{groupId}', 'PollController@getOptionUsers');

### NoteController ###
Route::get('add-note/{id}', 'NoteController@getAddNote');
Route::post('add-note/{id}', 'NoteController@postAddNote');
Route::get('edit-note/{id}/{groupId}', 'NoteController@getEditNote');
Route::post('edit-note/{id}/{groupId}', 'NoteController@postEditNote');
Route::get('delete-note/{id}/{groupId}', 'NoteController@getDeleteNote');

### GroupDiscussionsController ###
Route::post('add-discussion', 'GroupDiscussionsController@storeDiscussion');
Route::get('delete-discussion/{id}/{groupId}', 'GroupDiscussionsController@deleteDiscussion');
Route::post('edit-discussion', 'GroupDiscussionsController@updateDiscussion');

### GroupSubDiscussionsController ###
Route::post('add-subdiscussion', 'GroupSubDiscussionsController@storeSubDiscussion');
Route::get('delete-subdiscussion/{id}/{groupId}', 'GroupSubDiscussionsController@deleteSubDiscussion');
Route::post('edit-subdiscussion', 'GroupSubDiscussionsController@updateSubDiscussion');

### DiscussionsController ###
//Route::get('friend-discussion/{id}','DiscussionController@getFriendDiscussion');
Route::post('create-discussion','DiscussionController@postCreateDiscussion');
Route::get('user-discussion','DiscussionController@getUserDiscussion');
Route::post('start-chat','DiscussionController@postStartChat');
Route::post('send-measege','DiscussionController@postSendMeasege');
Route::post('reject-discussion','DiscussionController@postRejectDiscussion');
Route::get('remove-discussion/{id}','DiscussionController@getRemoveDiscussion');
