<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupSubDiscussion extends Model
{
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
  		'discussion_id',
  		'message',
    ];
    //protected $table = 'grоup_sub_discussions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
