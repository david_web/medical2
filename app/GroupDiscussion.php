<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDiscussion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
  		'group_id',
  		'message',
    ];
    protected $table = 'grоup_discussions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subDiscussions()
    {
    	return $this->hasMany('App\GroupSubDiscussion','discussion_id');
    }
}
