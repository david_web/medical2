<?php

namespace App\Policies;

use App\User;
use App\Discussion;
use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;

class DiscussionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the discussion.
     *
     * @param  \App\User  $user
     * @param  \App\Discussion  $discussion
     * @return mixed
     */
    public function view(User $user, Discussion $discussion)
    {
        $discussion = Discussion::find($discussion->id)->with(['discussions' => function($query) use ($user) {
        $query->where('user_discussions.user_id', $user->id);
        }])->first();
        
       // dd($discussion->id);
        // $discussionOne = Discussion::where('user_id', $user->id)->where('id', $discussion->id)->first();
        // $discussionTwo = Discussion::where('user_id', Auth::user()->id)->where('id', $discussion->id)->first();
        if( $discussion) {
            return true;
        }

       // $discussionOne->
    }

    /**
     * Determine whether the user can create discussions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user,Discussion $discussion)
    {

       return $user->id === $user->followers->follower_id;
    }

    /**
     * Determine whether the user can update the discussion.
     *
     * @param  \App\User  $user
     * @param  \App\Discussion  $discussion
     * @return mixed
     */
    public function update(User $user, Discussion $discussion)
    {
        return $user->id === $discussion->user_id;
    }

    /**
     * Determine whether the user can delete the discussion.
     *
     * @param  \App\User  $user
     * @param  \App\Discussion  $discussion
     * @return mixed
     */
    public function delete(User $user, Discussion $discussion)
    {
        return $user->id === $discussion->user_id;
    }
}
