<?php

namespace App\Policies;

use App\User;
use App\SubDiscussion;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubDiscussionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the subDiscussion.
     *
     * @param  \App\User  $user
     * @param  \App\SubDiscussion  $subDiscussion
     * @return mixed
     */
    public function view(User $user, SubDiscussion $subDiscussion)
    {
        //
    }

    /**
     * Determine whether the user can create subDiscussions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the subDiscussion.
     *
     * @param  \App\User  $user
     * @param  \App\SubDiscussion  $subDiscussion
     * @return mixed
     */
    public function update(User $user, SubDiscussion $subDiscussion)
    {
        return $user->id === $subDiscussion->user_id;
    }

    /**
     * Determine whether the user can delete the subDiscussion.
     *
     * @param  \App\User  $user
     * @param  \App\SubDiscussion  $subDiscussion
     * @return mixed
     */
    public function delete(User $user, SubDiscussion $subDiscussion)
    {
        return $user->id === $subDiscussion->user_id;
    }
}
