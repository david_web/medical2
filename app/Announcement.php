<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'group_id',
        'title',
        'date',
        'time',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'announcement_users', 'announcement_id', 'user_id');
    }
}
