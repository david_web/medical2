<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'password', 
        'first_name', 
        'last_name', 
        'other_name', 
        'date_of_birth', 
        'nationality',
        'path', 
        'residence', 
        'profession_id', 
        'other_profession', 
        'place_of_work_or_study', 
        'institution_name', 
        'mobile_number', 
        'whatsapp', 
        'facebook', 
        'vk', 
        'viber', 
        'is_validated',
        'verify_token', 
        'points',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'user_groups', 'user_id');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'followers', 'user_id', 'follower_id')->withPivot(['accepted']);
    }

    public function options()
    {
        return $this->belongsToMany('App\Option', 'user_options', 'user_id', 'option_id');
    }
    public function discussions()
    {
        return $this->belongsToMany('App\Discussion','user_discussions','user_id','discussion_id')->withPivot(['accepted']);
    }
    public function userMeasege()
    {
        return $this->hasMany('App\Measege');
    }
    
}
