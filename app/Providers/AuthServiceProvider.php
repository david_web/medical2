<?php

namespace App\Providers;

use App\Question;
use App\Policies\QuestionPolicy;
use App\Group;
use App\Policies\GroupPolicy;
use App\Folder;
use App\Policies\FolderPolicy;
use App\File;
use App\Policies\FilePolicy;
use App\User;
use App\Policies\UserPolicy;
use App\Announcement;
use App\Policies\AnnouncementPolicy;
use App\Poll;
use App\Policies\PollPolicy;
use App\Note;
use App\Policies\NotePolicy;
use App\Discussion;
use App\Policies\DiscussionPolicy;
use App\GroupDiscussion;
use App\Policies\GroupDiscussionPolicy;
use App\SubDiscussion;
use App\Policies\SubDiscussionPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Question::class => QuestionPolicy::class,
        Group::class => GroupPolicy::class,
        Folder::class => FolderPolicy::class,
        File::class => FilePolicy::class,
        User::class => UserPolicy::class,
        Announcement::class => AnnouncementPolicy::class,
        Poll::class => PollPolicy::class,
        Note::class => NotePolicy::class,
        Discussion::class => DiscussionPolicy::class,
        SubDiscussion::class => SubDiscussionPolicy::class,
        GroupDiscussion::class => GroupDiscussionPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
