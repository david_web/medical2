<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;

use App\Http\Validator\CustomValidationRule;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register custom validation rule.
        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new CustomValidationRule($translator, $data, $rules, $messages);
        });
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\UsersInterface',
            'App\Services\UsersService'
        );

        $this->app->bind(
            'App\Contracts\ProfessionsInterface',
            'App\Services\ProfessionsService'
        );

        $this->app->bind(
            'App\Contracts\QuestionsInterface',
            'App\Services\QuestionsService'
        );

        $this->app->bind(
            'App\Contracts\TagsInterface',
            'App\Services\TagsService'
        );

        $this->app->bind(
            'App\Contracts\CommentsInterface',
            'App\Services\CommentsService'
        );

        $this->app->bind(
            'App\Contracts\FilesInterface',
            'App\Services\FilesService'
        );

        $this->app->bind(
            'App\Contracts\FoldersInterface',
            'App\Services\FoldersService'
        );

        $this->app->bind(
            'App\Contracts\GroupsInterface',
            'App\Services\GroupsService'
        );

        $this->app->bind(
            'App\Contracts\FollowersInterface',
            'App\Services\FollowersService'
        );

        $this->app->bind(
            'App\Contracts\AnnouncementsInterface',
            'App\Services\AnnouncementsService'
        );

        $this->app->bind(
            'App\Contracts\PollsInterface',
            'App\Services\PollsService'
        );

        $this->app->bind(
            'App\Contracts\OptionsInterface',
            'App\Services\OptionsService'
        );

        $this->app->bind(
            'App\Contracts\GroupDiscussionsInterface',
            'App\Services\GroupDiscussionsService'
        );

        $this->app->bind(
            'App\Contracts\GroupSubDiscussionsInterface',
            'App\Services\GroupSubDiscussionsService'
        );

        $this->app->bind(
            'App\Contracts\NotesInterface',
            'App\Services\NotesService'
        );
        $this->app->bind(
             'App\Contracts\FrendsDiscussionsInterface',
            'App\Services\FrendsDiscussionsService'
        );
        $this->app->bind(
             'App\Contracts\MeasegesInterface', 
            'App\Services\MeasegesService'
        );
        $this->app->bind(
            'App\Contracts\DiscussionsInterface',
            'App\Services\DiscussionsService'
        );
         
    }
}