<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
  		'comment'
    ];

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'question_comments', 'comment_id', 'question_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function files()
    {
        return $this->belongsToMany('App\File', 'file_comments', 'comment_id', 'file_id');
    }
}
