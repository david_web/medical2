<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $fillable = [
        'name',
        'user_id',
  		'is_public',
  		'accepted'
    ];
    
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_discussions', 'discussion_id', 'user_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function measeges()
    {
        return $this->hasMany('App\Measege');
    }

    public function discussions()
    {
        return $this->belongsToMany('App\User','user_discussions','user_id','discussion_id')->withPivot(['accepted']);
    }
    
}
