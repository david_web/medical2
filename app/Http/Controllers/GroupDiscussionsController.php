<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contracts\GroupDiscussionsInterface;
use App\Contracts\GroupsInterface;

use Auth;

class GroupDiscussionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function storeDiscussion(Request $request, GroupsInterface $groupRepo, GroupDiscussionsInterface $discussionRepo)
    {
    	$data = $request->all();
        $group = $groupRepo->getGroupWithUsersById($data['group_id']);
        $user = Auth::user();
        if ($user->can('view', $group)) {
        	$data['user_id'] = Auth::id();
        	$discussionRepo->add($data);
        }
        return redirect()->back();
    }

    public function deleteDiscussion($id, $groupId, GroupsInterface $groupRepo, GroupDiscussionsInterface $discussionRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $discussion = $discussionRepo->getDiscussionById($id);
        if ($user->can('update', $group) || $user->can('delete', $discussion)) {
            foreach($discussion->subDiscussions as $subDiscussion) {
                $subDiscussion->delete();
            }
            $discussion->delete();
        }
        return redirect()->back();
    }

    public function updateDiscussion(Request $request, GroupsInterface $groupRepo, GroupDiscussionsInterface $discussionRepo)
    {
        $data = $request->all();
        $group = $groupRepo->getGroupWithAdminsAndManagers($data['group_id']);
        $user = Auth::user();
        $discussion = $discussionRepo->getDiscussionById($data['discussion_id']);
        if ($user->can('update', $group) || $user->can('update', $discussion)) {
            $discussionRepo->update($data['discussion_id'], ['message' => $data['message']]);
        }
        return redirect()->back();
    }
}
