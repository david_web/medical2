<?php

namespace App\Http\Controllers;

use App\Contracts\FilesInterface;
use App\Contracts\FoldersInterface;
use App\Contracts\GroupsInterface;

use Illuminate\Http\Request;
use App\Http\Requests\AddFolderRequest;

use Auth;

class FolderController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }
    
    public function getChildFolder($id, $groupId=null, FoldersInterface $folderRepo, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        $user = Auth::user();
        $folder = $folderRepo->getFolderById($id);
        $group = $groupRepo->getGroupWithUsersById($groupId);
        if ($user->can('view', $folder) || $user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($groupId);
            $filesData = $fileRepo->getFilesByFolderId($id);
            $foldersData = $folderRepo->getFoldersByParentId($id);
            foreach ($foldersData as $folderData) {
                foreach($folderData->parent_folders as $child){
                    $id = $child['id'];
                    $file = $fileRepo->getFilesByFolderId($id);
                    $child['fileCount'] = count($file);
                };
            };
            $data = [
                'foldersData' => $foldersData,
                'filesData' => $filesData,
                'groupData' => $groupData,
                'folder' => $folder
            ];
            $html = view('files.child-folder', $data)->render();
            if($data) {
                return response()->json(['success' => true, 'html' => $html, 'name' => $folder->name]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getParentFolders(FoldersInterface $folderRepo, FilesInterface $fileRepo)
    {
        $filesData = $fileRepo->getFilesByUserId(Auth::id());
        $foldersData = $folderRepo->getFoldersById(Auth::id());
        foreach ($foldersData as $folderData) {
            $id = $folderData['id'];
            $file = $fileRepo->getFilesByFolderId($id);
            $folderData['fileCount'] = count($file);
        };
        $data = [
            'foldersData' => $foldersData,
            'filesData' => $filesData,
            'groupData' => null
        ];
        $html = view('files.parent-folder', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }

    public function getModalChildFolder($id, FoldersInterface $folderRepo)
    {
        $foldersData = $folderRepo->getFoldersByParentId($id);
        $data = [
            'foldersData' => $foldersData
        ];
        $html = view('files.modal-child-folder', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);

    }

    public function getModalParentFolders(FoldersInterface $folderRepo)
    {
        $foldersData = $folderRepo->getFoldersById(Auth::id());
        $data = [
            'foldersData' => $foldersData
        ];
        $html = view('files.modal-parent-folder', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);

    }

    public function postAddFolder(AddFolderRequest $request, FoldersInterface $folderRepo, $id = null)
    {
        $data = $request->all();
        if($id == null) {
            $data['user_id'] = Auth::id();
            $data['parent_user_id'] = Auth::id();
            $folder = $folderRepo->add($data);
        } else {
            $data['parent_user_id'] = Auth::id();
            $folder = $folderRepo->add($data);
            $folder->child_folders()->attach($id);
        }
        $data = [
            'folder' => $folder
        ];
        $html = view('files.add-folder', $data)->render();
        if($folder) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }

    public function getDeleteFolder($id, $groupId=null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $user = Auth::user();
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $folder = $folderRepo->getFolderById($id);
        if ($user->can('update', $group) || $user->can('delete', $folder)) {
            $folderData = $folderRepo->deleteFolderById($id);
            if($folderData) {
                return response()->json(['success' => true, 'folderData' => $folderData]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getGroupParentFolders($id, FoldersInterface $folderRepo, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        $foldersData = $folderRepo->getFoldersByGroupId($id);
        $filesData = $fileRepo->getFilesByGroupId($id);
        $groupData = $groupRepo->getGroupWithUsersById($id);
        foreach ($foldersData as $folderData) {
            $id = $folderData['id'];
            $file = $fileRepo->getFilesByFolderId($id);
            $folderData['fileCount'] = count($file);
        };
        $data = [
            'foldersData' => $foldersData,
            'filesData' => $filesData,
            'groupData' => $groupData
        ];
        $html = view('files.parent-folder', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }

    public function getGroupModalParentFolders($id, FoldersInterface $folderRepo)
    {
        $foldersData = $folderRepo->getFoldersByGroupId($id);
        $data = [
            'foldersData' => $foldersData
        ];
        $html = view('files.modal-parent-folder', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);

    }

    public function postAddGroupFolder(AddFolderRequest $request, GroupsInterface $groupRepo, FoldersInterface $folderRepo, $id = null)
    {
        $data = $request->all();
        if($data['action'] == 'group') {
            $group = $groupRepo->getGroupWithAdminsAndManagers($id);
            $user = Auth::user();
            if ($user->can('update', $group)) {
                $data['user_id'] = Auth::id();
                $data['group_id'] = $id;
                $folder = $folderRepo->add($data);
            } else {
                return redirect()->back();
            }
        } else {
            $folder = $folderRepo->add($data);
            $folder->child_folders()->attach($id);
        }
        $data = [
            'folder' => $folder
        ];
        $html = view('files.add-folder', $data)->render();
        if($folder) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }
}
