<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PollRequest;

use App\Contracts\GroupsInterface;
use App\Contracts\PollsInterface;
use App\Contracts\OptionsInterface;

use Auth;

class PollController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getAddPoll($id, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $html = view('polls.modal-poll')->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function postAddPoll($id, PollRequest $request, GroupsInterface $groupRepo, PollsInterface $pollRepo, OptionsInterface $optionRepo)
    {
    	$group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
	    	$data = $request->all();
            $optionsCount = 0;
            foreach($data['options'] as $option) {
                if ($option !== null) $optionsCount += 1;
            }
            if ($optionsCount < 2) {
                return response()->json(['success' => false]);
            } else {
                $data['group_id'] = $id;
                $data['user_id'] = Auth::id();
                $poll = $pollRepo->add($data);
                foreach($data['options'] as $option) {
                    if ($option !== null) {
                        $optionData['poll_id'] = $poll->id;
                        $optionData['name'] = $option;
                        $option = $optionRepo->add($optionData);
                    }
                }

                $polls = $pollRepo->getPollsByGroupId($id);
                foreach($polls as $poll) {
                    $sum = 0;
                    foreach($poll->options as $option) {
                        $sum += count($option->users);
                    }
                    $poll['users_count'] = $sum;
                }
                $data = [
                    'polls' => $polls
                ];
                $html = view('polls.polls', $data)->render();
                return response()->json(['success' => true, 'html' => $html]);
            }
        }
        return redirect()->back();
    }

    public function getChooseOption($id, $groupId, GroupsInterface $groupRepo, OptionsInterface $optionRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($groupId);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $user = Auth::user();
            $user->options()->attach($id);

            $option = $optionRepo->getOptionById($id);

            $sum = 0;
            foreach($option->poll->options as $option) {
                $sum += count($option->users);
            }
            $pollUsersCount = $sum;
            $data = [
                'option' => $option,
                'pollUsersCount' => $pollUsersCount
            ];
            $html = view('polls.selected-poll', $data)->render();
            $pollId = $option->poll->id;
            return response()->json(['success' => true, 'html' => $html, 'pollId' => $pollId]);
        }
        return redirect()->back();
    }

    public function getDeletePoll($id, $groupId, PollsInterface $pollRepo, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $poll = $pollRepo->getPollById($id);
        if ($user->can('update', $group) || $user->can('delete', $poll)) {
            foreach($poll->options as $option) {
                $option->users()->detach();
                $option->delete();
            }
            $poll->delete();

            $polls = $pollRepo->getPollsByGroupId($groupId);
            foreach($polls as $poll) {
                $sum = 0;
                foreach($poll->options as $option) {
                    $sum += count($option->users);
                }
                $poll['users_count'] = $sum;
            }

            $data = [
                'polls' => $polls
            ];
            $html = view('polls.polls', $data)->render();
            if($poll) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getOptionUsers($id, $groupId, GroupsInterface $groupRepo, OptionsInterface $optionRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($groupId);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $option = $optionRepo->getOptionById($id);
            if ($option->poll['is_public'] == 1) {
                $data = [
                    'users' => $option->users
                ];
                $html = view('polls.modal-option-users', $data)->render();
                if($html) {
                    return response()->json(['success' => true, 'html' => $html]);
                }
                return response()->json(['success' => false]);
            }
        }
        return redirect()->back();
    }
}
