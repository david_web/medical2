<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AnnouncementRequest;

use App\Contracts\AnnouncementsInterface;
use App\Contracts\GroupsInterface;

use Auth;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getAddAnnouncement($id, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupUsers = [];
            foreach ($group->users as $user) {
                if ($user->id != Auth::id()) $groupUsers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };

            $data = [
                'action' => 'add',
                'groupUsers' => $groupUsers
            ];
            $html = view('announcements.modal-announcement', $data)->render();
            if($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function postAddAnnouncement(AnnouncementRequest $request, AnnouncementsInterface $announcementRepo, GroupsInterface $groupRepo)
    {
	    $data = $request->all();
	    $id = $data['group_id'];
    	$group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
	    	$data['user_id'] = Auth::id();
	    	$users_id = json_decode($data['users']);
	    	$data['date'] = new \Carbon\Carbon($data['date']);
	    	$data['time'] = new \Carbon\Carbon($data['time']);
	    	$announcement = $announcementRepo->add($data);

	    	if ($announcement) {
	    		foreach ($users_id as $user_id) {
	    			$announcement->users()->attach($user_id);
	       		}

                $announcements = $announcementRepo->getAnnouncementsByGroupId($id);
	       		$html = view('announcements.announcement', ['announcements' => $announcements, 'group' => $group])->render();
	            return response()->json(['success' => true, 'html' => $html]);
	    	}
	        return response()->json(['success' => false]);
	    }
	    return redirect()->back();
    }

    public function getDeleteAnnouncement($id, $groupId, AnnouncementsInterface $announcementRepo, GroupsInterface $groupRepo)
    {
    	$group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
	    $announcement = $announcementRepo->getAnnouncementById($id);
    	$user = Auth::user();
    	if ($user->can('delete', $announcement) || $user->can('update', $group)) {
    		foreach ($announcement->users as $user) {
    			$announcement->users()->detach($user->id);
    		}
    		$announcement = $announcementRepo->delete($id);
            if ($announcement == true) {
                return response()->json(['success' => true, 'id' => $id]);
            }
            return response()->json(['success' => false]);
    	} 
	    return redirect()->back();
    }

    public function getEditAnnouncement($id, $groupId, AnnouncementsInterface $announcementRepo, GroupsInterface $groupRepo)
    {
    	$group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
	    $announcement = $announcementRepo->getAnnouncementById($id);
    	$user = Auth::user();
    	if ($user->can('update', $announcement) || $user->can('update', $group)) {
    		$group = $groupRepo->getGroupById($groupId);
    		$groupUsers = [];
            foreach ($group->users as $user) {
                if ($user->id != Auth::id()) $groupUsers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };

            $selUsers = [];
            foreach ($announcement->users as $user) {
                array_push($selUsers, $user->id);
            }
            $announcement->user_id = $selUsers;


    		$data = [
    			'announcement' => $announcement,
    			'groupUsers' => $groupUsers,
    			'groupId' => $groupId,
                'action' => 'edit'
    		];

    		$html = view('announcements.modal-announcement', $data)->render();
            if ($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
    	}
    	return redirect()->back();
    }

    public function postEditAnnouncement($id, $groupId, AnnouncementRequest $request, AnnouncementsInterface $announcementRepo, GroupsInterface $groupRepo)
    {
    	$group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
	    $announcement = $announcementRepo->getAnnouncementById($id);
    	$user = Auth::user();
    	if ($user->can('update', $announcement) || $user->can('update', $group)) {
    		$data = $request->all();
    		$users_id = json_decode($data['users']);
    		$announcement = $announcementRepo->edit($id, $data);

    		if ($announcement == true) {
	    		$announcement = $announcementRepo->getAnnouncementById($id);
	    		$announcement->users()->detach();
	    		foreach ($users_id as $user_id) {
	    			$announcement->users()->attach($user_id);
	       		}
	       		$announcements = $announcementRepo->getAnnouncementsByGroupId($groupId);
	       		$html = view('announcements.announcement', ['announcements' => $announcements, 'group' => $group])->render();
	       		return response()->json(['success' => true, 'html' => $html]);
	       	}
	       	return response()->json(['success' => false]);
    	}
    	return redirect()->back();
    }
}
