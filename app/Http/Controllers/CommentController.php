<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CommentRequest;

use App\Contracts\CommentsInterface;

use Auth;

class CommentController extends Controller
{
	public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function postAddComment(CommentRequest $request, CommentsInterface $commentRepo)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $id = $request->id;
        $comment = $commentRepo->add($data);
        $comment->user;
        $comment->questions()->attach($id);
        if($comment) {
            return response()->json(['success' => true, 'comment' => $comment]);
        }
        return response()->json(['success' => false]);
    }

    public function getDeleteComment($id, CommentsInterface $commentRepo)
    {
    	$comment = $commentRepo->delete($id);
    	if($comment) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function getEditComment($id, CommentsInterface $commentRepo)
    {
        $comment = $commentRepo->getCommentById($id);
        return redirect()->back();
    }

    public function postAddFileComment(CommentRequest $request, CommentsInterface $commentRepo)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $id = $request->id;
        $comment = $commentRepo->add($data);
        $comment->user;
        $comment->files()->attach($id);
        if($comment) {
            return response()->json(['success' => true, 'comment' => $comment]);
        }
        return response()->json(['success' => false]);
    }
}
