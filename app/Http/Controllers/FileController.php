<?php

namespace App\Http\Controllers;

use App\Contracts\FilesInterface;
use App\Contracts\FoldersInterface;
use App\Contracts\GroupsInterface;
use App\Contracts\UsersInterface;

use Illuminate\Http\Request;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\AddFolderRequest;
use App\Http\Requests\EditFileRequest;

use Auth;
use File;

class FileController extends Controller
{
	public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getFileUpload()
    {
        $countries = config('countries');
        return view('files.main-file-upload', ['countries' => $countries]);
    }

    public function postFileUpload(FileUploadRequest $request, FilesInterface $fileRepo)
    {
        $data = $request->except(['file']);
        $data['user_id'] = Auth::id();
        $data['is_public'] = 1;

        $uploadFile = $request->file('file');
        $extension = $uploadFile->getClientOriginalExtension();
        $newName = str_random().'.'.$extension;
        $uploadPath = public_path() . '/assets/uploads/';
        $uploadFile->move($uploadPath, $newName);
        $size = getimagesize($uploadPath.$newName);
        $filesize = filesize($uploadPath.$newName);
        $data['path'] = $newName;
        $data['type'] = $extension;

        $filesData = $fileRepo->add($data);
        return redirect()->action('FileController@getMaterials');
    }

    public function getUserFileUpload(FoldersInterface $folderRepo, UsersInterface $userRepo)
    {
        $countries = config('countries');
        $foldersData = $folderRepo->getFoldersById(Auth::id());
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
        $data = [
            'foldersData' => $foldersData,
            'countries' => $countries,
            'friendRequests' => $userFriendRequests,

        ];
        return view('users.file-upload', $data);
    }

    public function postUserFileUpload(FileUploadRequest $request, FilesInterface $fileRepo)
    {
        $data = $request->except(['file']);
        $data['user_id'] = Auth::id();

        $uploadFile = $request->file('file');
        $extension = $uploadFile->getClientOriginalExtension();
        $newName = str_random().'.'.$extension;
        $uploadPath = public_path() . '/assets/uploads/';
        $uploadFile->move($uploadPath, $newName);
        $size = getimagesize($uploadPath.$newName);
        $filesize = filesize($uploadPath.$newName);
        $data['path'] = $newName;
        $data['type'] = $extension;

        $file = $fileRepo->add($data);
        return redirect()->action('FileController@getMyLibrary');
    }

    public function postGroupFileUpload($id, FileUploadRequest $request, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $data = $request->except(['file']);
            $data['user_id'] = Auth::id();
            $data['group_id'] = $id;

            $uploadFile = $request->file('file');
            $extension = $uploadFile->getClientOriginalExtension();
            $newName = str_random().'.'.$extension;
            $uploadPath = public_path() . '/assets/uploads/';
            $uploadFile->move($uploadPath, $newName);
            $size = getimagesize($uploadPath.$newName);
            $filesize = filesize($uploadPath.$newName);
            $data['path'] = $newName;
            $data['type'] = $extension;
        
            $file = $fileRepo->add($data);
        }
        return redirect()->back();
    }

    public function getMaterials(FilesInterface $fileRepo)
    {
    	$filesData = $fileRepo->getPublicFilesWithComments();
        foreach ($filesData as $key => $fileData) {
            $id = $fileData->user->id;
            $filesCount = $fileRepo->getFilesCount($id);
            $filesData[$key]->filesCount = $filesCount;
        }
    	return view('files.materials', compact('filesData'));
    }

    public function getMyLibrary(FoldersInterface $folderRepo, FilesInterface $fileRepo, UsersInterface $userRepo)
    {
        $filesData = $fileRepo->getFilesByUserId(Auth::id());
        $foldersData = $folderRepo->getFoldersById(Auth::id());
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
        foreach ($foldersData as $folderData) {
            $id = $folderData['id'];
            $file = $fileRepo->getFilesByFolderId($id);
            $folderData['fileCount'] = count($file);
        };
        $data = [
            'foldersData' => $foldersData,
            'filesData' => $filesData,
            'friendRequests' => $userFriendRequests,
        ];
        return view('files.mylibrary', $data);
    }

    public function getAddFile($id, Request $request, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($id);
        $user = Auth::user();
        if ($user->can('update', $group)) {
            $data = $request->all();
            $countries = config('countries');
            $dataRend = [
                'countries' => $countries,
                'groupId' => $group ? $group->id : null,
                'action' => 'add',
                'view' => $data['view']
            ];
            $html = view('files.modal-file', $dataRend)->render();
            if ($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getEditFile($id, $groupId = null, Request $request, FilesInterface $fileRepo, FoldersInterface $folderRepo, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $file = $fileRepo->getFileById($id);
        if ($user->can('update', $group) || $user->can('update', $file)) {
            $data = $request->all();
            $fileData = $fileRepo->getFileById($id);
            $countries = config('countries');
            $folder = $folderRepo->getFolderById($fileData->folder_id);
            
            $dataRend = [
                'fileData' => $fileData,
                'countries' => $countries,
                'name' => $folder ? $folder->name : null,
                'groupId' => $group ? $group->id : null,
                'action' => 'edit',
                'view' => $data['view']
            ];
            $html = view('files.modal-file', $dataRend)->render();
            if ($fileData) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function postEditFile($id, $groupId=null, EditFileRequest $request, FoldersInterface $folderRepo, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $file = $fileRepo->getFileById($id);
        if ($user->can('update', $group) || $user->can('update', $file)) {
            $data = $request->except('folderId');
            $file = $fileRepo->edit($data, $id);
            $dataFolderId = $request->only('folderId');
            $folderId = $dataFolderId['folderId'];

            if (!$folderId) {
                if ($groupId) {
                    if(!$data['file_type']) {
                        $filesData = $group->files->sortByDesc('id');
                    } elseif($data['file_type'] == 'pdf') {
                        $filesData = $group->files->where('type', 'pdf')->sortByDesc('id');
                    } elseif($data['file_type'] == 'txt') {
                        $filesData = $group->files->where('type', 'txt')->sortByDesc('id');
                    } elseif($data['file_type'] == 'docs') {
                        $filesData = $group->files->where('type', 'docs')->sortByDesc('id');
                    } elseif($data['file_type'] == 'ppt') {
                        $filesData = $group->files->where('type', 'ppt')->sortByDesc('id');
                    } elseif($data['file_type'] == 'video') {
                        $types = ['mp4', 'm4v', 'mov', 'mpg', 'mpeg', 'asf', 'avi'];
                        $filesData = $group->files->whereIn('type', $types)->sortByDesc('id');
                    } elseif($data['file_type'] == 'photo') {
                        $types = ['jpg', 'jpeg', 'png'];
                        $filesData = $group->files->whereIn('type', $types)->sortByDesc('id');
                    } elseif($data['file_type'] == 'audio') {
                        $filesData = $group->files->where('type', 'mp3')->sortByDesc('id');
                    }
                } elseif($data['action']) {
                    $filesData = $fileRepo->getPublicFilesWithComments();
                    foreach ($filesData as $key => $fileData) {
                        $userId = $fileData->user->id;
                        $filesCount = $fileRepo->getFilesCount($userId);
                        $filesData[$key]->filesCount = $filesCount;
                    }
                } else {
                    $filesData = $fileRepo->getFilesByUserId(Auth::id());
                }
            } else {
                $folder = $folderRepo->getFolderById($folderId);
                if ($groupId) {
                    if(!$data['file_type']) {
                        $filesData = $folder->files->sortByDesc('id');
                    } elseif($data['file_type'] == 'pdf') {
                        $filesData = $folder->files->where('type', 'pdf')->sortByDesc('id');
                    } elseif($data['file_type'] == 'txt') {
                        $filesData = $folder->files->where('type', 'txt')->sortByDesc('id');
                    } elseif($data['file_type'] == 'docs') {
                        $filesData = $folder->files->where('type', 'docs')->sortByDesc('id');
                    } elseif($data['file_type'] == 'ppt') {
                        $filesData = $folder->files->where('type', 'ppt')->sortByDesc('id');
                    } elseif($data['file_type'] == 'video') {
                        $types = ['mp4', 'm4v', 'mov', 'mpg', 'mpeg', 'asf', 'avi'];
                        $filesData = $folder->files->whereIn('type', $types)->sortByDesc('id');
                    } elseif($data['file_type'] == 'photo') {
                        $types = ['jpg', 'jpeg', 'png'];
                        $filesData = $folder->files->whereIn('type', $types)->sortByDesc('id');
                    } elseif($data['file_type'] == 'audio') {
                        $filesData = $folder->files->where('type', 'mp3')->sortByDesc('id');
                    }
                } else {
                    $filesData = $folder->files->sortByDesc('id');
                }
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($groupId);
            $rendData = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            if($data['action']) {
                $html = view('files.materials-files', $rendData)->render();
            } else {
                $html = view('files.files', $rendData)->render();
            }
            if($file == true) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getDeleteFile($id, $groupId = null, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $file = $fileRepo->getFileById($id);
        if ($user->can('update', $group) || $user->can('delete', $file)) {
            if($file) File::delete(public_path().'/assets/uploads/'.$file->path);
            foreach ($file->comments as $comment) {
                $comment->delete();
            }
            $file->comments()->detach();
            $file = $fileRepo->deleteFileById($id);
            if($file == true) {
                return response()->json(['success' => true, 'id' => $id]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getAllFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->sortByDesc('id');
            } else {
                $filesData = $group->files->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getPdfFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->where('type', 'pdf')->sortByDesc('id');
            } else {
                $filesData = $group->files->where('type', 'pdf')->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getTxtFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->where('type', 'txt')->sortByDesc('id');
            } else {
                $filesData = $group->files->where('type', 'txt')->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getDocsFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->where('type', 'docs')->sortByDesc('id');
            } else {
                $filesData = $group->files->where('type', 'docs')->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getPptFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $filesData = $group->files->where('type', 'ppt')->sortByDesc('id');
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getVideoFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $types = ['mp4', 'm4v', 'mov', 'mpg', 'mpeg', 'asf', 'avi'];
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->whereIn('type', $types)->sortByDesc('id');
            } else {
                $filesData = $group->files->whereIn('type', $types)->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getPhotoFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $types = ['jpg', 'jpeg', 'png'];
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->whereIn('type', $types)->sortByDesc('id');
            } else {
                $filesData = $group->files->whereIn('type', $types)->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }

    public function getAudioFiles($id, $folderId = null, GroupsInterface $groupRepo, FoldersInterface $folderRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            if($folderId) {
                $folder = $folderRepo->getFolderById($folderId);
                $filesData = $folder->files->where('type', 'mp3')->sortByDesc('id');
            } else {
                $filesData = $group->files->where('type', 'mp3')->sortByDesc('id');
            }
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $data = [
                'filesData' => $filesData,
                'groupData' => $groupData
            ];
            $html = view('files.files', $data)->render();
            return response()->json(['success' => true, 'html' => $html]);
        }
        return redirect()->back();
    }
}