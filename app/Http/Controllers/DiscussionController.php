<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateDiscussionRequest;
use App\Http\Requests\MeasegeRequest;

use App\Contracts\DiscussionsInterface;
use App\Contracts\UsersInterface;
use App\Contracts\MeasegesInterface;

use Auth;

class DiscussionController extends Controller
{
	public function getFriendDiscussion($id, UsersInterface $userRepo)
	{
		$user = $userRepo->getUserById($id);
        $data = [
            'user' => $user,
            ];
		$html = view('users.friend-discussion',$data)->render();
	  return response()->json(['success' => true,'html' => $html]);
		

	}

	public function postCreateDiscussion( CreateDiscussionRequest $request, DiscussionsInterface $discussionRepo,Usersinterface $userRepo)
	{        
	
		$data = $request->all();
		$user = $userRepo->getUserById($data['user_id']);
		if(isset($data['public'])){
		  $array = [
			'user_id'=>Auth::id(),
			'name'=>$data['name'],
			'is_public'=> 1,
		  ];
		}else{
			$array = [
			'user_id'=>Auth::id(),
			'name'=>$data['name'],			
		  ];
		}
		$discussion = $discussionRepo->add($array);
		Auth::user()->discussions()->attach($discussion->id);
		$user->discussions()->attach($discussion->id);
		$html = view('users.friend-discussion')->render();
	    return response()->json(['success' => true,'html' => $html]);
		
		
   }

   public function getUserDiscussion(DiscussionsInterface $discussionRepo,Usersinterface $userRepo,MeasegesInterface $measegeRepo)
   {
		$html = view('users.user-chat')->render();
		return response()->json(['success' => true,'html' => $html]);
   }
   
   public function postStartChat(Request $request,DiscussionsInterface $discussionRepo,Usersinterface $userRepo)
   {
		$array = [
			'accepted'=>1,
		];
		$data = $request->all();
		$user = $userRepo->getUserById($data['user_id']);
		$user->discussions()->updateExistingPivot($data['id'],$array);
		Auth::user()->discussions()->updateExistingPivot($data['id'],$array);
		$id = $data['id'];
		return response()->json(['success' => true]);
	
   }
  
   public function postSendMeasege(MeasegeRequest $request,MeasegesInterface $measegeRepo)
   {
		$data = $request->all();
		$measege = $measegeRepo->add($data);
		return redirect()->back();	
   }
	
	public function postRejectDiscussion(Request $request,DiscussionsInterface $discussionRepo,Usersinterface $userRepo)
	   {
		$array = [
		'accepted'=>0,
		  ];
	    $data = $request->all();
		$user = $userRepo->getUserById($data['user_id']);
		$delete = $discussionRepo->deleteDiscussionById($data['id']);
		$user->discussions()->detach($data['id'],$array);
		Auth::user()->discussions()->detach($data['id'],$array);
		return response()->json(['success' => true]);

	   }
   
	public function getRemoveDiscussion($id,Request $request,DiscussionsInterface $discussionRepo,MeasegesInterface 
		$measegeRepo,Usersinterface $userRepo)
	   {
		$discussion = $discussionRepo->getDiscussionById($id);
		$discussion->users()->detach();
		$deleteMesege = $measegeRepo->deleteMeasegeById($id); 
		$delete = $discussionRepo->deleteDiscussionById($id);
		

		

	   }
}
