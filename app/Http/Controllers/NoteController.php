<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NoteRequest;

use App\Contracts\GroupsInterface;
use App\Contracts\NotesInterface;

use Auth;
use File;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getAddNote($id,  GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $data = [
                'action' => 'add',
                'name' => null,
                'content' => null
            ];
            $html = view('notes.modal-note', $data)->render();
            if($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function postAddNote($id, NoteRequest $request, GroupsInterface $groupRepo, NotesInterface $noteRepo)
    {
    	$group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
        	$data = $request->all();
        	$name = str_random() . '.txt';
        	$path = public_path() . '/assets/notes/' . $name;
        	$content = $data['content'];
        	if($content) File::put($path, $content);
        	$data['path'] = $name;
        	$data['group_id'] = $id;
        	$data['user_id'] = Auth::id();
        	$note = $noteRepo->add($data);

            $rendData = [
                'group' => $group,
            ];
            $html = view('notes.notes', $rendData)->render();
            if($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function getDeleteNote($id, $groupId, GroupsInterface $groupRepo, NotesInterface $noteRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $note = $noteRepo->getNoteById($id);
        if ($user->can('update', $group) || $user->can('update', $note)) {
            if($note) File::delete(public_path().'/assets/notes/'.$note->path);
            $note->delete();
            return response()->json(['success' => true, 'id' => $id]);
        }
        return redirect()->back();
    }

    public function getEditNote($id, $groupId, GroupsInterface $groupRepo, NotesInterface $noteRepo)
    {
    	$group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $note = $noteRepo->getNoteById($id);
        if ($user->can('update', $group) || $user->can('update', $note)) {
            $path = public_path() . '/assets/notes/' . $note->path;
            $content = File::get($path);

        	$data = [
                'action' => 'edit',
                'note' => $note,
                'name' => $note->folder ? $note->folder->name : null,
                'content' => $content
            ];

            $html = view('notes.modal-note', $data)->render();
            if($html) {
                return response()->json(['success' => true, 'html' => $html]);
            }
            return response()->json(['success' => false]);
        }
        return redirect()->back();
    }

    public function postEditNote($id, $groupId, NoteRequest $request, GroupsInterface $groupRepo, NotesInterface $noteRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $note = $noteRepo->getNoteById($id);
        if ($user->can('update', $group) || $user->can('update', $note)) {
            $data = $request->all();
            $name = $note['path'];
            $path = public_path() . '/assets/notes/' . $name;
            $content = $data['content'];
            if($content) File::put($path, $content);
            $note = $noteRepo->edit($id, $data);
            return response()->json(['success' => true, 'id' => $id]);
        }
        return redirect()->back();
    }
}
