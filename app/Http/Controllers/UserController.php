<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contracts\ProfessionsInterface;
use App\Contracts\UsersInterface;
use App\Contracts\FilesInterface;
use App\Contracts\GroupsInterface;
use App\Contracts\FrendsDiscussionsInterface;
use App\Contracts\MeasegesInterface;

 

use App\Http\Requests\EditUserRequest;
use App\Http\Requests\EditSettingRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\MeasegeRequest;

use Auth;
use Hash;
use File;
use App\User;
use App\FrendDiscussion;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getMyProfile(UsersInterface $userRepo, GroupsInterface $groupRepo, FilesInterface $fileRepo)
    {
        $user = $userRepo->getUserById(Auth::id());
        $files = $fileRepo->getFilesByUserId(Auth::id());
        $groups = $groupRepo->getUserGroups(Auth::id());
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
        $friends = $userRepo->getUserFriendsById(Auth::id());

        $data = [
            'user' => $user,
            'files' => $files,
            'groups' => $groups,
            'friendRequests' => $userFriendRequests,
            'friends'=>$friends,
        ];

        return view('users.my-profile', $data);
    }

    public function getMyFriends(UsersInterface $userRepo)
    {
        $userFriends = $userRepo->getUserFriendsById(Auth::id());
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());

        $data = [
            'friendRequests' => $userFriendRequests,
            'userFriends' => $userFriends
        ];
        return view('users.my-friends', $data);
    }

    public function getProfileInformation(ProfessionsInterface $professionRepo, UsersInterface $userRepo)
    {
    	$id = Auth::id();
    	$user = $userRepo->getUserById($id);
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
    	$professions = $professionRepo->getProfessions();
    	$allprofessions = [];
        $allprofessions[''] = 'Select a profession';
        foreach ($professions as $profession) {
            $allprofessions[$profession->id] = $profession->profession;
        };

    	$data = [
    		'user' => $user,
    		'allprofessions' => $allprofessions,
    		'countries' => config('countries'),
            'friendRequests' => $userFriendRequests,
    	];
    	return view('users.profile-information', $data);
    }

    public function postEditUserInformation(EditUserRequest $request, UsersInterface $userRepo)
    {
    	$data = $request->all();
        $user = Auth::user();
        if ($data['profession_id'] != 5) $data['other_profession'] = null;
    	$user = $userRepo->edit($data, Auth::id());
    	return redirect()->back();
    }

    public function postChangePassword(ChangePasswordRequest $request, UsersInterface $userRepo)
    {
    	$password = $request->current_password;
    	if (Hash::check($password, Auth::user()->password)) {
    		$data = $request->only('password');
    		$data['password'] = bcrypt($data['password']);
    		$password = $userRepo->edit($data, Auth::id());
    		return redirect()->back()->with(['success' => 'Password successfully changed!']);
        }
        else {
        	return redirect()->back()->with(['danger' => 'Password is wrong!']);
        }
    }

    public function postImageUpload(ImageUploadRequest $request, UsersInterface $userRepo)
    {
        $user = Auth::user();
        $data = $request->all();
        if ($request->file('image')){
            if($user->path) File::delete(public_path().'/assets/uploads/'.$user->path);
            $upload = $request->file('image');
            $extension = $upload->getClientOriginalExtension();
            $newName = str_random().'.'.$extension;
            $uploadPath = public_path() . '/assets/uploads/';
            $upload->move($uploadPath, $newName);
            $size = getimagesize($uploadPath.$newName);
            $filesize = filesize($uploadPath.$newName);
            $data['path'] = $newName;
        };
        $user = $userRepo->edit($data, Auth::id());
        return redirect()->back();
    }

    public function getDeleteImage(UsersInterface $userRepo)
    {
        $user = Auth::user();
        if ($user->path) File::delete(public_path().'/assets/uploads/'.$user->path);
        $data = ['path' => null];
        $user = $userRepo->edit($data, Auth::id());
        return redirect()->back();
    }

    public function getUserPage($id, UsersInterface $userRepo, FilesInterface $fileRepo, GroupsInterface $groupRepo)
    {
        if (Auth::id() == $id) {
            return redirect()->action('UserController@getMyProfile');
        } else {
            $user = Auth::user();
            $user2 = $userRepo->getUserById($id);
            if ($user->can('view', $user2)) {
                $files = $fileRepo->getFilesByUserId($id);
                $groups = $groupRepo->getUserGroups($id);
                $userFriendRequests = $userRepo->getUserFriendRequests($id);
                $userFriends = $userRepo->getUserFriendsById($id);

                $data = [
                    'user' => $user2,
                    'files' => $files,
                    'groups' => $groups,
                    'friendRequests' => $userFriendRequests,
                    'userFriends' => $userFriends
                ];
                return view('users.user-page', $data);
            } else {
                $userFriendRequests = $userRepo->getUserFriendRequests($id);
                $userFriends = $userRepo->getUserFriendsById($id);

                $data = [
                    'user' => $user2,
                    'friendRequests' => $userFriendRequests,
                    'userFriends' => $userFriends
                ];
                return view('users.user-page', $data);
            }
        }
    }

    public function getAddFriend($id)
    {
        $user = Auth::user();
        $user->followers()->attach($id);

        return response()->json(['success' => true, 'id' => $id]);
    }

    public function getFriendRequests(UsersInterface $userRepo)
    {
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
        $data = [
            'friendRequests' => $userFriendRequests,
        ];

        $html = view('users.friend-requests', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }

    public function getFriends(UsersInterface $userRepo)
    {
        $userFriends = $userRepo->getUserFriendsById(Auth::id());

        $data = [
            'userFriends' => $userFriends
        ];
        $html = view('users.friends', $data)->render();
        if($data) {
            return response()->json(['success' => true, 'html' => $html]);
        }
        return response()->json(['success' => false]);
    }

    public function getAcceptRequest($id, UsersInterface $userRepo)
    {
        $user = Auth::user();
        $user->followers()->attach($id, ['accepted' => 1]);

        $user = $userRepo->getUserById($id);
        $user->followers()->updateExistingPivot(Auth::id(), ['accepted' => 1]);
        return redirect()->back();
    }

    public function getRejectRequest($id, UsersInterface $userRepo)
    {
        $user = $userRepo->getUserById($id);
        $user->followers()->detach(Auth::id());
        return redirect()->back();
    }

    public function getRemoveFriend($id, UsersInterface $userRepo)
    {
        $user = $userRepo->getUserById($id);
        $user->followers()->detach(Auth::id());

        $user = Auth::user();
        $user->followers()->detach($id);
        return redirect()->back();
    }

    // public function getUserDiscussion(FrendsDiscussionsInterface $FrendDiscussionRepo)
    // { 

       
    //     $request = $FrendDiscussionRepo->getUserDiscussionRequest(Auth::id());
    //     $AcceptedChat = $FrendDiscussionRepo->getUserChat(Auth::id());
    //     $data = [
    //          'discussionRequest'=>$request,
    //          'AcceptedChats' => $AcceptedChat,
    //     ];
    //     ;
        
    //     $html = view('users.user-chat',$data)->render();
    //     return response()->json(['success' => true,'html' => $html]);
    // }
    public function getUserLibrary(UsersInterface $userRepo, GroupsInterface $groupRepo, FilesInterface $fileRepo)
    {   
        $user = $userRepo->getUserById(Auth::id());
        $files = $fileRepo->getFilesByUserId(Auth::id());
        $data = [
            'user' => $user,
            'files' => $files,
        ];
        $html = view('users.user-library',$data)->render();
        return response()->json(['success' => true,'html' => $html]);
    }
    public function getFriendDiscussion($id, UsersInterface $userRepo)
    {
        $user = $userRepo->getUserById($id);
        $data = [
            'user' => $user,
            ];
        $html = view('users.friend-discussion',$data)->render();
         return response()->json(['success' => true,'html' => $html]);
    }
    public function getFriendLibrary($id, UsersInterface $userRepo, GroupsInterface $groupRepo, FilesInterface $fileRepo)
    {
       
        $user = $userRepo->getUserById($id);
        $files = $fileRepo->getFilesByUserId(Auth::id());
        $data = [
            'user' => $user,
            'files' => $files,
            
        ];
        $html = view('users.friend-library',$data)->render();
         return response()->json(['success' => true,'html' => $html]);
    }
   
    // public function postStartChat(FrendsDiscussionsInterface $FrendDiscussionRepo,Request $request)
    // {   

    //    $data = $request->all();
       
    //    $id = $data['id'];
    //     $newArray = [
    //         'user_id'=>Auth::id(),
    //         'follower_id'=>$data['user_id'],
    //         'discussion_topic'=>$data['topic'],
    //         'private'=> 0,
    //         'expected'=> 1,
    //     ];
    //     $new = $FrendDiscussionRepo->createRow($newArray);

    //     $data = [
    //         'expected'=> 1,
    //     ];
    //     $expected = $FrendDiscussionRepo->getDiscussionById($id,$data);

    //     return response()->json(['success' => true]);
    // }
       
       // public function postSendMeasege($id,MeasegeRequest $request,MeasegesInterface $measegerepo)
       // {
       //    //dd($id);
       //     $data = $request->all();
       //     $array = [
       //      'user_id'=>Auth::id(),
       //      'frend_discussion_id'=> $id,
       //      'sms'=>$data['sms'],
       //     ];
       //     $measege =  $measegerepo->add($array);
       //     return redirect()->back();
       // }
    
}
