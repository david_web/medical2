<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\RegisterRequest;

use App\Contracts\UsersInterface;
use App\Contracts\ProfessionsInterface;

use App\Mail\VerificationMail;

use Mail;
use Auth;

class AuthController extends Controller
{
    public function getRegister(ProfessionsInterface $professionRepo)
    {

        $professions = $professionRepo->getProfessions();
        $allprofessions = [];
        $allprofessions[''] = 'Select a profession';
        foreach ($professions as $profession) {
            $allprofessions[$profession->id] = $profession->profession;
        };
        $data = [
            'countries' => config('countries'),
            'allprofessions' => $allprofessions
        ];
    	return view('auth.register', $data);
    }

    public function postRegister(RegisterRequest $request, UsersInterface $userRepo, ProfessionsInterface $professionRepo)
    {
    	$data = $request->all();
        $data['password'] = bcrypt($data['password']);
    	$data['date_of_birth'] = new \Carbon\Carbon($data['date_of_birth']);
        $data['verify_token'] = str_random(20);
    	$user = $userRepo->add($data);
        
        Mail::to($user->email)->send(new VerificationMail($user->verify_token));
    	return redirect()->action('AuthController@getLogin')->with(['success' => 'Check your email!']);
    }

    public function getLogin()
    {
    	return view('auth.login');
    }

    public function postLogin(Request $request)
    {
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_validated' => 1])) {
    		
    		return redirect()->action('AdminController@getDashboard');
        }
        else
        {
        	return redirect()->back()->with(['danger' => 'Try again!']);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->action('AuthController@getLogin');
    }

    public function getVerify($token, UsersInterface $userRepo)
    {
        $user = $userRepo->getUserByVerifyToken($token);
        if (!$user) return redirect()->action('AuthController@getLogin')->with(['danger' => 'E-mail confirmation is not passed']);
        $id = $user->id;
        $data = [
            'is_validated' => 1,
            'verify_token' => null
        ];
        $update = $userRepo->edit($data, $id);
        return redirect()->action('AuthController@getLogin')->with(['success' => 'E-mail confirmation is passed']);
    }
}
