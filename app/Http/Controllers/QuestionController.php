<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\QuestionRequest;

use App\Contracts\QuestionsInterface;
use App\Contracts\UsersInterface;
use App\Contracts\TagsInterface;

use Auth;

use App\Question;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getAddQuestion(TagsInterface $tagRepo)
	{
        $tags = $tagRepo->getTags();
        $alltags = [];
        foreach ($tags as $tag) {
            $alltags[$tag->id] = $tag->tag;
        };
        $data = [
            'action' => 'add',
            'alltags' => $alltags
        ];
        return view('questions.addquestion', $data);
	}

	public function postAddQuestion(QuestionRequest $request, QuestionsInterface $questionRepo)
    {
    	$data = $request->all();
        $data['keyword'] = str_replace(' ', '-', $data['title']);
    	$tags_id = json_decode($data['tags']);
       	$data['user_id'] = Auth::id();
    	$question = $questionRepo->add($data);
    	if ($question != null) {
    		foreach ($tags_id as $tag_id) {
    			$question->tags()->attach($tag_id);
       		}
    	}

    	return redirect()->action('QuestionController@getQuestions');
    }

    public function getQuestions(QuestionsInterface $questionRepo)
    {
        $questionsData = $questionRepo->getAllWithTags();
        return view('questions.questions', compact('questionsData'));
    }

    public function getQuestion(QuestionsInterface $questionRepo, $id)
    {
        $questionData = $questionRepo->getQuestionWithDetailsById($id);
        $likesCount = $questionRepo->getLikesCount($id);
        return view('questions.question', compact('questionData'), compact('likesCount'));
    }

    public function getEditQuestion($id, QuestionsInterface $questionRepo, TagsInterface $tagRepo)
    {
        $user = Auth::user();
        $question = $questionRepo->getQuestionById($id);
        if ($user->can('update', $question)) {
            $tags = $tagRepo->getTags();

            $alltags = [];
            foreach ($tags as $tag) {
                $alltags[$tag->id] = $tag->tag;
            };
            $selTags = [];
            foreach ($question->tags as $tag) {
                array_push($selTags, $tag->id);
            }
            $question->tag_id = $selTags;

            $data = [
                'question' => $question,
                'alltags' => $alltags,
                'action' => 'edit',
            ];
            return view('questions.addquestion', $data);
        } else {
            return redirect()->back()->with(['danger' => 'You can`t edit this question!']);
        };
    }

    public function postEditQuestion(QuestionRequest $request, $id, QuestionsInterface $questionRepo)
    {
        $user = Auth::user();
        $question = $questionRepo->getQuestionById($id);
        if ($user->can('update', $question)) {
            $data = $request->only('title', 'description', 'tags');
            $data['keyword'] = str_replace(' ', '-', $data['title']);
            $question = $questionRepo->edit($data, $id);
            $editedQuestion = $questionRepo->getQuestionWithTagsById($id);
            $editedQuestion->tags()->detach();
            $tags_id = json_decode($data['tags']);
            if ($question != null) {
                foreach ($tags_id as $tag_id) {
                    $editedQuestion->tags()->attach($tag_id);
                }
            };
            return redirect()->action('QuestionController@getQuestion', [$id, $data['keyword']])->with(['success' => 'Question was successfully edited!']);
        } else {
            return redirect()->back()->with(['danger' => 'You can`t edit this question!']);
        };
    }

    public function getDeleteQuestion($id, QuestionsInterface $questionRepo)
    {
        $user = Auth::user();
        $question = $questionRepo->getQuestionWithTagsById($id);
        if ($user->can('delete', $question)) {
            $question->tags()->detach();
            $question = $questionRepo->delete($id);
            return redirect()->action('QuestionController@getQuestions')->with(['success' => 'Question was successfully delated!']);
        } else {
                return redirect()->back()->with(['danger' => 'You can`t delete this question!']);
        };
    }

    public function getAddLike($id, QuestionsInterface $questionRepo)
    {
        $question = $questionRepo->getQuestionWithLikes($id);

        if(!$question) return response()->json(['success' => false]);

        if($question->likes->contains(Auth::user())) {
            $question->likes()->detach(Auth::user()->id);
            $isLiked = false;
        } else {
            $question->likes()->attach(Auth::user()->id);
            $isLiked = true;
        }
        $likesCount = ($questionRepo->getLikesCount($id)->likes_count);
        
        $dataResponse = [
            'success' => true,
            'isLiked' => $isLiked,
            'likesCount' => $likesCount
        ];
        return response()->json($dataResponse);
    }

    public function getSearch(Request $request, QuestionsInterface $questionRepo)
    {
        $data = $request->all();
        $questionsData = $questionRepo->search($data);
        return view('questions.questions', compact('questionsData'));
    }

    public function getMyQuestions(QuestionsInterface $questionRepo, UsersInterface $userRepo)
    {
        $userFriendRequests = $userRepo->getUserFriendRequests(Auth::id());
        $questionsData = $questionRepo->getQuestionsWithTagsByUserId(Auth::id());
        $data = [
            'friendRequests' => $userFriendRequests,
            'questionsData' =>$questionsData
        ];
        return view('users.myquestions', $data);
    }
}
