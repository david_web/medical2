<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('authenticate');
    }
	
    public function getDashboard()
	{
		return view('dashboard.dashboard');
	}
}
