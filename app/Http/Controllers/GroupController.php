<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Http\Requests\ImageUploadRequest;

use App\Contracts\UsersInterface;
use App\Contracts\GroupsInterface;
use App\Contracts\FoldersInterface;
use App\Contracts\FilesInterface;
use App\Contracts\AnnouncementsInterface;
use App\Contracts\PollsInterface;
use App\Contracts\GroupDiscussionsInterface;

use Auth;
use File;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function getGroups(UsersInterface $userRepo, GroupsInterface $groupRepo)
    {
        $users = $userRepo->getAll();
        $groups = $groupRepo->getParentGroups();
        $groupRequests = $groupRepo->getGroupRequests(Auth::id());
        $allusers = [];
        foreach ($users as $user) {
            if ($user->id != Auth::id()) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
        };
        $data = [
            'groups' => $groups,
            'allusers' => $allusers,
            'groupRequests' => $groupRequests
        ];
        return view('groups.groups', $data);
    }

    public function getMyGroups(UsersInterface $userRepo, GroupsInterface $groupRepo)
    {
    	$users = $userRepo->getAll();
        $groupRequests = $groupRepo->getGroupRequests(Auth::id());
        $groups = $groupRepo->getUserGroups(Auth::id());
        $allusers = [];
        foreach ($users as $user) {
            if ($user->id != Auth::id()) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
        };

        $data = [
        	'allusers' => $allusers,
        	'groups' => $groups,
            'groupRequests' => $groupRequests
        ];

    	return view('users.my-groups', $data);
    }

    public function postAddGroup(GroupRequest $request, GroupsInterface $groupRepo)
    {
    	$data = $request->all();
    	$data['user_id'] = Auth::id();
    	$users_id = json_decode($data['users_id']);
    	$group = $groupRepo->add($data);
        $group->users()->attach([Auth::id() => ['accepted' => 1, 'group_accepted' => 1, 'role' => 'admin']]);
    	if ($users_id) {
    		foreach ($users_id as $user_id) {
    			$group->users()->attach([$user_id => ['group_accepted' => 1]]);
       		}
    	};
    	return redirect()->back();
    }

    public function getGroup($id, UsersInterface $userRepo, GroupsInterface $groupRepo, FoldersInterface $folderRepo, FilesInterface $fileRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $countries = config('countries');
            $filesData = $fileRepo->getFilesByGroupId($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $foldersData = $folderRepo->getFoldersByGroupId($id);
            foreach ($foldersData as $folderData) {
                $folderId = $folderData['id'];
                $file = $fileRepo->getFilesByFolderId($folderId);
                $folderData['fileCount'] = count($file);
            };
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $action = null;
            if($group['parent_group_id']) {
                $action = 'sub';
            }

            $data = [
            	'allusers' => $allusers,
            	'group' => $group,
                'foldersData' => $foldersData,
                'filesData' => $filesData,
                'countries' => $countries,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'action' => $action,
                'groupUserRequests' => $groupUserRequests
            ];
        	return view('groups.group-library', $data);
        } else {
            return redirect()->back();
        }
    }

    public function getGroupRequests(UsersInterface $userRepo, GroupsInterface $groupRepo)
    {
        $groupRequests = $groupRepo->getGroupRequests(Auth::id());
        $users = $userRepo->getAll();
        $allusers = [];
        foreach ($users as $user) {
            if ($user->id != Auth::id()) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
        };

        $data = [
            'allusers' => $allusers,
            'groupRequests' => $groupRequests
        ];

    	return view('groups.group-requests', $data);
    }

    public function getAcceptRequest($id, $userId=null, GroupsInterface $groupRepo)
    {
        if(!$userId) {
    	    $group = $groupRepo->getGroupById($id);
            if ($group) {
                $group->users()->updateExistingPivot(Auth::id(), ['accepted' => 1]);
                return response()->json(['success' => true]);
            }
            return response()->json(['success' => false]);
        } else {
            $group = $groupRepo->getGroupWithAdminsAndManagers($id);
            $user = Auth::user();
            if ($user->can('update', $group)) {
                $group->users()->updateExistingPivot($userId, ['group_accepted' => 1]);
            }
        }
        return redirect()->back();
    }

    public function getRejectRequest($id, $userId=null, GroupsInterface $groupRepo)
    {
        if(!$userId) {
        	$group = $groupRepo->getGroupById($id);
        	if ($group) {
        		$group->users()->detach(Auth::id());
                return response()->json(['success' => true]);
        	}
            return response()->json(['success' => false]);
        } else {
            $group = $groupRepo->getGroupWithAdminsAndManagers($id);
            $user = Auth::user();
            if ($user->can('update', $group)) {
                $group->users()->detach($userId);
            }
            return redirect()->back();
        }
    }

    public function postAddUserToGroup($id, Request $request, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $data = $request->all();
            $users_id = json_decode($data['users_id']);
            if ($users_id) {
                foreach ($users_id as $user_id) {
                    $group->users()->attach([$user_id => ['group_accepted' => 1]]);
                }
            }
        }
        return redirect()->back();
    }

    public function postGroupImageUpload($id, ImageUploadRequest $request, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($id);
        $user = Auth::user();
        if ($user->can('update', $group)) {
            $group = $groupRepo->getGroupById($id);
            $data = $request->all();
            if ($request->file('image')){
                if($group->path) File::delete(public_path().'/assets/uploads/'.$group->path);
                $upload = $request->file('image');
                $extension = $upload->getClientOriginalExtension();
                $newName = str_random().'.'.$extension;
                $uploadPath = public_path() . '/assets/uploads/';
                $upload->move($uploadPath, $newName);
                $size = getimagesize($uploadPath.$newName);
                $filesize = filesize($uploadPath.$newName);
                $data['path'] = $newName;
            };
            $group = $groupRepo->addImage($data, $id);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function getGroupManagement($id, GroupsInterface $groupRepo, UsersInterface $userRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $action = null;
            if($group['parent_group_id']) {
                $action = 'sub';
            }
            $data = [
                'allusers' => $allusers,
                'group' => $group,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'action' => $action,
                'groupUserRequests' => $groupUserRequests
            ];
            return view('groups.group-management', $data);
        } else {
            return redirect()->back();
        }
    }

    public function postChangeRole($id, Request $request, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdmins($id);
        $user = Auth::user();
        if ($user->can('update', $group)) {
            $group = $groupRepo->getGroupWithUsersById($id);
            $data = $request->all();
            $userId = $data['id'];
            $role = $data['role'];
            if ($group) {
                $group->users()
                      ->wherePivot('user_id', $userId)
                      ->updateExistingPivot($userId, ['role' => $role], false);
            }
        } else {
            return redirect()->back();
        }
    }

    public function getDeleteUser($id, $userId, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdmins($id);
        $user = Auth::user();
        if ($user->can('update', $group)) {
            $group = $groupRepo->getGroupWithUsersById($id);
            $group->users()->detach($userId);
        }
        return redirect()->back();
    }

    public function getDeleteGroupImage($id, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($id);
        $user = Auth::user();
        if ($user->can('update', $group)) {
            if ($group->path) File::delete(public_path().'/assets/uploads/'.$group->path);
            $data = ['path' => null];
            $group = $groupRepo->addImage($data, $id);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function getGroupAnnouncements($id, GroupsInterface $groupRepo, UsersInterface $userRepo, AnnouncementsInterface $announcementRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $announcements = $announcementRepo->getAnnouncementsByGroupId($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $groupUsers = [];
            foreach ($group->users as $user) {
                if ($user->id != Auth::id()) $groupUsers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $action = null;
            if($group['parent_group_id']) {
                $action = 'sub';
            }

            $data = [
                'allusers' => $allusers,
                'group' => $group,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'groupUsers' => $groupUsers,
                'announcements' => $announcements,
                'action' => $action,
                'groupUserRequests' => $groupUserRequests
            ];
            return view('groups.group-announcements', $data);
        }
        return redirect()->back();
    }

    public function getGroupPolls($id, GroupsInterface $groupRepo, UsersInterface $userRepo, PollsInterface $pollRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $polls = $pollRepo->getPollsByGroupId($id);

            foreach($polls as $poll) {
                $sum = 0;
                foreach($poll->options as $option) {
                    $sum += count($option->users);
                }
                $poll['users_count'] = $sum;
            }
            $action = null;
            if($group['parent_group_id']) {
                $action = 'sub';
            }

            $data = [
                'allusers' => $allusers,
                'group' => $group,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'polls' => $polls,
                'action' => $action,
                'groupUserRequests' => $groupUserRequests
            ];
            return view('groups.group-polls', $data);
        } else {
            return redirect()->back();
        }
    }

    public function getSubGroups($id, GroupsInterface $groupRepo, UsersInterface $userRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group) && !$group['parent_group_id']) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $subGroups = $groupRepo->getSubGroupsByGroupId($id);

            $data = [
                'allusers' => $allusers,
                'group' => $group,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'action' => null,
                'groupUserRequests' => $groupUserRequests,
                'subGroups' => $subGroups
            ];
            return view('groups.subgroups', $data);
        }
        return redirect()->back();
    }

    public function postAddSubGroup($id, GroupsInterface $groupRepo, GroupRequest $request)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($id);
        $user = Auth::user();
        if ($user->can('update', $group) && !$group['parent_group_id']) {
            $data = $request->all();
            $data['parent_group_id'] = $id;
            $data['user_id'] = Auth::id();
            $subGroup = $groupRepo->add($data);
            foreach ($group->users as $user) {
                $role = $group->users()->where('user_id', $user->id)->first()->pivot->role;
                $subGroup->users()->attach([$user->id => ['accepted' => 1, 'group_accepted' => 1, 'role' => $role]]);
            }
        }
        return redirect()->back();
    }

    public function getAskToJoin($id, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupById($id);
        $user = Auth::user();
        if ($user->can('askToJoin', $group)) {
            $group->users()->attach([Auth::id() => ['accepted' => 1]]);
            if(!$group->parent_group_id) {
                return response()->json(['success' => true, 'id' => $id]);
            }
        }
        return redirect()->back();
    }

    public function getLeaveGroup($id, GroupsInterface $groupRepo)
    {
        $group = $groupRepo->getGroupById($id);
        $group->users()->detach(Auth::id());
        if(!$group->parent_group_id) {
            return response()->json(['success' => true, 'id' => $id, 'name' => $group->name]);
        }
        return redirect()->back();
    }

    public function getGroupDiscussion($id, GroupsInterface $groupRepo, UsersInterface $userRepo, GroupDiscussionsInterface $discussionsRepo)
    {
        $group = $groupRepo->getGroupWithUsersById($id);
        $user = Auth::user();
        if ($user->can('view', $group)) {
            $groupData = $groupRepo->getGroupWithAdminsAndManagers($id);
            $users = $userRepo->getUserFriendsById(Auth::id());
            $groupAdmins = $groupRepo->getGroupWithAdmins($id);
            $groupUserRequests = $groupRepo->getGroupWithNotAcceptedUsers($id);
            $discussions = $discussionsRepo->getGroupDiscussions($id);
            $allusers = [];
            foreach ($users as $user) {
                if ($user->id != Auth::id() && !$group->users->contains($user)) $allusers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $groupUsers = [];
            foreach ($group->users as $user) {
                if ($user->id != Auth::id()) $groupUsers[$user->id] = $user->first_name . ' ' . $user->last_name;
            };
            $action = null;
            if($group['parent_group_id']) {
                $action = 'sub';
            }

            $data = [
                'allusers' => $allusers,
                'group' => $group,
                'groupData' => $groupData,
                'groupAdmins' => $groupAdmins,
                'groupUsers' => $groupUsers,
                'discussions' => $discussions,
                'action' => $action,
                'groupUserRequests' => $groupUserRequests
            ];
            return view('groups.group-discussion', $data);
        }
        return redirect()->back();
    }
}