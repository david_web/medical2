<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contracts\GroupSubDiscussionsInterface;
use App\Contracts\GroupsInterface;

use Auth;

class GroupSubDiscussionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function storeSubDiscussion(Request $request, GroupsInterface $groupRepo, GroupSubDiscussionsInterface $subDiscussionRepo)
    {
        $data = $request->all();
        $group = $groupRepo->getGroupWithUsersById($data['group_id']);
        $user = Auth::user();
        if ($user->can('view', $group)) {
        	$data['user_id'] = Auth::user()->id;
        	$subDiscussionRepo->add($data);
        }
    	return redirect()->back();
    }

    public function deleteSubDiscussion($id, $groupId, GroupsInterface $groupRepo, GroupSubDiscussionsInterface $subDiscussionRepo)
    {
        $group = $groupRepo->getGroupWithAdminsAndManagers($groupId);
        $user = Auth::user();
        $subDiscussion = $subDiscussionRepo->getSubDiscussionById($id);
        if ($user->can('update', $group) || $user->can('delete', $subDiscussion)) {
    	   $subDiscussion->delete();
        }
    	return redirect()->back();
    }

    public function updateSubDiscussion(Request $request, GroupsInterface $groupRepo, GroupSubDiscussionsInterface $subDiscussionRepo)
    {
        $data = $request->all();
        $group = $groupRepo->getGroupWithAdminsAndManagers($data['group_id']);
        $user = Auth::user();
        $subDiscussion = $subDiscussionRepo->getSubDiscussionById($data['subdiscussion_id']);
        if ($user->can('update', $group) || $user->can('update', $subDiscussion)) {
            $subDiscussionRepo->update($data['subdiscussion_id'], ['message' => $data['message']]);
        }
        return redirect()->back();
    }
}
