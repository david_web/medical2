<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Carbon\Carbon;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required|date|before:' . Carbon::now()->subYears(12)->addDay(1)->toDateString(),
            'nationality' => 'required',
            'residence' => 'required',
            'profession_id' => 'required',
            'place_of_work_or_study' => 'required',
            'institution_name' => 'required',
            'mobile_number' => 'required|mobile|min:9',
            'whatsapp' => 'required|mobile|min:9',
            'facebook' => 'required',
            'vk' => 'required',
            'viber' => 'required|mobile|min:9',
            'email' => 'email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'before' => 'Age must be at least 12 years',
        ];
    }

    public function attributes()
    {
        return [
            'profession_id' => 'profession'
        ];
    }
}
