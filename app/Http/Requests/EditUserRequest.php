<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Carbon\Carbon;

use Auth;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required|date|before:' . Carbon::now()->subYears(12)->addDay(1)->toDateString(),
            'nationality' => 'required',
            'residence' => 'required',
            'profession_id' => 'required',
            'place_of_work_or_study' => 'required',
            'institution_name' => 'required',
            'mobile_number' => 'required|mobile|min:9',
            'whatsapp' => 'required|mobile|min:9',
            'facebook' => 'required',
            'vk' => 'required',
            'viber' => 'required|mobile|min:9',
            'email' => 'email|unique:users,email,'.Auth::id()
        ];
        if ($this->file('image')) $rules['image'] = 'required|mimes:jpg,jpeg,gif,png';
        return $rules;
    }
}
