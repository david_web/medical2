<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => 'required',
            'institution_name' => 'required',
            'course' => 'required',
            'language' => 'required',
            'description' => 'required',
            'file' => 'required|mimes:txt,pdf,jpg,jpeg,png,mp4,m4v,mov,mpg,mpeg,asf,avi,mp3'
        ];
    }
}
