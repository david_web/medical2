<?php

namespace App\Http\Validator;

class CustomValidationRule extends \Illuminate\Validation\Validator
{

	public function validateMobile($attribute, $value, $parameters)
	    {
	        // Mobile number can start with plus sign and should start with number
	        // and can have minus sign and should be between 9 to 12 character long.
	        // return preg_match("/^\+?\d[0-9-]{9,12}/", $value);
	        return preg_match("/^([0-9\s\-\+\(\)]*)$/", $value);
	    }
}