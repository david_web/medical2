<?php

namespace App\Contracts;
    
interface GroupSubDiscussionsInterface

{
	/**
      * add new sub discussion in db
      *
      * @param array $param
      *
      * @return subDiscussion
    */
    public function add($param);

    /**
      * update sub discussion
      *
      * @param array $data
      * @param int $id
      *
      * @return bool
    */
    public function update($id, $data);

    /**
      * get sub discussion from db
      *
      * @param array $data
      * @param int $id
      *
      * @return sub discussion
    */
    public function getSubDiscussionById($id);
}