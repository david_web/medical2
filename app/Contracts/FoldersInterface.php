<?php

namespace App\Contracts;
    
interface FoldersInterface

{
	/**
      * add new folder in db
      *
      * @param array $param
      *
      * @return folder
    */
    public function add($param);

    /**
      * get folders from db by user id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersById($id);

    /**
      * get folder from db by parent folder id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersByParentId($id);

    /**
      * delete folder from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function deleteFolderById($id);

    /**
      * get folders from db by group id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersByGroupId($id);

    /**
      * get folder from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFolderById($id);
}	