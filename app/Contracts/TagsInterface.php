<?php

namespace App\Contracts;
    
interface TagsInterface

{
	/**
      * get tags
      *
      * 
      *
      * @return tag
    */
    public function getTags();
}