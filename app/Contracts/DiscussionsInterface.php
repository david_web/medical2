<?php

namespace App\Contracts;
    
interface DiscussionsInterface

{
	/**
      * add new  Discussion in db
      *
      * @param array $param
      *
      * @return param
    */
    public function add($param);

    public function getDiscussionById($id);

    public function discussionAccept($id,$param);
    public function deleteDiscussionById($id);
}    