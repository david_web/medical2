<?php

namespace App\Contracts;
    
interface QuestionsInterface

{
	/**
      * add new question in db
      *
      * @param array $param
      *
      * @return question
    */
    public function add($param);

    /**
      * get all questions from db
      *
      * @param array $param
      *
      * @return question
    */
    public function getAllWithTags();

    /**
      * get question with details from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithDetailsById($id);

    /**
      * get question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionById($id);

    /**
      * delete question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function delete($id);

    /**
      * edit question in db
      *
      * @param array $param
      *
      * @return question
    */
    public function edit($param, $id);

    /**
      * get question with tags from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithTagsById($id);

    /**
      * get question with likes from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithLikes($id);

    /**
      * get question likes count from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getLikesCount($id);

    /**
      * get questions from db by search
      *
      * @param array $param
      *
      * @return question
    */
    public function search($param);

    /**
      * get question with tags from db by user id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionsWithTagsByUserId($id);
}