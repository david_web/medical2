<?php

namespace App\Contracts;
    
interface AnnouncementsInterface

{
	/**
      * add new announcement in db
      *
      * @param array $param
      *
      * @return announcement
    */
    public function add($param);

    /**
      * get announcements from db by group id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function getAnnouncementsByGroupId($id);

    /**
      * get announcement from db by id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function getAnnouncementById($id);

    /**
      * delete announcement from db by id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function delete($id);

    /**
      * edit announcement from in by id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function edit($id, $param);
}