<?php

namespace App\Contracts;
    
interface UsersInterface

{
	/**
      * add new user in db
      *
      * @param array $param
      *
      * @return user
    */
    public function add($param);

    /**
      * get user from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserById($id);

    /**
      * edit user in db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function edit($param, $id);

    /**
      * get user from db by verify token
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserByVerifyToken($param);

    /**
      * get all users from db without auth user
      *
      * @param array $param
      *
      * @return user
    */
    public function getAll();

    /**
      * get user friend requests from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserFriendRequests($id);

    /**
      * get user friends from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserFriendsById($id);
}