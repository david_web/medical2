<?php

namespace App\Contracts;
    
interface PollsInterface
{
	/**
      * add new poll in db
      *
      * @param array $param
      *
      * @return poll
    */
    public function add($param);

    /**
      * get polls from db by group id
      *
      * @param array $param
      *
      * @return polls
    */
    public function getPollsByGroupId($id);

    /**
      * get polls from db by group id
      *
      * @param array $param
      *
      * @return polls
    */
    public function getPollById($id);
}