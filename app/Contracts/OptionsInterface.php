<?php

namespace App\Contracts;
    
interface OptionsInterface

{
	/**
      * add options in db
      *
      * @param array $param
      *
      * @return option
    */
    public function add($param);

    /**
      * get option from db by id
      *
      * @param array $param
      *
      * @return option
    */
    public function getOptionById($id);
}