<?php

namespace App\Contracts;
    
interface ProfessionsInterface

{
	/**
      * get professions
      *
      *
      *
      * @return profession
    */
    public function getProfessions();
}