<?php

namespace App\Contracts;
    
interface NotesInterface

{
	/**
      * add new note in db
      *
      * @param array $param
      *
      * @return note
    */
    public function add($param);

    /**
      * get note from db by id
      *
      * @param array $param
      *
      * @return note
    */
    public function getNoteById($id);

    /**
      * edit note in db
      *
      * @param array $param
      *
      * @return note
    */
    public function edit($id, $param);
}