<?php

namespace App\Contracts;
    
interface GroupsInterface

{
	/**
      * add new group in db
      *
      * @param array $param
      *
      * @return group
    */
    public function add($param);

    /**
      * get all groups from db
      *
      *
      *
      * @return group
    */
    public function getParentGroups();

    /**
      * get group with users from db
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupWithUsersById($id);

    /**
      * get user groups from db
      *
      * @param id $id
      *
      * @return group
    */
    public function getUserGroups($id);

    /**
      * get group from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupById($id);

    /**
      * add group image to db by id
      *
      * @param array $param,  id $id
      *
      * @return group
    */
    public function addImage($param, $id);

    /**
      * get group requests from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupRequests($id);

    /**
      * get group user requests from db by group id
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupWithNotAcceptedUsers($id);

    /**
      * get group with admins and managers from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupWithAdminsAndManagers($id);

    /**
      * get group with admins from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getGroupWithAdmins($id);

    /**
      * get group sub groups with users from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getSubGroupsByGroupId($id);
}