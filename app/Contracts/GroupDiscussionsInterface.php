<?php

namespace App\Contracts;
    
interface GroupDiscussionsInterface

{
    /**
      * get group discussion
      *
      * @param int $groupId
      *
      * @return discussions
    */
    public function getGroupDiscussions($groupId);

	/**
      * add new discussion in db
      *
      * @param array $param
      *
      * @return discussion
    */
    public function add($param);

    /**
      * update discussion
      *
      * @param array $id
      *
      * @return bool
    */
    public function update($id, $data);

    /**
      * get discussion from db
      *
      * @param array $id
      *
      * @return discussion
    */
    public function getDiscussionById($id);
}