<?php

namespace App\Contracts;
    
interface FrendsDiscussionsInterface

{   /**
      * add options in db
      *
      * @param array $param
      *
      * @return option
    */
    public function add($param);
    /**
      * get option from db by id
      *
      * @param array $param
      *
      * @return option
    */
    public function getUserDiscussionRequest($id);
    /**
      * get option from db by id
      *
      * @param array $param
      *
      * @return option
    */
    public function	getDiscussionById($id,$param);
    public function getUserChat($id); 
    public function createRow($param); 
}               