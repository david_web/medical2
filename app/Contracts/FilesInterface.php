<?php

namespace App\Contracts;
    
interface FilesInterface

{
	/**
      * add new file in db
      *
      * @param array $param
      *
      * @return file
    */
    public function add($param);

    /**
      * update file in db
      *
      * @param array $param
      *
      * @return file
    */
    public function edit($param, $id);

    /**
      * get all files from db with comments
      *
      * @param array $param
      *
      * @return file
    */
    public function getPublicFilesWithComments();

    /**
      * get files count from db by user id
      *
      * @param array $param
      *
      * @return count
    */
    public function getFilesCount($id);

    /**
      * get files from db by user id
      *
      * @param array $param
      *
      * @return file
    */
    public function getFilesByUserId($id);

    /**
      * get files by folder id from db
      *
      * @param array $param
      *
      * @return file
    */
    public function getFilesByFolderId($id);

    /**
      * get files from db by group id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFilesByGroupId($id);

    /**
      * get file from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFileById($id);

    /**
      * delete file from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function deleteFileById($id);
}