<?php

namespace App\Contracts;
    
interface CommentsInterface

{
	/**
      * add new comment in db
      *
      * @param array $param
      *
      * @return comment
    */
    public function add($param);

    /**
      * get comment from db by id
      *
      * @param array $param
      *
      * @return comment
    */
    public function getCommentById($id);

    /**
      * delete comment from db by id
      *
      * @param array $param
      *
      * @return comment
    */
    public function delete($id);
} 