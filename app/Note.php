<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    	'group_id',
        'folder_id',
        'name',
        'path',
    ];

    public function folder()
    {
        return $this->belongsTo('App\Folder');
    }
}
