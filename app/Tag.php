<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Question', 'question_tags', 'question_id');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'question_tags', 'tag_id');
    }
}
