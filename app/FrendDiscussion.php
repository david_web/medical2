<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrendDiscussion extends Model
{
        protected $fillable = [  
          'user_id',
          'follower_id',
          'discussion_topic',
          'private',
          'expected'
       ];

       protected $table = 'frends_discussions' ;  
       public function user()
	{
		return $this->belongsTo('App\User');
	}
}
