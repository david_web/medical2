<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    	'group_id',
        'parent_user_id',
        'name',
    ];

    public function parent_folders()
    {
        return $this->belongsToMany('App\Folder', 'folder_sub_folders', 'parent_id', 'child_id');
    }

    public function child_folders()
    {
        return $this->belongsToMany('App\Folder', 'folder_sub_folders', 'child_id', 'parent_id');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }
}
