<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measege extends Model
{
    protected $fillable = [
        'user_id',
        'discussion_id',
        'measege',
       
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
