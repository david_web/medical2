<?php

namespace App\Services;

use App\Contracts\TagsInterface;

use App\Tag;

class TagsService implements TagsInterface
{
	/**
     * Create a new instance of ProfessionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->tag = new Tag();
    }

    /**
      * get all tags
      *
      * @return tag
    */
    public function getTags()
    {
        return $this->tag->get();
    }
}