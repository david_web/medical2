<?php

namespace App\Services;

use App\Contracts\QuestionsInterface;

use App\Question;

class QuestionsService implements QuestionsInterface
{
	/**
     * Create a new instance of UsersService.
     *
     * @return void
     */
    function __construct()
    {
        $this->question = new Question();
    }

    /**
      * add new question in db
      *
      * @param array $param
      *
      * @return question
    */
    public function add($array)
    {
        return $this->question->create($array);
    }

    /**
      * get all questions from db
      *
      * @param array $param
      *
      * @return question
    */
    public function getAllWithTags()
    {
      return $this->question->with(['tags'])->orderBy('id', 'desc')->paginate(8);
    }

    /**
      * get question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithDetailsById($id)
    {
      return $this->question->with(['comments', 'tags', 'user'])->find($id);
    }

    /**
      * get question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionById($id)
    {
      return $this->question->with(['tags'])->find($id);
    }

    /**
      * delete question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function delete($id)
    {
      return $this->question->with(['tags', 'comments'])->find($id)->delete();
    }

    /**
      * edit question in db
      *
      * @param array $param
      *
      * @return question
    */
    public function edit($array, $id)
    {
      return $this->question->with('tags')->find($id)->update($array);
    }

    /**
      * get question from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithTagsById($id)
    {
      return $this->question->with('tags')->find($id);
    }

    /**
      * get question with likes from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionWithLikes($id)
    {
      return $this->question->with('likes')->find($id);
    }

    /**
      * get question likes count from db by id
      *
      * @param array $param
      *
      * @return question
    */
    public function getLikesCount($id)
    {
      return $this->question->withCount('likes')->find($id);
    }

    /**
      * get questions from db by search
      *
      * @param array $param
      *
      * @return question
    */
    public function search($param)
    {
      return $this->question->whereHas('tags', function($q) use ($param) {
        $q->where('tag', 'LIKE', '%'.$param['search'].'%');
      })->orderBy('id', 'desc')->paginate(8);
    }

    /**
      * get question from db by user id
      *
      * @param array $param
      *
      * @return question
    */
    public function getQuestionsWithTagsByUserId($id)
    {
      return $this->question->where('user_id', $id)->with('tags')->orderBy('id', 'desc')->paginate(8);
    }
}