<?php

namespace App\Services;

use App\Contracts\AnnouncementsInterface;

use App\Announcement;

class AnnouncementsService implements AnnouncementsInterface
{
	/**
     * Create a new instance of ComentsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->announcement = new Announcement();
    }

    /**
      * add new announcement in db
      *
      * @param array $param
      *
      * @return announcement
    */
    public function add($array)
    {
    	return $this->announcement->create($array);
    }

    /**
      * get announcements from db by group id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function getAnnouncementsByGroupId($id)
    {
      return $this->announcement->where('group_id', $id)->get();
    }

    /**
      * get announcements from db by group id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function getAnnouncementById($id)
    {
      return $this->announcement->find($id);
    }

    /**
      * delete announcement from db by id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function delete($id)
    {
      return $this->announcement->find($id)->delete();
    }

    /**
      * edit announcement from in by id
      *
      * @param array $param
      *
      * @return announcement
    */
    public function edit($id, $array)
    {
      return $this->announcement->find($id)->update($array);
    }
}