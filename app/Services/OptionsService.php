<?php

namespace App\Services;

use App\Contracts\OptionsInterface;

use App\Option;

class OptionsService implements OptionsInterface
{
	/**
     * Create a new instance of OptionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->option = new Option();
    }

    /**
      * add options in db
      *
      * @param array $param
      *
      * @return option
    */
    public function add($array)
    {
    	return $this->option->create($array);
    }

    /**
      * get option from db by id
      *
      * @param array $param
      *
      * @return option
    */
    public function getOptionById($id)
    {
      return $this->option->with('poll')->find($id);
    }
}