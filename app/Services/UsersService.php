<?php

namespace App\Services;

use App\Contracts\UsersInterface;

use App\User;

class UsersService implements UsersInterface
{
	/**
     * Create a new instance of UsersService.
     *
     * @return void
     */
    function __construct()
    {
        $this->user = new User();
    }

    /**
      * add new user in db
      *
      * @param array $param
      *
      * @return user
    */
    public function add($array)
    {
        return $this->user->create($array);
    }

    /**
      * get user from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserById($id)
    {
      return $this->user->find($id);
    }

    /**
      * edit user in db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function edit($array, $id)
    {
      return $this->user->find($id)->update($array);
    }

    /**
      * get user from db by verify token
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserByVerifyToken($array)
    {
      return $this->user->where('verify_token', $array)->first();
    }

    /**
      * get all users from db without auth user
      *
      * @param array $param
      *
      * @return user
    */
    public function getAll()
    {
      return $this->user->get();
    }

    /**
      * get user friend requests from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserFriendRequests($id)
    {
      return $this->user->whereHas('followers', function($query) use ($id) {
                                    $query->where([['follower_id', $id], ['accepted', 0]]);
                                })->get();
    }

    /**
      * get user friends from db by id
      *
      * @param array $param
      *
      * @return user
    */
    public function getUserFriendsById($id)
    {
      return $this->user->whereHas('followers', function($query) use ($id) {
                                    $query->where([['follower_id', $id], ['accepted', 1]]);
                                })->get();
    }
}