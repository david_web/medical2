<?php

namespace App\Services;

use App\Contracts\MeasegesInterface;

use App\Measege;

class MeasegesService implements MeasegesInterface
{
	  /**
     * Create a new instance of NotesService.
     *
     * @return void
     */
    function __construct()
    {
        $this->measege = new Measege();
    }

	  // *
   //    * add new note in db
   //    *
   //    * @param array $param
   //    *
   //    * @return note
    
    public function add($array)
    {
    	return $this->measege->create($array);
    }

   public function deleteMeasegeById($id)
   {
     return $this->measege->where('discussion_id',$id)->delete();
   }
    
}