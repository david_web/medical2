<?php

namespace App\Services;

use App\Contracts\NotesInterface;

use App\Note;

class NotesService implements NotesInterface
{
	  /**
     * Create a new instance of NotesService.
     *
     * @return void
     */
    function __construct()
    {
        $this->note = new Note();
    }

	  /**
      * add new note in db
      *
      * @param array $param
      *
      * @return note
    */
    public function add($array)
    {
    	return $this->note->create($array);
    }

    /**
      * get note from db by id
      *
      * @param array $param
      *
      * @return note
    */
    public function getNoteById($id)
    {
      return $this->note->find($id);
    }

    /**
      * edit note in db
      *
      * @param array $param
      *
      * @return note
    */
    public function edit($id, $array)
    {
      return $this->note->find($id)->update($array);
    }
}