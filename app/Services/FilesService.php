<?php

namespace App\Services;

use App\Contracts\FilesInterface;

use App\File;

class FilesService implements FilesInterface
{
	/**
     * Create a new instance of ComentsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->file = new File();
    }

    /**
      * add new file in db
      *
      * @param array $param
      *
      * @return file
    */
    public function add($array)
    {
        return $this->file->create($array);
    }

    /**
      * update file in db
      *
      * @param array $param
      *
      * @return file
    */
    public function edit($array, $id)
    {
      return $this->file->find($id)->update($array);
    }

    /**
      * get all files from db with comments
      *
      * @param array $param
      *
      * @return file
    */
    public function getPublicFilesWithComments()
    {
      return $this->file->with('comments', 'user')->where('is_public', 1)->orderBy('id', 'desc')->paginate(8);
    }

    /**
      * get files count from db by user id
      *
      * @param array $param
      *
      * @return count
    */
    public function getFilesCount($id)
    {
      return $this->file->where('user_id', $id)->with('user')->count();
    }

    /**
      * get files from db by user id
      *
      * @param array $param
      *
      * @return file
    */
    public function getFilesByUserId($id)    
    {
      return $this->file->where([['group_id', null], ['user_id', $id]])->orderBy('id', 'desc')->get();
    }

    /**
      * get files by folder id from db
      *
      * @param array $param
      *
      * @return file
    */
    public function getFilesByFolderId($id)
    {
      return $this->file->where('folder_id', $id)->orderBy('id', 'desc')->get();
    }

    /**
      * get files from db by group id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFilesByGroupId($id)
    {
      return $this->file->where('group_id', $id)->orderBy('id', 'desc')->get();
    }

    /**
      * get file from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFileById($id)
    {
      return $this->file->find($id);
    }

    /**
      * delete file from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function deleteFileById($id)
    {
      return $this->file->find($id)->delete();
    }
}