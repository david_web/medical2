<?php

namespace App\Services;

use App\Contracts\GroupSubDiscussionsInterface;

use App\GroupSubDiscussion;

class GroupSubDiscussionsService implements GroupSubDiscussionsInterface
{
	/**
     * Create a new instance of SubDiscussionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->subDiscussion = new GroupSubDiscussion();
    }

    /**
      * add new sub discussion in db
      *
      * @param array $param
      *
      * @return subDiscussion
    */
    public function add($array)
    {
    	return $this->subDiscussion->create($array);
    }

    /**
      * update sub discussion
      *
      * @param array $data
      * @param int $id
      *
      * @return bool
    */
    public function update($id, $data)
    {
        return $this->subDiscussion->whereId($id)->update($data);
    }

    /**
      * get sub discussion from db
      *
      * @param array $data
      * @param int $id
      *
      * @return sub discussion
    */
    public function getSubDiscussionById($id)
    {
        return $this->subDiscussion->find($id);
    }
}