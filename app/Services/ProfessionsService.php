<?php

namespace App\Services;

use App\Contracts\ProfessionsInterface;

use App\Profession;

class ProfessionsService implements ProfessionsInterface
{
	/**
     * Create a new instance of ProfessionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->profession = new Profession();
    }

    /**
      * get all professions
      *
      * @return profession
    */
    public function getProfessions()
    {
        return $this->profession->get();
    }
}