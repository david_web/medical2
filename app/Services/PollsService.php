<?php

namespace App\Services;

use App\Contracts\PollsInterface;

use App\Poll;

class PollsService implements PollsInterface
{
	/**
     * Create a new instance of PollsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->poll = new Poll();
    }

    /**
      * add ne poll in db
	  *
      * @param array $param
      *
      * @return profession
    */
    public function add($array)
    {
		  return $this->poll->create($array);
    }

    /**
      * get polls from db by group id
      *
      * @param array $param
      *
      * @return polls
    */
    public function getPollsByGroupId($id)
    {
      return $this->poll->where('group_id', $id)->with('options')->orderBy('id', 'desc')->get();
    }

    /**
      * get polls from db by group id
      *
      * @param array $param
      *
      * @return polls
    */
    public function getPollById($id)
    {
      return $this->poll->with('options')->find($id);
    }
}