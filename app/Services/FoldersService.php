<?php

namespace App\Services;

use App\Contracts\FoldersInterface;

use App\Folder;

class FoldersService implements FoldersInterface
{
	/**
     * Create a new instance of ComentsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->folder = new Folder();
    }

    /**
      * add new folder in db
      *
      * @param array $param
      *
      * @return folder
    */
    public function add($array)
    {
        return $this->folder->create($array);
    }

    /**
      * get folder from db by user id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersById($id)
    {
        return $this->folder->where([['user_id', $id], ['group_id', null]])->get();
    }

    /**
      * get folder from db by parent folder id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersByParentId($id)
    {
        return $this->folder->where('id', $id)->with('parent_folders')->get();
    }

    /**
      * delete folder from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function deleteFolderById($id)
    {
      return $this->folder->find($id)->delete();
    }

    /**
      * get folder from db by id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFolderById($id)
    {
      return $this->folder->find($id);
    }

    /**
      * get folders from db by group id
      *
      * @param array $param
      *
      * @return folder
    */
    public function getFoldersByGroupId($id)
    {
      return $this->folder->where('group_id', $id)->get();
    }
}