<?php

namespace App\Services;

use App\Contracts\CommentsInterface;

use App\Comment;

class CommentsService implements CommentsInterface
{
	/**
     * Create a new instance of ComentsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->comment = new Comment();
    }

    /**
      * add new comment in db
      *
      * @param array $param
      *
      * @return comment
    */
    public function add($array)
    {
        return $this->comment->create($array);
    }

    /**
      * get comment from db by id
      *
      * @param array $param
      *
      * @return comment
    */
    public function getCommentById($id)
    {
      return $this->comment->find($id);
    }

    /**
      * delete comment from db by id
      *
      * @param array $param
      *
      * @return comment
    */
    public function delete($id)
    {
      return $this->comment->find($id)->delete();
    }
}    