<?php

namespace App\Services;

use App\Contracts\FrendsDiscussionsInterface;

use App\FrendDiscussion;

class FrendsDiscussionsService implements FrendsDiscussionsInterface
{
	function __construct()
    {
        $this->frendDiscussion = new FrendDiscussion();
    }

	 public function add($array)
    {
    	return $this->frendDiscussion->create($array);
    }
    public function getUserDiscussionRequest($id)
    {
    	return $this->frendDiscussion->where([['follower_id',$id],['expected', 0]])->get();
    	
    }
    public function getDiscussionById($id,$data)
    {
        return $this->frendDiscussion->where([['id',$id]])->update($data);
    }
    public function getUserChat($id)
    {
        return $this->frendDiscussion->where([['follower_id',$id],['expected', 1]])->get();
    }
    public function createRow($array)
    {
        return $this->frendDiscussion->create($array);
    }
}