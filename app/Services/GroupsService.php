<?php

namespace App\Services;

use App\Contracts\GroupsInterface;

use App\Group;

class GroupsService implements GroupsInterface
{
	/**
     * Create a new instance of ComentsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->group = new Group();
    }

    /**
      * add new group in db
      *
      * @param array $param
      *
      * @return group
    */
    public function add($array)
    {
    	return $this->group->create($array);
    }

    /**
      * get all groups from db
      *
      * @param array $param
      *
      * @return group
    */
    public function getParentGroups()
    {
      return $this->group->with(['users' => function($query) {
                                  $query->wherePivot('accepted', 1)->wherePivot('group_accepted', 1);
                              }])->withCount(['users' => function($query) {
                                        $query->where([['accepted', 1], ['group_accepted', 1]]);
                                    }])->where('parent_group_id', null)->get();
    }
    
    /**
      * get user groups from db
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupWithUsersById($id)
    {
      return $this->group->with(['users' => function($query) {
                                  $query->wherePivot('accepted', 1)->wherePivot('group_accepted', 1);
                              }])->find($id);
    }

    /**
      * get user groups from db
      *
      * @param array $param
      *
      * @return group
    */
    public function getUserGroups($id)
    {
        return $this->group->whereHas('users', function($query) use ($id) {
                                    $query->where([['user_id', $id], ['accepted', 1], ['group_accepted', 1]]);
                                })->withCount(['users' => function($query) {
                                        $query->where([['accepted', 1], ['group_accepted', 1]]);
                                    }])->where('parent_group_id', null)->get();
    }

    /**
      * get group from db by id
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupById($id)
    {
      return $this->group->find($id);
    }

    /**
      * add group image to db by id
      *
      * @param array $param
      *
      * @return group
    */
    public function addImage($array, $id)
    {
      return $this->group->find($id)->update($array);
    }

    /**
      * get group requests from db by id
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupRequests($id)
    {
      return $this->group->whereHas('users', function($query) use ($id) {
                                    $query->where([['user_id', $id], ['accepted', 0]]);
                                })->get();
    }

    /**
      * get group user requests from db by group id
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupWithNotAcceptedUsers($id)
    {
      return $this->group->with(['users' => function($query) {
                                    $query->wherePivot('group_accepted', 0);
                                }])->find($id);
    }

    /**
      * get group with admins and managers from db by id
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupWithAdminsAndManagers($id)
    {
      return $this->group->with(['users' => function($query) {
                                  $query->wherePivotIn('role', ['admin', 'manager']);
                              }])->find($id);
    }

    /**
      * get group with admins and managers from db by id
      *
      * @param array $param
      *
      * @return group
    */
    public function getGroupWithAdmins($id)
    {
      return $this->group->with(['users' => function($query) {
                                  $query->wherePivot('role', 'admin');
                              }])->find($id);
    }
    
    /**
      * get group sub groups with users from db by id
      *
      * @param id $id
      *
      * @return group
    */
    public function getSubGroupsByGroupId($id)
    {
      return $this->group->with(['users' => function($query) {
                                  $query->wherePivot('accepted', 1)->wherePivot('group_accepted', 1);
                              }])->where('parent_group_id', $id)->get();
    }
}