<?php

namespace App\Services;

use App\Contracts\GroupDiscussionsInterface;

use App\GroupDiscussion;
use App\GroupSubDiscussion;

class GroupDiscussionsService implements GroupDiscussionsInterface
{
	/**
     * Create a new instance of DiscussionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->discussion = new GroupDiscussion();
    }

    /**
      * get group discussion
      *
      * @param int $groupId
      *
      * @return discussions
    */
    public function getGroupDiscussions($groupId)
    {
        return $this->discussion->whereGroupId($groupId)->with(['user', 'subDiscussions.user'])->get();
    }

    /**
      * add new discussion in db
      *
      * @param array $param
      *
      * @return discussion
    */
    public function add($array)
    {
      return $this->discussion->create($array);
    }

    /**
      * update discussion
      *
      * @param array $id
      *
      * @return bool
    */
    public function update($id, $data)
    {
        return $this->discussion->whereId($id)->update($data);
    }

    /**
      * get discussion from db
      *
      * @param array $id
      *
      * @return discussion
    */
    public function getDiscussionById($id)
    {
      return $this->discussion->find($id);
    }
}