<?php

namespace App\Services;

use App\Contracts\DiscussionsInterface;

use App\Discussion;


class DiscussionsService implements DiscussionsInterface
{
	/**
     * Create a new instance of DiscussionsService.
     *
     * @return void
     */
    function __construct()
    {
        $this->discussion = new Discussion();
    }
    public function add($array)
    {
    	return $this->discussion->create($array);
    }
    public function getDiscussionById($id)
    {
        return $this->discussion->find($id);
    }
    public function discussionAccept($id,$array)
    {
        return $this->discussion->where('id',$id)->update($array);
    }
    public function deleteDiscussionById($id)
    {
        return $this->discussion->where('id',$id)->delete();
    }
}