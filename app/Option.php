<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'poll_id',
        'name'
    ];

    public function poll()
    {
        return $this->belongsTo('App\Poll');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_options', 'option_id', 'user_id');
    }
}