<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'folder_id',
        'group_id',
        'country',
        'institution_name',
        'course',
        'language',
        'description',
        'path',
        'type',
        'is_public'
    ];

    public function comments()
    {
        return $this->belongsToMany('App\Comment', 'file_comments', 'file_id', 'comment_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}
