<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'keyword',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'question_tags', 'question_id', 'tag_id');
    }

    public function comments()
    {
        return $this->belongsToMany('App\Comment', 'question_comments', 'question_id', 'comment_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likes()
    {
        return $this->belongsToMany('App\User', 'question_likes', 'question_id', 'user_id');
    }
}
