<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'parent_group_id',
        'name',
        'path'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_groups', 'group_id', 'user_id')->withPivot(['accepted', 'group_accepted', 'role']);
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function subGroups()
    {
        return $this->hasMany('App\Group', 'parent_group_id');
    }
}
