<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id',
        'user_id',
        'name',
        'is_public'
    ];

    public function options()
    {
        return $this->hasMany('App\Option');
    }
}