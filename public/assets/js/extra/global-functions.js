function Confirm(URL, TXT, formName) {
    swal({
        title: TXT,
        showCancelButton: true,
        confirmButtonColor: "#2196F3",
        closeOnConfirm: true,
        animation: "slide-from-top"
    }, function (result) {
        if (result) {
            if (formName != '') {
                console.log('dsasda');
                $('#' + formName).submit();
            } else
                self.location = URL;
        }
    });

}

function showNotification(title, message, type) {
    // Create new Notification
    new PNotify({
        title: title,
        text: message,
        shadow: false,
        opacity: 0.90,
        type: type,
        stack: {
            "dir1": "down",
            "dir2": "left",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        width: "320",
        delay: 5000
    });
}

function getCookies() {
    var pairs = document.cookie.split(";");
    var cookies = {};
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("=");
        cookies[pair[0]] = unescape(pair[1]);
    }
    return cookies;
}

export {
    Confirm,
    showNotification,
    getCookies,
}