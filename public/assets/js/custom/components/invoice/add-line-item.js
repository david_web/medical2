// import store from '../../resources/assets/js/custom/components/invoice/store/createInvoice';
import Vuelidate from 'vuelidate';
import { required, minLength, between } from 'vuelidate/lib/validators';
Vue.use(Vuelidate);

Vue.component('add-line-item', {
    data() {
        return {
            taxType: 'percent',
            subAccounts: [],
            products: [],
            account: '',
            account_id: '',
            subAccount: {},
            subAccount_id: '',
            name: '',
            description: '',
            quantity: '',
            price: '',
            discount: '',
            tax: '',
            id: 0,
            editedTax: {
                name: '',
                rate: 0
            },
            itemDetails: {
                account: '',
                subAccount: '',
                name: '',
                description: '',
                quantity: '',
                price: '',
                discount: '',
                tax: '',
            }
        };
    },

    validations: {
        account: { required },
        subAccount: { required },
        name: { required },
        quantity: { required },
        price: { required }
    },

    computed: {
        product: function () {
            return store.state.product;
        },
        taxes(){
            return store.state.taxes;
        },
        pickerOptions(){
            return store.pickerOptions;
        }
    },
    methods: {
        addItem(date) {
            var validation = true;
            if (this.account_id == '') {
                this.$v.account.$touch();
                validation = false
            }
            if (this.subAccount_id == '') {
                this.$v.subAccount.$touch();
                validation = false
            }
            if (this.name == '') {
                this.$v.name.$touch();
                validation = false
            }
            if (this.quantity == '') {
                this.$v.quantity.$touch();
                validation = false
            }
            if (this.price == '') {
                this.$v.price.$touch();
                validation = false
            }

            if (validation) {
                var subTotal = parseFloat(this.price) * parseFloat(this.quantity);
                if(this.discount == ""){
                    var total = subTotal;
                } else{
                    var total = subTotal - (( parseFloat(this.discount) / 100 ) * subTotal);
                }
                if (this.tax != "") {
                    total = total + (( parseFloat(this.tax) / 100 ) * total);
                }
                this.id++;

                this.itemDetails = {
                        id: this.id,
                        account: this.account,
                        account_id: this.account_id,
                        subAccount: this.subAccount,
                        subAccount_id: this.subAccount_id,
                        name: this.name,
                        description: this.description,
                        quantity: this.quantity,
                        price: this.price,
                        discount: this.discount,
                        tax: this.tax,
                        subTotal: subTotal.toFixed(2),
                        total: total.toFixed(2)
                    }
                store.state.items.push(this.itemDetails);
            }
        },

        chooseTaxType: function (type){
            store.state.taxType = type;
            this.taxType = type;
        },

        createTax: function () {
            var self = this;
            axios.post('/expensetaxes', {
                _token: token,
                editedTax: self.editedTax
            }).then(function (response) {
                var data = response.data;
                showNotification('Success', 'Tax ' + data.name + ' created.', 'success');
                self.taxes.push(data);
            });
            $('#add-tax-modal').modal('hide');

        },

    },
    mounted(){
        var that = this;
        Vue.nextTick(function () {
            $(that.$refs.selectAccount).select2().on('select2:select', function () {
                that.account_id = $(this).val();
                that.account = $(this).find('option:selected').text();
                if ($(this).val() == 'retail'){
                    axios.get('/get-products/')
                        .then(function(response) {
                            that.products = response.data.products
                    }); 
                }else{
                    axios.get('/get-subaccounts/' + that.account_id)
                        .then(function(response) {
                            that.subAccounts = response.data.subAccounts;
                    });  
                }
                $(that.$refs.selectSubAccount).select2();
            });
            $(that.$refs.selectSubAccount).select2().on('select2:select', function () {
                if ($(this).val() != 'AZ') {
                    if (that.account_id == 'retail') {
                        that.subAccount = that.products.find(function (product) {
                            return product.id == $(this).val();
                        }.bind(this));
                    } else {
                        that.subAccount = that.subAccounts.find(function (subAccount) {
                            return subAccount.id == $(this).val();
                        }.bind(this));
                    }
                    that.name = that.subAccount.name;
                    that.description = that.subAccount.description;
                    that.subAccount_id = $(this).val();
                }
            });

            $(that.$refs.selectTaxRate).select2().on('select2:select', function () {

                if( $(this).val() != 'add' ){
                    that.tax = $(this).val();
                } else {
                    $('#add-tax-modal').modal('show');
                    $(this).val('');
                }
            });

        }.bind(this));
    }
});