import SelectFamily from '../../resources/assets/js/custom/components/invoice/select-family.js';
import AddLineItem from '../../resources/assets/js/custom/components/invoice/add-line-item.js';
import store from '../../resources/assets/js/custom/components/invoice/store/createInvoice';

window.store = store;

Vue.component('create-invoice', {

    components: {
        SelectFamily,
        AddLineItem
    },

    data() {
        return {
            products: [],
            payment_type_id: '1',
            delButton: false,
        }
    },

    computed: {
        invoice(){
            var subTotal = 0;
            var total = 0;
            if(store.state.items.length > 0){
                $.each( store.state.items, function( index, value ) {
                    subTotal += parseFloat(value.subTotal);
                    total += parseFloat(value.total);
                });
            }
            store.state.invoice.subTotal = subTotal;
            store.state.invoice.total = total;
            store.state.invoice.payment_type_id = this.payment_type_id;
            return store.state.invoice;
        },
        user(){
            return user;
        },
        account(){
            return store.state.account;
        },
        taxType(){
            return store.state.taxType;
        },
        product(){
            return store.state.product;
        },
        accounts(){
            return store.state.accounts;
        },
        products(){
            return store.state.products;
        },
        taxes(){
            return store.state.taxes;
        },
        items(){
            return store.state.items;
        }
    },

    methods: {
        deleteButton: function () {
            var that = this;
            this.delButton = false;
            $('.itemCheckBoxes:checked').each(function() {
                that.delButton = true;
            });
        },

        deleteItem: function(id) {
            $.each(store.state.items, function( index, value ) {
                if(value.id == id) {
                    store.state.items.splice(index, 1);
                    return false;
                }
            });
        },

        deleteItems: function () {
            var that = this;
            $('.itemCheckBoxes:checked').each(function() {
                that.deleteItem($(this).val());
                $(this).prop( "checked", false );
            });
        },

        saveInvoice: function () {
            var validation = true;
            var a = {
                invoice: this.invoice,
                items: this.items,
                account: this.account,
                taxType: store.state.taxType
            }

            if (this.account.accountable.primary_contact.first_name == '') {
                validation = false;
                $.jGrowl('Please select a customer', {
                    header: 'Error!',
                    theme: 'alert-bordered alert-danger'
                });
            }

            if (this.items.length == 0 ) {
                validation = false;
                $.jGrowl('Please select an item', {
                    header: 'Error!',
                    theme: 'alert-bordered alert-danger'
                });
            }

            if (validation){
                axios.post('/invoices', a)
                    .then(function(response) {
                    if(response.data.result == 'success'){
                        location.href = '/invoice/send-invoice/' + response.data.id;
                    }
                }); 
            }
        }
    },

    mounted(){
        console.log(store.state.account)
        store.state.accounts.push.apply(store.state.accounts, accounts);
        store.state.products.push.apply(store.state.products, products);
        store.state.taxes.push.apply(store.state.taxes, taxes);
        var self = this;

        Vue.nextTick(function () {
            $('#select-tax-rate').select2().on('select2:select', function () {
                self.product.tax = $(this).val();
            });

            $('#select-product').select2({
                tags: true
            }).on('select2:select', function () {
                var val = $(this).val();
                if (!isNaN(val)) {
                    self.product = self.products.find(function (product) {
                        return product.id == val;
                    }.bind(this));
                } else {
                    self.product.name = val;
                }
            });
        });
    }
});
