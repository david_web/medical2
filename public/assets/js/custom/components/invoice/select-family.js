import DatePicker from 'vue-datepicker';
//import {pickerOptions} from '/resources/assets/js/custom/components/invoice/store/defaults';
// import store from '../store/createInvoice';

Vue.component('select-family', {

    data() {
        return {
            invoiceNumber: newInvoiceNumber,
            invoice_date: '',
            due_date: '',
        }
    },

    components: {
        DatePicker,
    },

    computed: {
        accounts(){
            return store.state.accounts;
        },
        pickerOptions(){
            return store.pickerOptions;
        }
    },
    methods: {
        updateInvoiceNumber() {
            store.state.invoice.invoice_number = this.invoiceNumber;
        },
    },
    mounted(){
        var that = this;

        function formatRepo (repo) {
            if (repo.loading) return repo.text;
            var markup = '<div class="select2-result-repository clearfix">';
            if (repo.type == 'family') {
                markup += '<b>' + repo.family_name + '</b>';

                markup += '<div class="select2-result-repository__statistics" style="font-size:11px"><b>Contacts: </b>';
                $.each(repo.contacts, function (key, value) {
                    markup += '<div class="select2-result-repository__forks" style="font-size:11px">' +
                        value.first_name + ' ' + value.last_name + "</div>";
                });
                markup += '</div>';

                markup += '<div class="select2-result-repository__statistics" style="font-size:11px"><b>Memberships: </b>';
                $.each(repo.memberships, function (key, value) {
                    markup += '<div class="select2-result-repository__forks" style="font-size:11px">' +
                        value.first_name + ' ' + value.last_name + "</div>";
                });
                markup += '</div>';

                markup += '</div>'
            }
            if (repo.type == 'contact') {
                markup += repo.first_name + ' ' + repo.last_name + ' (contact)</div>'
            }
            if (repo.type == 'membership') {
                markup += repo.first_name + ' ' + repo.last_name + ' (membership)</div>'
            }
            return markup;
        }
        function formatRepoSelection (repo) {
            return repo.family_name;
        }

        store.state.invoice.invoice_number = this.invoiceNumber;
        Vue.nextTick(function () {
            $(this.$refs.select).select2({
                ajax: {
                    url: ajax_url + "/invoice/get-accounts",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.accounts,
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
            }).on('select2:select', function () {
                store.state.account = store.state.accounts.find(function (account) {
                    return account.id == $(this).val();
                }.bind(this));
            });
        }.bind(this));
        $('#invoiceDate').pickadate({
            format: 'mmmm d, yyyy',
            onSet: function() {
                store.state.invoice.invoice_date = $('#invoiceDate').val();
            }
        });
        $('#dueDate').pickadate({
            format: 'mmmm d, yyyy',
            onSet: function() {
                store.state.invoice.due_date = $('#dueDate').val();
            }
        });

        $('#dueDate').pickadate().pickadate('picker').set('select', new Date());
        $('#invoiceDate').pickadate().pickadate('picker').set('select', new Date());
    }
});