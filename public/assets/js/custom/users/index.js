import Invites from './components/Invites.vue';
import Users from './components/Users.vue';
import Roles from './components/Roles.vue';

export default function () {
    new Vue({
        el: '#main',
        data: {
            permissions: permissions,
            roles: roles,
            users: users,
            notActive: notActive
        },
        methods: {
            getInactive: function (clone) {
                return _.find(this.notActive, function (user) {
                    return user.id == clone.id && user.type == clone.type;
                });
            },
        },
        components:{
            Invites,
            Users,
            Roles,
        }
    });
}