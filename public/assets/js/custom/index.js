//TODO: move eventhub to seperate file
window.eventHub = new Vue();

//Http lib exposed to window
import axios from 'axios';
window.axios = axios;

//Attendances index component
Vue.component('attendance', require('./components/Attendance.vue'));
//teacher management modal
Vue.component('teacher-mm', require('./components/TeacherMM.vue'));
/**
 * Inits
 * */
import vueTableInit from './vue-tables';
vueTableInit();

import globalInits from '../extra/global-inits'
globalInits();

/**
 * Window assignments
 * */
import * as  globals from '../extra/global-functions'

Object.keys(globals).forEach(function (key) {
    window[key] = globals[key];
});


import Users from './users';
import Invoices from './invoices';

window.pageScripts = {
    Users,
    Invoices
};