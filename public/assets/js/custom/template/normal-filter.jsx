  module.exports = function(h, that) {

    if (!that.opts.filterable) return '';

    var search = that.source=='client'?
                that.search.bind(that, that.data):
                that.serverSearch.bind(that);

  if (that.opts.filterable && !that.opts.filterByColumn) {
      var id = 'VueTables__search_' + that.id;
      return <div class="dataTables_filter">
          <label>
              <input type="search" placeholder="Search" class="form-control input-sm"
                  type="text"
                  value={that.query}
                  placeholder={that.display('filterPlaceholder')}
                  on-keyup={search}
                  id={id} />
              </label>
          </div>
    }

    return '';
  }
