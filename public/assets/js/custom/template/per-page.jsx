module.exports = function(h, that) {

  var perpageValues = require('./per-page-values.jsx')(h, that);

  if (perpageValues.length > 1) {
    var id = 'VueTables__limit_' + that.id;
    return  <div class="dataTables_length form-group row">

        <label class="control-label col-sm-6"  for={id}>
          <span>{that.display('limit')}</span>
        </label>
        <div class="col-sm-6">
          <select class="form-control no-select2"
                  name="limit"
                  value={that.limit}
                  ref="limit"
                  on-change={that.setLimit.bind(that)}
                  id={id}
          >
            {perpageValues}
          </select>
        </div>
    </div>
  }

  return '';
}
