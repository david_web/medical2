module.exports = function(source) {
return function(h) {

var rows = require('./template/rows.jsx')(h, this)
var normalFilter = require('./template/normal-filter.jsx')(h, this)
var dropdownPagination = require('./template/dropdown-pagination.jsx')(h, this)
var columnFilters = require('./template/column-filters.jsx')(h, this);
var footerHeadings = require('./template/footer-headings.jsx')(h, this);
var noResults = require('./template/no-results.jsx')(h, this);
var pagination = require('./template/pagination.jsx')(h, this);
var dropdownPaginationCount = require('./template/dropdown-pagination-count.jsx')(h, this);
var headings = require('./template/headings.jsx')(h, this);
var perPage = require('./template/per-page.jsx')(h, this);

return <div class={"VueTables VueTables--" + this.source}>
  <div class="datatable-header pl-20 pr-20">
      {normalFilter}

      {dropdownPagination}
      {perPage}
  </div>

  <table class={'VueTables__table table ' + this.opts.skin}>
    <thead>
      <tr class="bg-slate-600">
        {headings}
      </tr>
      {columnFilters}
    </thead>
    {footerHeadings}
    <tbody>
      {noResults}
      {rows}
    </tbody>
  </table>

  {pagination}
  {dropdownPaginationCount}
</div>
}
}