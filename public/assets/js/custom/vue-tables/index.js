import VueTables from 'vue-tables-2';
import template from './template.jsx';

export default function () {
    var options = {
        wrapperClasses: {'table-responsive': true},
        skin: 'table table-striped admin-form dataTable',
        sortIcon: {
            base: 'glyphicon pull-right',
            up: 'glyphicon-chevron-up',
            down: 'glyphicon-chevron-down'
        }

    };

    Vue.use(VueTables.client, options, false, template('client'));

}