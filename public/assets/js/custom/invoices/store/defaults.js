export default {
    pickerOptions: {
        type: 'day',
        format: 'MMMM D, Y',
        week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        wrapperClass: 'input-group',
        inputClass: 'form-control',

    }
}