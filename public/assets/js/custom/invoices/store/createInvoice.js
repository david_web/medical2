import Vuex from 'vuex';

export default new Vuex.Store({
    state: {
        invoice: {
            invoice_date: '',
            due_date: ''
        },
        accounts: [],
        products: [],
        taxes: [],
        account: {
            accountable: {
                primary_contact: {
                    first_name: '',
                    last_name: '',
                    address1: '',
                    city: '',
                    state: '',
                    home_phone: '',
                    email: ''
                }
            }
        },
        product: {
            name: '',
            description: '',
            price: 0,
            quantity: 1,
            discount: 0,
            tax: 0
        },
        taxType: 'percent',
        items: [],

    }
});