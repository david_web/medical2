import CreateInvoice from './components/CreateInvoice.vue';
export default function () {
    new Vue({
        el: '#main',
        components:{
            CreateInvoice
        },
    });
}