jQuery(document).ready(function() {
    /* Files */
        $('body').on('click', '.edit_file', function() {
            var id = $(this).attr('data-id');
            var view = 'materials';
            $.ajax({
                url: '/edit-file/' + id,
                method: 'GET',
                data: {view:view},
                success: function(result) {
                    if (result.success == true) {
                        $('#post_modal_content').html(result.html);
                        $('#country').select2();
                        $('#edit_post').modal('show');
                    }
                }
            });
        })

        $('body').on('click', '#edit_post_button', function() {
            var token = $('[name=csrf-token]').attr('content');
            var id = $(this).attr('data-id');
            var action = 'materials';

            var country = $('#country').val();
            var institution_name = $('#institution_name').val();
            var course = $('#course').val();
            var language = $('#language').val();
            var description = $('#description').val();
            var is_public = $('#hidden_public').val();
            $.ajax({
                url: '/edit-file/' + id,
                method: 'POST',
                data: {_token:token, action:action, country:country, institution_name:institution_name, course:course, language:language, description:description, is_public:is_public},
                success: function(result) {
                    if (result.success == true) {
                        $('#files_div').html(result.html);
                        $('#edit_post').modal('hide');
                    }
                },
                error: function(errorResult) {
                    if (errorResult.responseJSON['country'] != undefined) {
                        $('#country-error').html(errorResult.responseJSON['country']);
                        $('#country_div').addClass('has-error');
                        $('.select2-selection').css('border-color', '#D84315');
                    } else {
                        $('#country-error').html('');
                        $('#country_div').removeClass('has-error');
                        $('.select2-selection').css('border-color', '#ddd');
                    }

                    if (errorResult.responseJSON['institution_name'] != undefined) {
                        $('#institution_name-error').html(errorResult.responseJSON['institution_name']);
                        $('#institution_div').addClass('has-error');
                    } else {
                        $('#institution_name-error').html('');
                        $('#institution_div').removeClass('has-error');
                    }

                    if (errorResult.responseJSON['course'] != undefined) {
                        $('#course-error').html(errorResult.responseJSON['course']);
                        $('#course_div').addClass('has-error');
                    } else {
                        $('#course-error').html('');
                        $('#course_div').removeClass('has-error');
                    }

                    if (errorResult.responseJSON['language'] != undefined) {
                        $('#language-error').html(errorResult.responseJSON['language']);
                        $('#language_div').addClass('has-error');
                    } else {
                        $('#language-error').html('');
                        $('#language_div').removeClass('has-error');
                    }

                    if (errorResult.responseJSON['description'] != undefined) {
                        $('#description-error').html(errorResult.responseJSON['description']);
                        $('#description_div').addClass('has-error');
                    } else {
                        $('#description-error').html('');
                        $('#description_div').removeClass('has-error');
                    }
                }
            })
        })

        $('body').on('click', '.delete_file_button', function() {
            id = $(this).attr('data-id');
            $.ajax({
                url: '/delete-file/' + id,
                method: 'GET',
                success: function(result) {
                    if (result.success == true) {
                        $('#file_div' + result.id).remove();
                    }
                }
            })
        })
    /* /Files */

    /* Comments */
        $('body').on('click', '.add_comment1', function() {
            var id = $(this).attr('data-id');
            $('#comment' + id).show(100);
        	$('#comment_' + id).hide();
        });

        $('body').on( "click", '.add_comment', function() {
        	var token = $('[name=csrf-token]').attr('content');
            var id = $(this).attr('data-id');
        	var comment = $('#comment-' + id).val();
            $.ajax({
                url: '/add-file-comment',
                method: 'POST',
                data: {id:id, comment:comment, _token:token},
                success: function(result) {
                	if (result.success == true) {
                		$('.comments' + id).append(
                			'<tr id = "com' + result.comment.id + '"><td style="font-size: 13px">' + result.comment.comment + ' –&nbsp; <a href="#">' +  result.comment.user.first_name
                            + ' ' + result.comment.user.last_name + '</a> <span class="text-muted">Apr 13 at 14:51<span><br>' + 
                            '<a>edit</a> <a class = "del_comment" data-id =' + result.comment.id + '>delete</a></td></tr>'
                		);
                    	$('#comment' + id).hide();
            			$('#comment_' + id).show();
                    	$('#comment-' + id).val('');
                	}
                }
        	});
        });

        $('body').on('click', '.del_comment', function() {
            var id = $(this).attr('data-id');
        	$.ajax({
        		url: '/delete-comment/' + id,
                method: 'GET',
                success: function(result) {
                	if (result.success == true) {
                        $('#com' + id).remove();
                	}
                }
        	});
        });
    /* /Comments */
});