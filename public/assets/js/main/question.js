$(document).ready(function() {
    $('#comment').click(function() {
    	$('#comment1').show(100);
    	$('#comment').hide();
    });

    $( ".add_comment" ).on( "click", function() {
    	var token = $('[name=csrf-token]').attr('content');
    	var comment = $('.comment').val();
    	var id = $(this).attr('data-id');

        $.ajax({
            url: '/add-comment',
            method: 'POST',
            data: {id:id, comment:comment, _token:token},
            success: function(result) {
            	if (result.success == true) {
            		$('.comments').append(
            			'<tr id = ' + result.comment.id + '><td style="font-size: 13px">' + result.comment.comment + ' –&nbsp; <a href="#">' +  result.comment.user.first_name
                        + ' ' + result.comment.user.last_name + '</a> <span class="text-muted">Apr 13 at 14:51<span><br>' + 
                        '<a>edit</a> <a class = "del_comment" data-id =' + result.comment.id + '>delete</a></td></tr>'
            		);
                	$('#comment1').hide();
        			$('#comment').show();
                	$('.comment').val('');
            	}
            }
    	});
    });

    $('body').on('click', '.del_comment', function() {
        var id = $(this).attr('data-id');
    	$.ajax({
    		url: '/delete-comment/' + id,
            method: 'GET',
            success: function(result) {
            	if (result.success == true) {
                    $('#'+ id).remove();
            	}
            }
    	});
    });

    $('#like').click(function() {
        var id = $('#like').attr('data-id');

        $.ajax({
            url: '/add-like/' + id,
            method: 'GET',
            success: function(result) {
                if (result.success) {
                    if (result.isLiked) {
                        $('.like1').attr('src','/assets/images/like-field.png');
                    } else {
                        $('.like1').attr('src','/assets/images/like.png');
                    }
                    if(result.likesCount != 0) {
                        $('.likesCount').html(result.likesCount);
                    } else {
                        $('.likesCount').html('');
                    }
                }
            }
        });
    });
});