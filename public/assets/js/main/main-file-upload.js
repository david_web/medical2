$('#country').select2();

if($('#countries_div').hasClass('has-error')) {
	$('.select2-selection').css('border-color', '#D84315');
}

if($('#file_div').hasClass('has-error')) {
	$('.filename').css('border-color', '#D84315');
}