$(".group_users").select2();

function displayUsers() {
	var users = $( ".group_users" ).val() || [];
	$('#user').val(JSON.stringify(users));
}
$( "select" ).change( displayUsers );

$('.accept').on('click', function() {
	var id = $(this).attr('data-id');
	$.ajax({
		url: '/accept-request/' + id,
        method: 'GET',
        success: function(result) {
        	if (result.success == true) {
        		$('#request' + id).remove();
        	}
        }
	})
});

$('.reject').on('click', function() {
	var id = $(this).attr('data-id');
	$.ajax({
		url: '/reject-request/' + id,
        method: 'GET',
        success: function(result) {
        	if (result.success == true) {
        		$('#request' + id).remove();
        	}
        }
	})
});
