$(".group_users").select2();

function displayUsers() {
	var users = $( ".group_users" ).val() || [];
	$('#user').val(JSON.stringify(users));
}
$( "select" ).change( displayUsers );

$('.leave_society_button').on('click', function() {
	var id = $(this).attr('data-id');
	$.ajax({
		url: '/leave-society/' + id,
		method: 'GET',
		success: function(result) {
			if (result.success == true) {
				$('#req' + result.id).html('<a class="ask_to_join" data-id="' + result.id + '"><i style="font-size: 12px" class="glyphicon glyphicon-plus"></i> ask to join</a>');
				$('#name' + result.id).html('<span style="font-size: 18px"><b>' + result.name + '</b></span>')
        	}
        }
	})
})

$('body').on('click', '.ask_to_join', function() {
	var id = $(this).attr('data-id');
	$.ajax({
		url: '/ask-to-join/' + id,
		method: 'GET',
		success: function(result) {
			if (result.success == true) {
				console.log(result.id)
				$('#req' + result.id).html('<span>request has been sent</span>');
        	}
        }
	})
})