jQuery(document).ready(function() {
	window.modal_folders = {};
	$('#country').select2();
	$('body').on('dblclick', '.modal_folder', function() {

		var id = $(this).attr('data-id');
		var name = $(this).attr('data-name');
		$('#save_folder').attr('data-id', id);
		modal_folders[id] = name;
		$.ajax({
            url: '/modal-folder/' + id,
            method: 'GET',
            success: function(result) {
			if (result.success == true) {
            		$('#modal_folder').html(result.html);
            		$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
            		$.each(modal_folders, function(key, value) {
						$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
					});
            	}
            }
		});
	});

	$('body').on('click', '.modal_folder', function() {
		$('.folder_img').attr('src', '/assets/images/folder.png');
		$('#folder_img' + $(this).attr('data-id')).attr('src', '/assets/images/select-folder.png');
		$('#folder_select').html('Selected folder: ' +  $(this).attr('data-name'));
		$('#selected_child_folder').html('Selected folder: ' + $(this).attr('data-name'));
		var id = $(this).attr('data-id');
		$('#save_folder').attr('data-id', id);
		$('#save_folder').attr('data-name', $(this).attr('data-name'));
	});
	
	$('#save_folder').on('click', function() {
		$('#choose').val($(this).attr('data-name'));
		$('#hidden_choose').val($(this).attr('data-id'));
	});

	$('body').on('click', '.modal_folder_a', function() {
		var id = $(this).attr('data-id');
		$.ajax({
            url: '/modal-folder/' + id,
            method: 'GET',
            success: function(result) {
			if (result.success == true) {
    				$('#modal_folder').html(result.html);
					$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
					var removeModalFolder = {};
					$.each(modal_folders, function(key, value) {
						removeModalFolder[key] = value;
						$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
						if (key == id)
							return false;
					});
					modal_folders = removeModalFolder;
            	}
            }
		});
	});

	$('body').on('click', '#modal_my_library', function() {
		$.ajax({
            url: '/modal-folder',
            method: 'GET',
            success: function(result) {
			if (result.success == true) {
    				$('#modal_folder').html(result.html);
					$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
					modal_folders = {};
            	}
            }
		});
	});

	$('body').on('click', '#modal_folder', function() {
		if (event.target !== this)
		return;
		$('.folder_img').attr('src', '/assets/images/folder.png');
		if(jQuery.isEmptyObject(modal_folders)) {
			$('#save_folder').removeAttr('data-name');
			$('#save_folder').removeAttr('data-id');
			$('#folder_select, #selected_child_folder').html("You didn't select any folder");
		}
		else {
			var keys = Object.keys(modal_folders);
			$('#save_folder').attr('data-name', modal_folders[keys[keys.length - 1]]);
			$('#save_folder').attr('data-id', keys[keys.length - 1]);
			$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
		}
	});

	if($('#file_div').hasClass('has-error')) {
		$('.filename').css('border-color', '#D84315');
	}

	if($('#countries_div').hasClass('has-error')) {
		$('.select2-selection').css('border-color', '#D84315');
	}

	$('#checkbox').on('click', function() {
		if ($('#public').is(':checked')) {
			$('#hidden_public').val(1);
		} else {
			$('#hidden_public').val(0);
		}
	})

});