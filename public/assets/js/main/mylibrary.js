jQuery(document).ready(function() {
	/* Folders */
		window.folders = {};
		$('body').on('dblclick', '.folder', function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			$('#btn_add_folder').attr('data-id', id);
			folders[id] = name;
			$.ajax({
	            url: '/folder/' + id,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	            		$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	            		$('#folder').html(result.html);
						$.each(folders, function(key, value) {
							$('.folder_route').append('<a class="folder_a" data-id="' + key + '">' + value + '/ </a>');
						});
	            	}
	            }
			});
		});

		$('body').on('click', '#button_add_folder', function() {
			$('#error').html('');
			$('#form').removeClass('has-error');
			$('#folder_name').val('');
		});

		$('body').on('click', '.folder_a', function() {
			var id = $(this).attr('data-id');
			$('#btn_add_folder').attr('data-id', id);
			$.ajax({
	            url: '/folder/' + id,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	    				$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	    				$('#folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						var removeFolder = {};
						$.each(folders, function(key, value) {
							removeFolder[key] = value;
							$('.folder_route').append('<a class="folder_a" data-id="' + key + '">' + value + '/ </a>');
							if (key == id)
								return false;
						});
						folders = removeFolder;
	            	}
	            }
			});
		});

		$('body').on('click', '#my_library', function() {
			$('#btn_add_folder').removeAttr('data-id');
			$.ajax({
	            url: '/folder',
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	    				$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	    				$('#folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						folders = {};
	            	}
	            }
			});
		});

		$('body').on('click', '#btn_add_folder', function() {
			var token = $('[name=csrf-token]').attr('content');
	    	console.log(token);
			
	        var id = $(this).attr('data-id');
	    	var name = $('#folder_name').val();
	    	url = '/add-folder/';
	    	console.log(id);

	    	if(id != undefined)
	    		url += id;
	    	console.log(url);
			$.ajax({
	            url: url,
	            method: 'POST',
	            data: {name:name, _token:token},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#folder1').append(result.html);
	            		$('#folder_name').val("");
	            		$('#add_folder').modal('hide');
	            	}
	            },
	            error: function(errorResult) {
					$('#error').html(errorResult.responseJSON['name']);
					$('#form').addClass('has-error');
	            }
	    	});
		});

		$('body').on('mouseover', '.folder', function() {
			$('#delete_folder' + $(this).attr('data-id')).show();
		});

		$('body').on('mouseout', '.folder', function() {
			$('#delete_folder' + $(this).attr('data-id')).hide();
		});

		$('body').on('click', '.delete_folder_button', function() {
			var id = $(this).attr('data-id');
			$.ajax({
	            url: '/delete-folder/' + id,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	    				$('#folder_div' + id).remove();
	    				$('#br' + id).remove();
	            	}
	            }
			});
		});
	/* /Folders */

	/* Modal-folders */
		window.modal_folders = {};
		$('#country').select2();
		$('body').on('dblclick', '.modal_folder', function() {

			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			$('#save_folder').attr('data-id', id);
			modal_folders[id] = name;
			$.ajax({
	            url: '/modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	            		$('#modal_folder').html(result.html);
	            		$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
	            		$.each(modal_folders, function(key, value) {
							$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
						});
	            	}
	            }
			});
		});

		$('body').on('click', '.modal_folder', function() {
			$('.folder_img').attr('src', '/assets/images/folder.png');
			$('#folder_img' + $(this).attr('data-id')).attr('src', '/assets/images/select-folder.png');
			$('#folder_select').html('Selected folder: ' +  $(this).attr('data-name'));
			$('#selected_child_folder').html('Selected folder: ' + $(this).attr('data-name'));
			var id = $(this).attr('data-id');
			$('#save_folder').attr('data-id', id);
			$('#save_folder').attr('data-name', $(this).attr('data-name'));
		});
		
		$('#save_folder').on('click', function() {
			$('#choose').val($(this).attr('data-name'));
			$('#hidden_choose').val($(this).attr('data-id'));
		});

		$('body').on('click', '.modal_folder_a', function() {
			var id = $(this).attr('data-id');
			$.ajax({
	            url: '/modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	    				$('#modal_folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						var removeModalFolder = {};
						$.each(modal_folders, function(key, value) {
							removeModalFolder[key] = value;
							$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
							if (key == id)
								return false;
						});
						modal_folders = removeModalFolder;
	            	}
	            }
			});
		});

		$('body').on('click', '#modal_my_library', function() {
			$.ajax({
	            url: '/modal-folder',
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
	    				$('#modal_folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						modal_folders = {};
	            	}
	            }
			});
		});

		$('body').on('click', '#modal_folder', function() {
			if (event.target !== this)
			return;
			$('.folder_img').attr('src', '/assets/images/folder.png');
			if(jQuery.isEmptyObject(modal_folders)) {
				$('#save_folder').removeAttr('data-name');
				$('#save_folder').removeAttr('data-id');
				$('#folder_select, #selected_child_folder').html("You didn't select any folder");
			}
			else {
				var keys = Object.keys(modal_folders);
				$('#save_folder').attr('data-name', modal_folders[keys[keys.length - 1]]);
				$('#save_folder').attr('data-id', keys[keys.length - 1]);
				$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
			}
		});
	/* /Modal-folders */

	/* Files */
		$('body').on('click', '.edit_file', function() {
    		var id = $(this).attr('data-id');
    		var view = 'mylibrary';
    		$.ajax({
	            url: '/edit-file/' + id,
	            method: 'GET',
	            data: {view:view},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#post_modal_content').html(result.html);
						$('#country').select2();
	            		$('#add_post').modal('show');
	            		$('#choose_folder_button, #choose').on('click', function() {
							$.ajax({
					            url: '/modal-folder',
					            method: 'GET',
					            success: function(result) {
								if (result.success == true) {
					    				$('#modal_folder').html(result.html);
										$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
										modal_folders = {};
					            	}
					            }
							});
						});
	            	}
	            }
	    	});
    	})

		$('body').on('click', '#edit_post_button', function() {
	    	var token = $('[name=csrf-token]').attr('content');
    		var id = $(this).attr('data-id');
    		var folderId = $('#folder1').attr('data-id');

    		var country = $('#country').val();
    		var institution_name = $('#institution_name').val();
    		var course = $('#course').val();
    		var language = $('#language').val();
    		var description = $('#description').val();
    		var is_public = $('#hidden_public').val();
    		var folder_id = $('#hidden_choose').val();
    		$.ajax({
    			url: '/edit-file/' + id,
    			method: 'POST',
    			data: {_token:token, folderId:folderId, country:country, institution_name:institution_name, course:course, language:language, description:description, folder_id:folder_id, action:null, is_public:is_public},
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            		$('#add_post').modal('hide');
	            	}
            	},
            	error: function(errorResult) {
		        	if (errorResult.responseJSON['country'] != undefined) {
						$('#country-error').html(errorResult.responseJSON['country']);
						$('#country_div').addClass('has-error');
						$('.select2-selection').css('border-color', '#D84315');
					} else {
		        		$('#country-error').html('');
		        		$('#country_div').removeClass('has-error');
		        		$('.select2-selection').css('border-color', '#ddd');
		        	}

		        	if (errorResult.responseJSON['institution_name'] != undefined) {
						$('#institution_name-error').html(errorResult.responseJSON['institution_name']);
						$('#institution_div').addClass('has-error');
					} else {
		        		$('#institution_name-error').html('');
		        		$('#institution_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['course'] != undefined) {
						$('#course-error').html(errorResult.responseJSON['course']);
						$('#course_div').addClass('has-error');
					} else {
		        		$('#course-error').html('');
		        		$('#course_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['language'] != undefined) {
						$('#language-error').html(errorResult.responseJSON['language']);
						$('#language_div').addClass('has-error');
					} else {
		        		$('#language-error').html('');
		        		$('#language_div').removeClass('has-error');
		        	}

		        	if (errorResult.responseJSON['description'] != undefined) {
						$('#description-error').html(errorResult.responseJSON['description']);
						$('#description_div').addClass('has-error');
					} else {
		        		$('#description-error').html('');
		        		$('#description_div').removeClass('has-error');
		        	}
		        }
    		})
    	})

    	$('body').on('click', '.delete_file_button', function() {
    		id = $(this).attr('data-id');
    		$.ajax({
	            url: '/delete-file/' + id,
	            method: 'GET',
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#file_div' + result.id).remove();
	            	}
            	}
            })
    	})
	/* /Files */
});