jQuery(document).ready(function() {
	$('#create_poll_button').on('click', function() {
		var id = $('#poll_panel').attr('data-id');
		$.ajax({
			url: '/add-poll/' + id,
			method: 'GET',
			success: function(result) {
				if (result.success == true) {
					$('#poll_modal_content').html(result.html);
					$('#poll_modal').modal('show');
	            }
	        }
		})
	})

	$('body').on('click', '#add_option', function() {
		$('#options').append('<li><div><label for="option" class="control-label">option</label><input type="text" name="option" class="form-control option"></div></li>')
	})

	$('body').on('click', '#btn_add_poll', function() {
		var token = $('[name=csrf-token]').attr('content');
		var id = $('#poll_panel').attr('data-id');
		var pollName = $('#name').val();
		var public = 0;
		if ($('#public').is(':checked')) public = 1;
		window.options = new Array();
		$("#options input").each(function() {
		  options.push($(this).val());
		});

		$.ajax({
			url: '/add-poll/' + id,
			method: 'POST',
			data: {_token:token, name:pollName, options:options, is_public:public},
			success: function(result) {
				if (result.success == true) {
					$('#polls_div').html(result.html);
					$('#poll_modal').modal('hide');
	            } else {
	            	$('#opt_err_div').show();
	            	$('#name-error').html('');
	        		$('#name_div').removeClass('has-error');
	            }
	        },
	        error: function(errorResult) {
	        	console.log(errorResult.responseJSON['name']);
	        	if (errorResult.responseJSON['name'] != undefined) {
					$('#name-error').html(errorResult.responseJSON['name']);
					$('#name_div').addClass('has-error');
	        	} else {
	        		$('#name-error').html('');
	        		$('#name_div').removeClass('has-error');
	        	}
	        }
		})
	})

	$('body').on('click', '.poll_option', function() {
		var id = $(this).attr('data-id');
		var groupId = $('#poll_panel').attr('data-id');
		$.ajax({
			url: '/choose-option/' + id + '/' + groupId,
			method: 'GET',
			success: function(result) {
				if (result.success == true) {
					$('#poll_div' + result.pollId).html(result.html);
	            }
	        }
		})
	})

	$('body').on('click', '.delete_poll', function() {
		var id = $(this).attr('data-id');
		var groupId = $('#poll_panel').attr('data-id');
		$.ajax({
			url: '/delete-poll/' + id + '/' + groupId,
			method: 'GET',
			success: function(result) {
				if (result.success == true) {
					$('#polls_div').html(result.html);
	            }
	        }
		})
	})

	$('body').on('click', '.option_users', function() {
		var id = $(this).attr('data-id');
		var groupId = $('#poll_panel').attr('data-id');
		$.ajax({
			url: '/option-users/' + id + '/' + groupId,
			method: 'GET',
			success: function(result) {
				if (result.success == true) {
					$('#option_users_content').html(result.html);
					$('#option_users_modal').modal('show');
	            }
	        }
		})
	})
})