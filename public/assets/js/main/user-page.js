jQuery(document).ready(function() {
	$('#add_friend_button').on('click', function() {
		var id = $(this).attr('data-id');
		$.ajax({
			url: '/add-friend/' + id,
			method: 'GET',
			success: function(result) {
				if (result.success == true) {
					console.log(result.id);
					$('#add_friend_button').remove();
					$('#add_friend_div').html('<div class="btn"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Request has been sent <span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right" style="padding: 0;"><li><a href="/remove-friend/'
					 + result.id + '">Cancel</a></li></ul></div>')
            	}
            }
		})
	})
	$('#friend_discussion').click(function(){
		var id = $(this).attr('data-id');
		$.ajax({
			url:'/friend-discussion/' + id,
			method:'GET',
			 success: function(result){
			 	if (result.success == true) {			 		
					$('#friend_library_discussions').html(result.html);
				}
			}
		});
	})
	$('#friend_library').click(function(){
		var id = $(this).attr('data-id');
		
		$.ajax({
			url:'/friend-library/' + id,
			method:'GET',
			success: function(result){
			 	if (result.success == true) {	
					$('#friend_library_discussions').html(result.html);
				}
			}
		});
	})
	
})