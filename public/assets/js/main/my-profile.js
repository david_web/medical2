$(document).ready(function(){

	$('#friends_sel').select2();
		window.public = true;

	$('#user_discussion').click(function(){
		
		$.ajax({
			url:'user-discussion',
			method:'get',
			 success: function(result){
			 	if (result.success == true) {
					$('#library_discussions').html(result.html);
				}
			}
		});
		
	});
	$('#user_library').click(function(){
		
		$.ajax({
			url:'user-library',
			metod:'get',
			 success: function(result){
			 	if (result.success == true) {
			 		
					$('#library_discussions').html(result.html);
				}
			}
		});
	});
	// 
	$('#create_discussion').click(function(){
		var name = $('#inp').val();
		var id = $(this).attr('data-id');
		var user_id = $('.option').val();
		var token = $('[name=csrf-token]').attr('content');
		$.ajax({
			url:'create-discussion',
			method:'post',
			data:{id:id,user_id:user_id,name:name,_token:token},
			success:function(result){
				if (result.success == true) {
					$('#modal_iconified').modal('hide');
				}
			}

		})
	})
	$('body').on('click', '.start_chat', function(){
		var id = $(this).attr('data-id');
		var user_id =$(this).attr('data');
		var token = $('[name=csrf-token]').attr('content');
		var name = $(this).attr('data-topic');
		 $.ajax({
		 	url:'start-chat',
		 	method:'post',
		 	data:{id:id,user_id:user_id,_token:token,name:name},
		 	success: function(result){
		 		if (result.success == true){
		 			
		 			$('#li' + id).fadeOut("slow");
		 		}
		 	 }
		 });
	});

	$('#public').click(function(event){
		if ($(this).parent().find('input').is(':checked')){
	
		window.public = true;	
		
		}else{
		
			window.public = false;
		}

	});
	$('body').on('click','.reject',function(){
		var id = $(this).attr('data-id');
		var user_id =$(this).attr('data');
		var token = $('[name=csrf-token]').attr('content');
		$.ajax({
			url:'reject-discussion',
			method:'post',
			data:{id:id,user_id:user_id,_token:token},
			success:function(result){
				if (result.success == true){
		 			
		 			$('#li' + id).fadeOut("slow");
		 		}
			}
		})
	});
})