jQuery(document).ready(function() {
	$('#my_friends').on('click', function() {
		$.ajax({
			url: '/friends',
            method: 'GET',
            success: function(result) {
				if (result.success == true) {
					$('#friends_div').html(result.html);
            	}
            }
		})
	})

	$('#friend_requests').on('click', function() {
		$.ajax({
			url: '/friend-requests',
            method: 'GET',
            success: function(result) {
				if (result.success == true) {
					$('#friends_div').html(result.html);
            	}
            }
		})
	})
})