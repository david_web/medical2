jQuery(document).ready(function() {
	$(".group_users").select2();
	$("#country").select2();
	function displayUsers() {
		var users = $( ".group_users" ).val() || [];
		$('#user').val(JSON.stringify(users));
	}
	$( "select" ).change( displayUsers );

	/* Folders */
		window.folders = {};
		$('body').on('dblclick', '.folder', function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			var groupId = $('#folder').attr('data-id');
			$('#btn_add_folder').attr('data-id', id);
			folders[id] = name;
			$.ajax({
	            url: '/folder/' + id + '/' + groupId,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
						$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	            		$('#folder').html(result.html);
	            		$('#folder_name_span').html(result.name);
						$.each(folders, function(key, value) {
							$('.folder_route').append('<a class="folder_a" data-id="' + key + '">' + value + '/ </a>');
						});
	            	}
	            }
			});
		});

		$('body').on('click', '.folder_a', function() {
			var id = $(this).attr('data-id');
			var groupId = $('#folder').attr('data-id');
			$('#btn_add_folder').attr('data-id', id);
			$.ajax({
	            url: '/folder/' + id + '/' + groupId,
	            method: 'GET',
	            success: function(result) {
				if (result.success == true) {
						$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	    				$('#folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
	            		$('#folder_name_span').html(result.name);
						var removeFolder = {};
						$.each(folders, function(key, value) {
							removeFolder[key] = value;
							$('.folder_route').append('<a class="folder_a" data-id="' + key + '">' + value + '/ </a>');
							if (key == id)
							return false;
						});
						folders = removeFolder;
	            	}
	            }
			});
		});

		$('body').on('click', '#my_library', function() {
			$('#btn_add_folder').removeAttr('data-id');
			var id = $('#folder').attr('data-id');
			$.ajax({
	            url: '/group-folder/' + id,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
						$('#folder_route_div').html('<span style="font-size: 20px" class="folder_route"><a id="my_library">library/</a></span>');
	    				$('#folder').html(result.html);
	    				$('#folder_name_span').html('');
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						folders = {};
	            	}
	            }
			});
		});

		$('body').on('click', '#button_add_folder', function() {
			$('#error').html('');
			$('#form').removeClass('has-error');
			$('#folder_name').val('');
		});

		$('body').on('click', '#btn_add_folder', function() {
			var token = $('[name=csrf-token]').attr('content');
	    	var name = $('#folder_name').val();
	        var id = $(this).attr('data-id');
	    	var group = $('#folder').attr('data-id');
	    	var url = '/add-group-folder/';
	    	if(id != undefined) {
	    		url += id;
	    		var action = 'parent';
	    	} else {
	    		url += group;
	    		var action = 'group';
	    	};
			$.ajax({
	            url: url,
	            method: 'POST',
	            data: {name:name, _token:token, action:action},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#folder1').append(result.html);
	            		$('#folder_name').val("");
	            		$('#add_folder').modal('hide');
	            	}
	            },
	            error: function(errorResult) {
					$('#error').html(errorResult.responseJSON['name']);
					$('#form').addClass('has-error');
	            }
	    	});
		});

		if ($('#button_add_folder').attr('data-allowed') != undefined) {
			$('body').on('mouseover', '.folder', function() {
				$('#delete_folder' + $(this).attr('data-id')).show();
			});

			$('body').on('mouseout', '.folder', function() {
				$('#delete_folder' + $(this).attr('data-id')).hide();
			});
		}

		$('body').on('click', '.delete_folder_button', function() {
			var groupId = $('#folder').attr('data-id');
			var id = $(this).attr('data-id');
			$.ajax({
	            url: '/delete-folder/' + id + '/' + groupId,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
	    				$('#folder_div' + id).remove();
	    				$('#br' + id).remove();
	            	}
	            }
			});
		});

		$('#folder_modal, #save_folder').on('click', function(){
			$('#choose_folder').modal('hide');
		});
	/* /Folders */

	/* Modal folders */
		window.modal_folders = {};
		$('#country').select2();
		$('body').on('dblclick', '.modal_folder', function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			$('#save_folder').attr('data-id', id);
			modal_folders[id] = name;
			$.ajax({
	            url: '/modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
	            		$('#modal_folder').html(result.html);
	            		$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
	            		$.each(modal_folders, function(key, value) {
							$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
						});
	            	}
	            }
			});
		});

		$('body').on('click', '.modal_folder', function() {
			$('.folder_img').attr('src', '/assets/images/folder.png');
			$('#folder_img' + $(this).attr('data-id')).attr('src', '/assets/images/select-folder.png');
			$('#folder_select').html('Selected folder: ' +  $(this).attr('data-name'));
			$('#selected_child_folder').html('Selected folder: ' + $(this).attr('data-name'));
			var id = $(this).attr('data-id');
			$('#save_folder').attr('data-id', id);
			$('#save_folder').attr('data-name', $(this).attr('data-name'));
		});
		
		$('#save_folder').on('click', function() {
			$('#choose').val($(this).attr('data-name'));
			$('#note_folder').val($(this).attr('data-name'));
			$('#hidden_choose').val($(this).attr('data-id'));
			$('#note_folder_id').val($(this).attr('data-id'));
		});

		$('body').on('click', '.modal_folder_a', function() {
			var id = $(this).attr('data-id');
			$.ajax({
	            url: '/modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
	    				$('#modal_folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						var removeModalFolder = {};
						$.each(modal_folders, function(key, value) {
							removeModalFolder[key] = value;
							$('.modal_folder_route').append('<a class="modal_folder_a" data-id="' + key + '">' + value + '/ </a>');
							if (key == id)
								return false;
						});
						modal_folders = removeModalFolder;
	            	}
	            }
			});
		});

		$('body').on('click', '#modal_folder', function() {
			if (event.target !== this)
			return;
			$('.folder_img').attr('src', '/assets/images/folder.png');
			if(jQuery.isEmptyObject(modal_folders)) {
				$('#save_folder').removeAttr('data-name');
				$('#save_folder').removeAttr('data-id');
				$('#folder_select, #selected_child_folder').html("You didn't select any folder");
			}
			else {
				var keys = Object.keys(modal_folders);
				$('#save_folder').attr('data-name', modal_folders[keys[keys.length - 1]]);
				$('#save_folder').attr('data-id', keys[keys.length - 1]);
				$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
			}
		});

		$('body').on('click', '#modal_my_library', function() {
			var id = $('#modal_folder').attr('data-id');
			$.ajax({
	            url: '/group-modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
	    				$('#modal_folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						modal_folders = {};
	            	}
	            }
			});
		});

		$('body').on('click', '#choose_folder_button, #choose, #note_folder, #choose_folder_button', function() {
			console.log(1);
			var id = $('#modal_folder').attr('data-id');
			$.ajax({
	            url: '/group-modal-folder/' + id,
	            method: 'GET',
	            success: function(result) {
					if (result.success == true) {
	    				$('#modal_folder').html(result.html);
						$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
						modal_folders = {};
	            	}
	            }
			});
		});
	/* /Modal folders */

	/* Modal errors */
		if($('#image_div').hasClass('has-error')){
			$( '#upload_image_img' ).trigger( "click" );
		};

		if($('#country_div').hasClass('has-error') || $('#institution_div').hasClass('has-error') || $('#course_div').hasClass('has-error') || $('#language_div').hasClass('has-error') ||
		$('#description_div').hasClass('has-error') || $('#file_div').hasClass('has-error')){
			$('#add_post').modal("show");
		};
	/* /Modal errors */

	/* Comments */
		$('body').on('click', '.add_comment1', function() {
	        var id = $(this).attr('data-id');
	        $('#comment' + id).show(100);
	    	$('#comment_' + id).hide();
	    });

	    $( "body" ).on( "click", '.add_comment', function() {
	    	var token = $('[name=csrf-token]').attr('content');
	        var id = $(this).attr('data-id');
	    	var comment = $('#comment-' + id).val();
	        $.ajax({
	            url: '/add-file-comment',
	            method: 'POST',
	            data: {id:id, comment:comment, _token:token},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('.comments' + id).append(
	            			'<tr id = "com' + result.comment.id + '"><td style="font-size: 13px">' + result.comment.comment + ' –&nbsp; <a href="">' +  result.comment.user.first_name
	                        + ' ' + result.comment.user.last_name + '</a> <span class="text-muted">Apr 13 at 14:51<span><br>' + 
	                        '<a>edit</a> <a class = "del_comment" data-id =' + result.comment.id + '>delete</a></td></tr>'
	            		);
	                	$('#comment' + id).hide();
	        			$('#comment_' + id).show();
	                	$('#comment-' + id).val('');
	            	}
	            }
	    	});
	    });

	    $('body').on('click', '.del_comment', function() {
	        var id = $(this).attr('data-id');
	    	$.ajax({
	    		url: '/delete-comment/' + id,
	            method: 'GET',
	            success: function(result) {
	            	if (result.success == true) {
	                    $('#com' + id).remove();
	            	}
	            }
	    	});
	    });
    /* /Comments */
    
    /* Admin functions */
    	$('.radioRole').on('click', function() {
	    	var token = $('[name=csrf-token]').attr('content');
    		var role = $(this).attr('data-role');
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		$.ajax({
	            url: '/change-role/' + groupId,
	            method: 'POST',
	            data: {id:id, role:role, _token:token},
	            success: function(result) {

	            }
	    	});
    	})

    	$('.delete_user').on('click', function() {
    		var id = $(this).attr('data-id');
    		$('#delete_user_modal' + id).modal('show');
    	})

    	$('.delete_user_no, .close').on('click', function() {
    		var id = $(this).attr('data-id');
    		$('#delete_user_modal' + id).modal('hide');
    	})
    /* /Admin functions */

    /* Files */
    	$('body').on('click', '.edit_file', function() {
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		var view = 'group';
    		$.ajax({
	            url: '/edit-file/' + id + '/' + groupId,
	            method: 'GET',
	            data: {view:view},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#post_modal_content').html(result.html);
						$('#country').select2();
	            		$('#add_post').modal('show');
	            		$('#choose_folder_button, #choose').on('click', function() {
							id = $('#modal_folder').attr('data-id');
							$.ajax({
					            url: '/group-modal-folder/' + id,
					            method: 'GET',
					            success: function(result) {
								if (result.success == true) {
					    				$('#modal_folder').html(result.html);
										$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
										modal_folders = {};
					            	}
					            }
							});
						});
	            	}
	            }
	    	});
    	})

    	$('body').on('click', '#button_add_post', function() {
    		var id = $('#group').attr('data-id');
    		var view = 'group';
    		$.ajax({
	            url: '/add-file/' + id,
	            method: 'GET',
	            data: {view:view},
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#post_modal_content').html(result.html);
						$('#country').select2();
	            		$('#add_post').modal('show');
	            		$('#choose_folder_button, #choose').on('click', function() {
							id = $('#modal_folder').attr('data-id');
							$.ajax({
					            url: '/group-modal-folder/' + id,
					            method: 'GET',
					            success: function(result) {
								if (result.success == true) {
					    				$('#modal_folder').html(result.html);
										$('#selected_child_folder').html('Selected folder: ' + $('#save_folder').attr('data-name'));
										modal_folders = {};
					            	}
					            }
							});
						});
	            	}
	            }
	    	});
    	})

    	$('body').on('click', '#edit_post_button', function() {
	    	var token = $('[name=csrf-token]').attr('content');
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');

    		var country = $('#country').val();
    		var institution_name = $('#institution_name').val();
    		var course = $('#course').val();
    		var language = $('#language').val();
    		var description = $('#description').val();
    		var folder_id = $('#hidden_choose').val();
    		var file_type = $('#files_div').attr('data-type');
    		$.ajax({
    			url: '/edit-file/' + id + '/' + groupId,
    			method: 'POST',
    			data: {_token:token, folderId:folderId, country:country, institution_name:institution_name, course:course, language:language, description:description, folder_id:folder_id, action:null, file_type:file_type},
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            		$('#add_post').modal('hide');
	            	}
            	},
            	error: function(errorResult) {
		        	if (errorResult.responseJSON['country'] != undefined) {
						$('#country-error').html(errorResult.responseJSON['country']);
						$('#country_div').addClass('has-error');
						$('.select2-selection').css('border-color', '#D84315');
					} else {
		        		$('#country-error').html('');
		        		$('#country_div').removeClass('has-error');
		        		$('.select2-selection').css('border-color', '#ddd');
		        	}

		        	if (errorResult.responseJSON['institution_name'] != undefined) {
						$('#institution_name-error').html(errorResult.responseJSON['institution_name']);
						$('#institution_div').addClass('has-error');
					} else {
		        		$('#institution_name-error').html('');
		        		$('#institution_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['course'] != undefined) {
						$('#course-error').html(errorResult.responseJSON['course']);
						$('#course_div').addClass('has-error');
					} else {
		        		$('#course-error').html('');
		        		$('#course_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['language'] != undefined) {
						$('#language-error').html(errorResult.responseJSON['language']);
						$('#language_div').addClass('has-error');
					} else {
		        		$('#language-error').html('');
		        		$('#language_div').removeClass('has-error');
		        	}

		        	if (errorResult.responseJSON['description'] != undefined) {
						$('#description-error').html(errorResult.responseJSON['description']);
						$('#description_div').addClass('has-error');
					} else {
		        		$('#description-error').html('');
		        		$('#description_div').removeClass('has-error');
		        	}
		        }
    		})
    	})

    	$('body').on('click', '.delete_file_button', function() {
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		$.ajax({
	            url: '/delete-file/' + id + '/' + groupId,
	            method: 'GET',
	            success: function(result) {
	            	if (result.success == true) {
	            		$('#file_div' + result.id).remove();
	            	}
            	}
            })
    	})
    /* /Files */

    /* Files navigation */
    	$('#all_files').on('click', function() {
    		$('#files_div').attr('data-type', '');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/all-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#pdfs').on('click', function() {
    		$('#files_div').attr('data-type', 'pdf');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/pdf-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#texts').on('click', function() {
    		$('#files_div').attr('data-type', 'txt');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/txt-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#docs').on('click', function() {
    		$('#files_div').attr('data-type', 'docs');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/docs-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#ppt').on('click', function() {
    		$('#files_div').attr('data-type', 'ppt');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/ppt-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#video').on('click', function() {
    		$('#files_div').attr('data-type', 'video');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/video-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#photo').on('click', function() {
    		$('#files_div').attr('data-type', 'photo');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/photo-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})

    	$('#audio').on('click', function() {
    		$('#files_div').attr('data-type', 'audio');
    		var id = $('#group').attr('data-id');
    		var folderId = $('#folder1').attr('data-id');
    		var url = '/audio-files/' + id;
    		if(folderId != undefined) {
    			url += '/' + folderId;
    		};
    		$.ajax({
    			url: url,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#files_div').html(result.html);
	            	}
            	}
    		})
    	})
    /* /Files navigation */

    /* Notes */
    	$('body').on('click', '#button_add_note', function() {
    		var id = $('#group').attr('data-id');
    		$.ajax({
    			url: '/add-note/' + id,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#note_modal_content').html(result.html);
    					$('#add_note').modal('show');
	            	}
            	}
    		})
    	})

    	$('body').on('click', '#btn_add_note', function() {
	    	var token = $('[name=csrf-token]').attr('content');
    		var id = $('#group').attr('data-id');
    		var name = $('#note_name').val();
    		var folder_id = $('#note_folder_id').val();
    		var content = $('#note_content').val();
    		$.ajax({
    			url: '/add-note/' + id,
    			method: 'POST',
    			data: {_token:token, name:name, content:content, folder_id:folder_id},
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#notes_div').html(result.html);
	            		$('#add_note').modal('hide');
	            	}
            	},
            	error: function(errorResult) {
		        	if (errorResult.responseJSON['name'] != undefined) {
						$('#note-name-error').html(errorResult.responseJSON['name']);
						$('#note_name_div').addClass('has-error');
					} else {
		        		$('#note-name-error').html('');
		        		$('#note_name_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['content'] != undefined) {
						$('#note-content-error').html(errorResult.responseJSON['content']);
						$('#note_content_div').addClass('has-error');
					} else {
		        		$('#note-content-error').html('');
		        		$('#note_content_div').removeClass('has-error');
		        	}
		        }
    		})
    	})

    	$('body').on('click', "#delete_note_btn", function() {
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		$.ajax({
    			url: '/delete-note/' + id + '/' + groupId,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#note_div' + result.id).remove();
	            		$('#note_modal' + result.id).modal('hide');
	            	}
            	}
    		})
    	})

    	$('body').on('click', '#edit_note_btn', function() {
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');
    		$.ajax({
    			url: '/edit-note/' + id + '/' + groupId,
    			method: 'GET',
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#note_modal_content').html(result.html)
    					$('#add_note').modal('show');
	            	}
            	}
    		})
    	})

    	$('body').on('click', '#btn_edit_note', function() {
	    	var token = $('[name=csrf-token]').attr('content');
    		var id = $(this).attr('data-id');
    		var groupId = $('#group').attr('data-id');

    		var name = $('#note_name').val();
    		var folder_id = $('#note_folder_id').val();
    		var content = $('#note_content').val();
    		$.ajax({
    			url: '/edit-note/' + id + '/' + groupId,
    			method: 'POST',
    			data: {_token:token, name:name, content:content, folder_id:folder_id},
    			success: function(result) {
	            	if (result.success == true) {
	            		$('#note_modal' + result.id).modal('hide');
	            		$('#add_note').modal('hide');
	            	}
            	},
            	error: function(errorResult) {
		        	if (errorResult.responseJSON['name'] != undefined) {
						$('#note-name-error').html(errorResult.responseJSON['name']);
						$('#note_name_div').addClass('has-error');
					} else {
		        		$('#note-name-error').html('');
		        		$('#note_name_div').removeClass('has-error');
		        	}

					if (errorResult.responseJSON['content'] != undefined) {
						$('#note-content-error').html(errorResult.responseJSON['content']);
						$('#note_content_div').addClass('has-error');
					} else {
		        		$('#note-content-error').html('');
		        		$('#note_content_div').removeClass('has-error');
		        	}
		        }
    		})
    	})
    /* /Notes */

    /* Discussions */
	    $('.reply').click(function () {
	    	$('#discussForm').attr('action', '/add-subdiscussion');
	    	$('#discussForm #discussion_id').val($(this).data('id'));
	    	$('#discussForm textarea').attr('placeholder', 'Reply to ' + $(this).data('message'));
	    	$('#discussForm #send').hide();
	    	$('#discussForm #reply').show();
	    	$('#discussForm #cancell').show();
	    });

	    $('#discussForm #cancell').click(function () {
	    	$('#discussForm').attr('action', '/add-discussion');
	    	$('#discussForm textarea').attr('placeholder', 'Enter your message...');
	    	$('#discussForm textarea').val('');
	    	$('#discussForm #send').show();
	    	$('#discussForm #reply').hide();
	    	$('#discussForm #cancell').hide();
	    	$('#discussForm #edit').hide();
	    });

	    $('.edit').click(function () {
	    	if($(this).data('type') == 'disc') {
	    		$('#discussForm').attr('action', '/edit-discussion');
	    		$('#discussForm #discussion_id').val($(this).data('id'));
	    	} else {
	    		$('#discussForm').attr('action', '/edit-subdiscussion');
	    		$('#discussForm #subdiscussion_id').val($(this).data('id'));
	    	}
	    	$('#discussForm textarea').val($(this).data('message'));
	    	$('#discussForm #send').hide();
	    	$('#discussForm #reply').hide();
	    	$('#discussForm #cancell').show();
	    	$('#discussForm #edit').show();
	    });
    /* /Discussions */
});