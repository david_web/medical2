$(".pickadate").pickadate({
	format: 'yyyy/mm/dd',
	min: [new Date()]
})

$(".users").select2();

function displayUsers() {
	var users = $( ".users" ).val();
	$('#users').val(JSON.stringify(users));
	if (users == null) {
		$('#users').val('');
	}
}
$( "select" ).change( displayUsers );

$('#create_announcement_button').on('click', function() {
	var id = $('#announcement_panel').attr('data-id');
	$.ajax({
		url: '/add-announcement/' + id,
		method: 'GET',
		success: function(result) {
        	if (result.success == true) {
        		$('#anouncement_modal_content').html(result.html);

        		$('.pickadate').pickadate({
        			format: 'yyyy/mm/dd'
        		});
        		$('.pickatime').pickatime({
        			format: 'HH:i'
        		});
        		$('.users').select2();
        		function displayUsers() {
					var users = $( ".users" ).val();
					$('#users').val(JSON.stringify(users));
					if (users == null) {
						$('#users').val('');
					}
				}
				$( "select" ).change(displayUsers);

        		$('#announcement_modal').modal('show');
        	}
        }
	})
})

$('body').on('click', '#btn_add_announcement', function() {
	var token = $('[name=csrf-token]').attr('content');
	var groupId = $('#announcement_panel').attr('data-id');
	var title = $('#title').val();
	var date = $('#date').val();
	var time = $("input[name='time']").val();
	var users = $('#users').val();
	$.ajax({
		url: '/add-announcement',
        method: 'POST',
        data: {_token:token, group_id:groupId, title:title, date:date, time:time, users:users},
        success: function(result) {
        	if (result.success == true) {
        		$('#announcement_modal').modal('hide');
        		$('#announcement_div').html(result.html);
        	}
        },
        error: function(errorResult) {
        	if (errorResult.responseJSON['title'] != undefined) {
				$('#title-error').html(errorResult.responseJSON['title']);
				$('#title_div').addClass('has-error');
        	} else {
        		$('#title-error').html('');
        		$('#title_div').removeClass('has-error');
        	}

        	if (errorResult.responseJSON['date'] != undefined) {
				$('#date-error').html(errorResult.responseJSON['date']);
				$('#date_div').addClass('has-error');
			} else {
        		$('#date-error').html('');
        		$('#date_div').removeClass('has-error');
        	}

			if (errorResult.responseJSON['time'] != undefined) {
				$('#time-error').html(errorResult.responseJSON['time']);
				$('#time_div').addClass('has-error');
			} else {
        		$('#time-error').html('');
        		$('#time_div').removeClass('has-error');
        	}

			if (errorResult.responseJSON['users'] != undefined) {
				$('#users-error').html(errorResult.responseJSON['users']);
				$('#users_div').addClass('has-error');
				$('.select2-selection').css('border-color', '#D84315');
			} else {
        		$('#users-error').html('');
        		$('#users_div').removeClass('has-error');
        		$('.select2-selection').css('border-color', '#ddd');
        	}
        }
	})
})

$('body').on('click', '#edit_announcement', function() {
	var id = $(this).attr('data-id');
	var groupId = $('#announcement_panel').attr('data-id');
	$.ajax({
		url: '/edit-announcement/' + id + '/' + groupId,
		method: 'GET',
		success: function(result) {
        	if (result.success == true) {
        		$('#anouncement_modal_content').html(result.html);
        		$('.users').select2();
        		function displayUsers() {
					var users = $( ".users" ).val();
					$('#users').val(JSON.stringify(users));
					if (users == null) {
						$('#users').val('');
					}
				}
				$( "select" ).change(displayUsers);
        		$('.users').trigger('change');
        		$('.pickadate').pickadate({
        			format: 'yyyy/mm/dd'
        		});
        		$('.pickatime').pickatime({
        			format: 'HH:i'
        		});
        		$('#announcement_modal').modal('show');
        	}
    	}
	})
})

$('body').on('click', '#btn_edit_announcement', function() {
	var token = $('[name=csrf-token]').attr('content');
	var id = $(this).attr('data-id');
	var groupId = $('#announcement_panel').attr('data-id');
	var title = $('#title').val();
	var date = $('#date').val();
	var time = $('#time').val();
	var users = $('#users').val();
	$.ajax({
		url: '/edit-announcement/' + id + '/' + groupId,
		method: 'POST',
		data: {_token:token, group_id:groupId, title:title, date:date, time:time, users:users},
		success: function(result) {
        	if (result.success == true) {
        		$('#announcement_div').html(result.html);
        		$('#announcement_modal').modal('hide');
        	}
        },
        error: function(errorResult) {
        	if (errorResult.responseJSON['title'] != undefined) {
				$('#title-error').html(errorResult.responseJSON['title']);
				$('#title_div').addClass('has-error');
        	} else {
        		$('#title-error').html('');
        		$('#title_div').removeClass('has-error');
        	}

        	if (errorResult.responseJSON['date'] != undefined) {
				$('#date-error').html(errorResult.responseJSON['date']);
				$('#date_div').addClass('has-error');
			} else {
        		$('#date-error').html('');
        		$('#date_div').removeClass('has-error');
        	}

			if (errorResult.responseJSON['time'] != undefined) {
				$('#time-error').html(errorResult.responseJSON['time']);
				$('#time_div').addClass('has-error');
			} else {
        		$('#time-error').html('');
        		$('#time_div').removeClass('has-error');
        	}

			if (errorResult.responseJSON['users'] != undefined) {
				$('#users-error').html(errorResult.responseJSON['users']);
				$('#users_div').addClass('has-error');
				$('.select2-selection').css('border-color', '#D84315');
			} else {
        		$('#users-error').html('');
        		$('#users_div').removeClass('has-error');
        		$('.select2-selection').css('border-color', '#ddd');
        	}
        }
	})
})

$('body').on('click', '.delete_announcement', function() {
	var id = $(this).attr('data-id');
	var groupId = $('#announcement_panel').attr('data-id');
	$.ajax({
		url: '/delete-announcement/' + id + '/' + groupId,
		method: 'GET',
		success: function(result) {
        	if (result.success == true) {
        		$('#announcement_div' + result.id).remove();
        		$('#hr' + result.id).remove();
        	}
        }
	})
})