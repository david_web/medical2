
var app = new Vue({
	el: '#app',
	data: {
		familyName: '',
		errors: '',
		marketing: '',
		healthInsuranceName: '',
		policyNumber: '',
		familyDoctorName: '',
		familyDoctorNumber: '',
		contactInformation: [{
				errors: {},
				isDelete: false,
				first_name: '',
				middle_name: '',
				last_name: '',
				relationship: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zip: '',
				cell_phone: '',
				home_phone: '',
				work_phone: '',
				email: '',
				is_primary: 'null'
			}],
		membershipInformation: [{
				errors: {},
				isDelete: false,
				first_name: '',
				middle_name: '',
				last_name: '',
				nick_name: '',
				dob: '',
				gender: '',
				cell_phone: '',
				email: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zip: '',
				medical_info: '',
				allergies: ''
			}],
		contactId: 0,
		membershipId: 0
	},

	methods: {
		addContact: function () {
			console.log(1);
			this.contactId++;
			var a = {
				isDelete: true,
				contactId: this.contactId,
				first_name: '',
				middle_name: '',
				last_name: '',
				relationship: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zip: '',
				cell_phone: '',
				home_phone: '',
				work_phone: '',
				email: '',
				is_primary: 'null'
			}
			this.contactInformation.push(a);
		},

		addMembership: function () {
			this.membershipId++;
			var a = {
				errors: {},
				isDelete: true,
				membershipId: this.membershipId,
				first_name: '',
				middle_name: '',
				last_name: '',
				nick_name: '',
				dob: '',
				gender: '',
				cell_phone: '',
				email: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zip: '',
				medical_info: '',
				allergies: ''
			}
			this.membershipInformation.push(a);
		},

		addFamily: function () {
			var data = {
				family_name: this.familyName,
				marketing: this.marketing,
				health_insurance: this.healthInsuranceName,
				health_insurance_policy:this.policyNumber,
				doctor_name: this.familyDoctorName,
				doctor_phone: this.familyDoctorNumber,

				contacts: this.contactInformation
			};
			axios.post('/families', data).then(function (response) {
				if(response.data.result == 'success') {
					location.href = '/families';
				} else {
					if(response.data.family) {
						this.errors = response.data.family;
						console.log(this.errors)
					}
					// this.errors = response.data;
				}
			});
		},

		deleteContact: function (id) {
			this.contactId--;
			this.contactInformation.splice(id, 1);
			for(var i = 0; i < this.contactInformation.length; i++) {
				this.contactInformation[i].contactId = i;
			}

		},

		deleteMembership: function (id) {
			this.membershipId--;
			this.membershipInformation.splice(id, 1);
			for(var i = 0; i < this.membershipInformation.length; i++) {
				this.membershipInformation[i].membershipId = i;
			}

		},

	},

	mounted: function () {
		// alert(1);
	}
	
});