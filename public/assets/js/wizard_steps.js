/* ------------------------------------------------------------------------------
*
*  # Steps wizard
*
*  Specific JS code additions for wizard_steps.html page
*
*  Version: 1.1
*  Latest update: Dec 25, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Wizard examples
    // ------------------------------

    // Basic wizard setup
    $(".steps-basic").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Async content loading
    $(".steps-async").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onContentLoaded: function (event, currentIndex) {
            $(this).find('select.select').select2();

            $(this).find('select.select-simple').select2({
                minimumResultsForSearch: Infinity
            });

            $(this).find('.styled').uniform({
                radioClass: 'choice'
            });

            $(this).find('.file-styled').uniform({
                fileButtonClass: 'action btn bg-warning'
            });
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Saving wizard state
    $(".steps-state-saving").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        saveState: true,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Specify custom starting step
    $(".steps-starting-step").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        startIndex: 2,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    //
    // Wizard with validation
    //

    // Show form
    var form = $(".steps-validation").show();


    // Initialize wizard
    $(".steps-validation").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex) {

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }

            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {

                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },

        onStepChanged: function (event, currentIndex, priorIndex) {

            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }

            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },

        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },

        onFinished: function (event, currentIndex) {
            $('#errors').empty().css('display', 'none');
            var data = {
                family_name: '',
                marketing: '',
                health_insurance: '',
                health_insurance_policy: '',
                doctor_name: '',
                doctor_phone: '',
                contacts: [],
                memberships: []
            };
            data.family_name = $('#family_name').val();
            data.marketing = $('#marketing').val();
            data.health_insurance = $('#health_insurance').val();
            data.health_insurance_policy = $('#health_insurance_policy').val();
            data.doctor_name = $('#doctor_name').val();
            data.doctor_phone = $('#doctor_phone').val();
            $('.contacts').each(function () {
                var a = {
                    first_name: $(this).find($('[name=first_name]')).val(),
                    middle_name: $(this).find($('[name=middle_name]')).val(),
                    last_name: $(this).find($('[name=last_name]')).val(),
                    relationship: $(this).find($('[name=relationship]')).val(),
                    address1: $(this).find($('[name=address1]')).val(),
                    address2: $(this).find($('[name=address2]')).val(),
                    city: $(this).find($('[name=city]')).val(),
                    state: $(this).find($('[name=state]')).val(),
                    zip: $(this).find($('[name=zip]')).val(),
                    cell_phone: $(this).find($('[name=cell_phone]')).val(),
                    home_phone: $(this).find($('[name=home_phone]')).val(),
                    work_phone: $(this).find($('[name=work_phone]')).val(),
                    email: $(this).find($('[name=email]')).val(),
                    is_primary: $(this).find($('[name=is_primary]')).val(),
                }
                data.contacts.push(a);
            });
            $('.memberships').each(function () {
                var a = {
                    first_name: $(this).find($('[name=first_name]')).val(),
                    middle_name: $(this).find($('[name=middle_name]')).val(),
                    last_name: $(this).find($('[name=last_name]')).val(),
                    nick_name: $(this).find($('[name=nick_name]')).val(),
                    dob: $(this).find($('[name=dob]')).val(),
                    gender: $(this).find($('[name=gender]')).val(),
                    cell_phone: $(this).find($('[name=cell_phone]')).val(),
                    email: $(this).find($('[name=email]')).val(),
                    address1: $(this).find($('[name=address1]')).val(),
                    address2: $(this).find($('[name=address2]')).val(),
                    city: $(this).find($('[name=city]')).val(),
                    state: $(this).find($('[name=state]')).val(),
                    zip: $(this).find($('[name=zip]')).val(),
                    medical_info: $(this).find($('[name=medical_info]')).val(),
                    allergies: $(this).find($('[name=allergies]')).val(),
                    classes: $(this).find($('[name=classes]')).val(),
                }
                data.memberships.push(a);
            });
            $.post('/families', data, function (response) {
                if(response.result == 'success') {
                    location.href = '/families';
                } else {
                    $('#errors').css('display', 'block');
                    $.each(response, function (key1, errors) {
                        $.each(errors, function (key2, error) {
                            $.each(error, function (index, value) {
                                $('#errors').append('<div>' + value + '</div>')
                            })
                        });
                    });
                }
            });

            data.contacts = [];
            data.memberships = [];
        }
    });


    // Initialize validation
    $(".steps-validation").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        rules: {
            email: {
                email: true
            }
        }
    });



    // Initialize plugins
    // ------------------------------

    // Select2 selects
    $('.select').select2();


    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });
    
});
